# Willow Pattern Adventure disassembled

## What
This is a project to find out the inner workings of the Commodore 64 game 'The willow pattern adventure' and add some features to it.

## Copyright
The game was made by Gregg Duddle for Firebird, music by David Whittaker. Copyright still applies to the game.  

## Code
Code is made in KickAssembler format.

### Compile and run script for Relaunch:
```java -jar [pathtoKickass]\KickAss.jar RSOURCEFILE -afo```


```x64 -moncommands vice.txt -8  karate.d64 OUTFILE```

game is run with Vice emulator with breakpoints set in file 'vice.txt'