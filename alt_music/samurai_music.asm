.var brkFile = createFile("vice.txt") 

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}

.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		

.pc = $0810 


	sei
	lda #$35
	sta $01

	ldy #18
!:	ldx #$00
sm1:	lda music,x
sm2:	sta $e000,x
	inx
	bne sm1
	inc sm1+2
	inc sm2+2
	dey
	bne !-

	lda #2
	jsr $f000//init_music
	lda #$37
	sta $01
	lda #<start 		
	sta $0314
	lda #>start
	sta $0315
	cli
	lda #$0f
	sta 54296
	rts

//* = $e000
start:
	sei
	lda #$35
	sta $01
	 jsr music
	lda #$37
	sta $01
	//rti 
	jmp $ea31
music:		
.pseudopc $e000 {	
jmp  LE1B4    //
DE003: .byte  $FF // 
DE004: .byte  $01 // 
DE005: .byte  $03, $02 // 
DE007: .byte  $01, $03, $02 // 
DE00A: .byte  $80, $14, $81 // 
DE00D: .byte  $78, $05, $03 // 
DE010: .byte  $00, $00, $00 // 
DE013: .byte  $11 // 
DE014: .byte  $2F // 
DE015: .byte  $00 // 
DE016: .byte  $0E // 
DE017: .byte  $43, $E5, $DA // 
DE01A: .byte  $E8, $E8, $E8 // 
DE01D: .byte  $E1, $D5, $63 // 
DE020: .byte  $EC, $EC, $EA // 
DE023: .byte  $04, $04, $02 // 
DE026: .byte  $00, $00, $00 // 
DE029: .byte  $0A, $09, $08 // 
DE02C: .byte  $0A, $0A, $0A // 
DE02F: .byte  $00, $00, $00 // 
DE032: .byte  $00, $00, $00 // 
DE035: .byte  $03, $02, $06 // 
DE038: .byte  $00, $00, $0A // 
DE03B: .byte  $3F, $00, $07 // 
DE03E: .byte  $02, $00, $08 // 
DE041: .byte  $30, $98, $C0 // 
DE044: .byte  $0B, $05, $70 // 
DE047: .byte  $30, $98, $C0 // 
DE04A: .byte  $0B, $05, $70 // 
DE04D: .byte  $30, $CF, $A0 // 
DE050: .byte  $0B, $05, $73 // 
DE053: .byte  $00, $00, $01 // 
DE056: .byte  $5F, $5F, $5F // 
DE059: .byte  $EA, $EA, $EA, $00, $00, $00 // 
DE05F: .byte  $DC // 
DE060: .byte  $E9 // 
DE061: .byte  $DB // 
DE062: .byte  $E9 // 
DE063: .byte  $D5 // 
DE064: .byte  $EE, $DC, $E9, $DB, $E9, $B9, $EE, $DC // 
       .byte  $E9, $DB, $E9, $03, $EE // 
DE071: .byte  $00, $06, $0C, $00, $00 // 
DE076: .byte  $FE, $FD, $FB, $F7, $EF, $DF, $BF, $7F // 
DE07E: .byte  $01, $02, $04, $08, $10, $20, $40, $80 // 
DE086: .byte  $00, $0C, $1C, $2D, $3E, $51, $66, $7B // 
       .byte  $91, $A9, $C3, $DD, $FA // 
DE093: .byte  $02, $05, $08, $0B, $11, $17, $23, $2F // 
		.byte  $47, $01, $03, $07, $06, $04, $01, $01 // 
//jump tables
DE0A3: .byte  $F4//<LE3F4  
DE0A4: .byte  $E3, $E6, $E2, $EB, $E3, $00, $E4 //>LE3F4, <LE2E6, >LE2E6, <LE3EB, >LE3EB, <LE400, >LE400 //  
DE0AB: .byte  $00 // 
DE0AC: .byte  $00, $43, $E8, $DA, $E8, $E5, $E8, $68 // 
       .byte  $E8, $DA, $E8, $16, $E9, $AA, $E8, $DA // 
       .byte  $E8, $3A, $E9, $6A, $E9, $70, $E9, $74 // 
       .byte  $E9, $78, $E9, $7B, $E9, $7E, $E9, $81 // 
       .byte  $E9, $B1, $E9, $B4, $E9, $B7, $E9, $BA // 
       .byte  $E9, $BD, $E9, $C0, $E9, $C3, $E9, $C6 // 
       .byte  $E9, $C9, $E9, $CC, $E9, $CF, $E9, $D2 // 
       .byte  $E9, $D5, $E9, $D8, $E9 // 
DE0E9: .byte  $00 // 
DE0EA: .byte  $00, $DB, $E9, $DB, $E9, $DB, $E9, $DB // 
       .byte  $E9, $DB, $E9, $DB, $E9, $DB, $E9, $DB // 
       .byte  $E9, $DB, $E9, $31, $EA, $31, $EA, $31 // 
       .byte  $EA, $39, $EA, $39, $EA, $39, $EA, $3B // 
       .byte  $EA, $43, $EA, $45, $EA, $47, $EA, $47 // 
       .byte  $EA, $4B, $EA, $4B, $EA, $4F, $EA, $4F // 
       .byte  $EA, $53, $EA, $55, $EA, $55, $EA, $59 // 
       .byte  $EA, $5B, $EA, $5D, $EA // 
DE127: .byte  $00 // 
DE128: .byte  $00, $5F, $EA, $5F, $EA, $5F, $EA, $5F // 
       .byte  $EA, $5F, $EA, $5F, $EA, $5F, $EA, $5F // 
       .byte  $EA, $5F, $EA, $29, $ED, $29, $ED, $29 // 
       .byte  $ED, $96, $ED, $96, $ED, $96, $ED, $99 // 
       .byte  $ED, $B2, $ED, $B8, $ED, $BE, $ED, $BE // 
       .byte  $ED, $CA, $ED, $CA, $ED, $D4, $ED, $D4 // 
       .byte  $ED, $E0, $ED, $E9, $ED, $E9, $ED, $F1 // 
       .byte  $ED, $F7, $ED // 
DE163: .byte  $FD, $ED, $95, $E1, $98, $E1, $9B, $E1 // 
       .byte  $9E, $E1, $A1, $E1, $A4, $E1, $A5, $E1 // 
       .byte  $A6, $E1, $A7, $E1, $A9, $E1, $AB, $E1 // 
       .byte  $AD, $E1, $AE, $E1, $B0, $E1, $B1, $E1 // 
       .byte  $B2 // 
DE184: .byte  $E1, $03, $03, $03, $03, $03, $01, $01 // 
       .byte  $01, $02, $02, $02, $01, $02, $01, $01 // 
DE194: .byte  $01, $04, $04, $04, $04, $04, $04, $04 // 
       .byte  $04, $04, $04, $04, $04, $05, $05, $05 // 
       .byte  $04, $03, $03, $03, $03, $03, $03, $05 // 
       .byte  $05, $03, $03, $03, $02, $03, $03 // 
DE1B3: .byte  $1C // 
LE1B4: ldx $FF       // 
      beq  LE203    // 
      lda #$00      // 
      sta  DE529    // 
      sta  DE52A    // 
      sta  DE52B    // 
      ldy  DE184,X  // 
      txa           // 
      asl           // 
      tax           // 
      dey           // 
      tya           // 
      clc           // 
      adc  DE163,X  // 
      sec           // 
      sbc #$94      // 
      sta  DE1B3    // 
LE1D5: jsr  LE503    // 
      ldx  DE1B3    // 
      lda  DE194,X  // 
      cmp  DE525    // 
      bmi  LE1F9    // 
      pha           // 
      txa           // 
      ldx  DE524    // 
      sta  DE004,X  // 
      lda #$FF      // 
      sta  DE529,X  // 
      lda #$00      // 
      sta  DE007,X  // 
      pla           // 
      sta  DE526,X  // 
LE1F9: dec  DE1B3    // 
      dey           // 
      bpl  LE1D5    // 
      lda #$00      // 
      sta $FF       // 
LE203: cld           // 
      ldx #$02      // 
      stx  DE003    // 
LE209: ldx  DE003    // 
      lda  DE004,X  // 
      beq  LE214    // 
      jsr  LE21D    // 
LE214: dec  DE003    // 
      bpl  LE209    // 
      jsr  LE4A9    // 
      rts           // 
LE21D: cmp  DE007,X  // 
      beq  LE295    // 
      sta  DE007,X  // 
      ldy  DE500,X  // 
      lda #$00      // 
      sta $D400,Y   // Voice 1: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D401,Y   // Voice 1: Frequency Control - High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D402,Y   // Voice 1: Pulse Waveform Width - Low-Byte MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D403,Y   // Voice 1: Pulse Waveform Width - High-Byte 
      sta $D404,Y   // Voice 1: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 1 with Osc. 3 Output,  1 = On 1 Synchronize Osc. 1 with Osc. 3 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D405,Y   // Envelope Generator 1: Attack / Decay Cycle  Control  MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D406,Y   // Envelope Generator 1: Sustain / Release Cycle  Control  MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D415     // Filter Cutoff Frequency: Low-Nybble  (Bits 2-0)  MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta  DE053,X  // 
      sta  DE023,X  // 
      sta  DE032,X  // 
      sta  DE00A,X  // 
      lda #$01      // 
      sta  DE026,X  // 
      lda #$FF      // 
      sta  DE82B,X  // 
      ldy  DE004,X  // 
      tya           // 
      asl           // 
      tay           // 
      lda  DE127,Y  // 
      sta  DE056,X  // 
      lda  DE128,Y  // 
      sta  DE059,X  // 
      lda  DE0AB,Y  // 
      sta  DE017,X  // 
      lda  DE0AC,Y  // 
      sta  DE01A,X  // 
      lda  DE071,X  // 
      tax           // 
      lda  DE0E9,Y  // 
      sta  DE05F,X  // 
      sta  DE061,X  // 
      lda  DE0EA,Y  // 
      sta  DE060,X  // 
      sta  DE062,X  // 
      inc  DE05F,X  // 
      bne  LE294    // 
      inc  DE060,X  // 
LE294: rts           // 
LE295: lda  DE071,X  // 
      tay           // 
      lda  DE05F,Y  // 
      sta  LE2FC+1  // 
      lda  DE060,Y  // 
      sta  LE2FC+2  // 
      lda  DE061,Y  // 
      sta  LE2F0+1  // 
      lda  DE062,Y  // 
      sta  LE2F0+2  // 
      lda  DE063,Y  // 
      sta $F7       // 
      lda  DE064,Y  // 
      sta $F8       // 
      lda  DE017,X  // 
      sta $F9       // 
      lda  DE01A,X  // 
      sta $FA       // 
      lda  DE01D,X  // 
      sta $FB       // 
      lda  DE020,X  // 
      sta $FC       // 
LE2CF: ldy  DE023,X  // 
      lda ($F9),Y   // 
      asl           // 
      tay           // 
      lda  DE0A3,Y  // 
      sta $FD       // 
      lda  DE0A4,Y  // 
      sta $FE       // 
      ldy  DE023,X  // 
      jmp ($00FD)    // 
LE2E6:      
      lda  DE026,X  // 
      beq  LE312    // 
      iny           // 
      lda ($F9),Y   // 
      asl           // 
      tay           // 
LE2F0: lda  DE9DB,Y  // 
      clc           // 
      adc  DE056,X  // 
      sta  DE01D,X  // 
      sta $FB       // 
LE2FC: lda  DE9DC,Y  // 
      adc  DE059,X  // 
      sta  DE020,X  // 
      sta $FC       // 
      lda #$00      // 
      sta  DE029,X  // 
      sta  DE026,X  // 
      jmp  LE33C    // 
LE312: dec  DE02C,X  // 
      beq  LE31A    // 
      jmp  LE499    // 
LE31A: lda  DE02F,X  // 
      bne  LE339    // 
      lda #$01      // 
      sta  DE02F,X  // 
      sta  DE02C,X  // 
      lda  DE032,X  // 
      and #$40      // 
      bne  LE336    // 
      lda  DE00A,X  // 
      and #$FE      // 
      sta  DE00A,X  // 
LE336: jmp  LE499    // 
LE339: inc  DE029,X  // 
LE33C: ldy  DE029,X  // 
      lda ($FB),Y   // 
      bpl  LE35D    // 
      cmp #$FF      // 
      bne  LE355    // 
      lda #$01      // 
      sta  DE026,X  // 
      inc  DE023,X  // 
      inc  DE023,X  // 
      jmp  LE2CF    // 
LE355: and #$7F      // 
      jsr  LE42E    // 
      jmp  LE339    // 
LE35D: pha           // 
      and #$0F      // 
      sta  DE038,X  // 
      tay           // 
      bne  LE36A    // 
      pla           // 
      jmp  LE39E    // 
LE36A: lda  DE086,Y  // 
      sta  DE047,X  // 
      lda #$01      // 
      sta  DE04A,X  // 
      pla           // 
      lsr           // 
      lsr           // 
      lsr           // 
      lsr           // 
      clc           // 
      ldy #$05      // 
      adc ($F7),Y   // 
      sta  DE035,X  // 
      tay           // 
      beq  LE39E    // 
      bpl  LE391    // 
      lda #$00      // 
      sta  DE047,X  // 
      sta  DE04A,X  // 
      beq  LE39E    // 
LE391: lda  DE047,X  // 
LE394: asl           // 
      rol  DE04A,X  // 
      dey           // 
      bne  LE394    // 
      sta  DE047,X  // 
LE39E: lda #$00      // 
      sta  DE02F,X  // 
      inc  DE029,X  // 
      ldy  DE029,X  // 
      lda ($FB),Y   // 
      pha           // 
      and #$0F      // 
      tay           // 
      lda  DE093,Y  // 
      sta  DE02C,X  // 
      cpy #$0E      // 
      bne  LE3BC    // 
      sta  DE02F,X  // 
LE3BC: lda  DE032,X  // 
      bne  LE3D5    // 
      lda  DE047,X  // 
      sta  DE041,X  // 
      lda  DE04A,X  // 
      sta  DE044,X  // 
      lda #$FF      // 
      sta  DE82B,X  // 
      jsr  LE4F4    // 
LE3D5: pla           // 
      and #$C0      // 
      sta  DE032,X  // 
      lda  DE038,X  // 
      beq  LE3E8  // 
      lda  DE00A,X  // 
      ora #$01      // 
      sta  DE00A,X  // 
LE3E8: jmp  LE499    // 
LE3EB:
      iny           // 
      lda ($F9),Y   // 
      sta  DE023,X  // 
      jmp  LE2CF    // 
LE3F4: lda #$00
		sta $e004,x
		sta $e007,x
		sta $e526,x
		rts
//		.byte  $A9, $00, $9D, $04, $E0, $9D, $07, $E0 // 
//       .byte  $9D, $26, $E5, $60 // 
LE400:      lda  DE053,X  // 
      bne  LE414    // 
      iny           // 
      lda ($F9),Y   // 
      sta  DE023,X  // 
      iny           // 
      lda ($F9),Y   // 
      sta  DE053,X  // 
      jmp  LE2CF    // 
LE414: dec  DE053,X  // 
      beq  LE422    // 
      iny           // 
      lda ($F9),Y   // 
      sta  DE023,X  // 
      jmp  LE2CF    // 
LE422: inc  DE023,X  // 
      inc  DE023,X  // 
      inc  DE023,X  // 
      jmp  LE2CF    // 
LE42E: asl           // 
      tay           // 
      lda  DE071,X  // 
      tax           // 
      lda  DEF8B,Y  // 
      clc           // 
      adc #$03      // 
      sta $F7       // 
      sta  DE063,X  // 
      lda  DEF8C,Y  // 
      adc #$EE      // 
      sta $F8       // 
      sta  DE064,X  // 
      ldx  DE003    // 
      ldy #$00      // 
      lda ($F7),Y   // 
      sta  DE00A,X  // 
      iny           // 
      lda ($F7),Y   // 
      sta  DE00D,X  // 
      iny           // 
      lda ($F7),Y   // 
      sta  DE010,X  // 
      iny           // 
      lda  DE013    // 
      and  DE076,X  // 
      sta  DE013    // 
      lda ($F7),Y   // 
      and #$0F      // 
      beq  LE497    // 
      lda ($F7),Y   // 
      and #$F0      // 
      sta  DE498    // 
      lda  DE013    // 
      ora  DE07E,X  // 
      and #$0F      // 
      ora  DE498    // 
      sta  DE013    // 
      iny           // 
      lda  DE014    // 
      and #$0F      // 
      sta  DE498    // 
      lda ($F7),Y   // 
      and #$70      // 
      ora  DE498    // 
      sta  DE014    // 
LE497: rts           // 
DE498: .byte  $0F // 
LE499: jsr  LE788    // 
      jsr  LE6E5    // 
      jsr  LE611    // 
      jsr  LE5CD    // 
      jsr  LE67D    // 
      rts           // 
LE4A9: lda  DE014    // 
      sta $D418     // Select Filter Mode and Volume 7 Cut-Off Voice 3 Output: 1 = Off, 0 = On   6 Select Filter High-Pass Mode: 1 = On 5 Select Filter Band-Pass Mode: 1 = On 4 Select Filter Low-Pass Mode: 1 = On 3-0 Select Output Volume: 0-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DE013    // 
      sta $D417     // Filter Resonance Control / Voice Input  Control 7-4-2006 Select Filter Resonance: 0-15 3 Filter External Input: 1 = Yes, 0 = No 2 Filter Voice 3 Output: 1 = Yes, 0 = No  Filter Voice 2 Output: 1 = Yes, 0 = No 0 Filter Voice 1 Output: 1 = Yes, 0 = No    MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DE015    // 
      sta $D415     // Filter Cutoff Frequency: Low-Nybble  (Bits 2-0)  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DE016    // 
      sta $D416     // Filter Cutoff Frequency: High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      ldx #$02      // 
LE4C3: ldy  DE500,X  // 
      lda  DE04D,X  // 
      sta $D400,Y   // Voice 1: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DE050,X  // 
      sta $D401,Y   // Voice 1: Frequency Control - High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DE03B,X  // 
      sta $D402,Y   // Voice 1: Pulse Waveform Width - Low-Byte MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DE03E,X  // 
      sta $D403,Y   // Voice 1: Pulse Waveform Width - High-Byte 
      lda  DE00D,X  // 
      sta $D405,Y   // Envelope Generator 1: Attack / Decay Cycle  Control  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DE010,X  // 
      sta $D406,Y   // Envelope Generator 1: Sustain / Release Cycle  Control  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DE00A,X  // 
      sta $D404,Y   // Voice 1: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 1 with Osc. 3 Output,  1 = On 1 Synchronize Osc. 1 with Osc. 3 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
      dex           // 
      bpl  LE4C3    // 
      rts           // 
LE4F4: ldy  DE500,X  // 
      lda  DE00A,X  // 
      and #$FE      // 
      sta $D404,Y   // Voice 1: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 1 with Osc. 3 Output,  1 = On 1 Synchronize Osc. 1 with Osc. 3 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
      rts           // 
DE500: .byte  $00, $07, $0E // 
LE503: ldx #$02      // 
      lda #$0F      // 
      sta  DE525    // 
LE50A: lda  DE529,X  // 
      bpl  LE513    // 
LE50F: dex           // 
      bpl  LE50A    // 
      rts           // 
LE513: lda  DE526,X  // 
      cmp  DE525    // 
      bpl  LE50F    // 
      sta  DE525    // 
      stx  DE524    // 
      jmp  LE50F    // 
DE524: .byte  $02 // 
DE525: .byte  $04 // 
DE526: .byte  $04, $04, $04 // 
DE529: .byte  $00 // 
DE52A: .byte  $00 // 
DE52B: .byte  $00 // 
LE52C: lda  DE583,Y  // 
      sta  LE54C+1  // 
      lda  DE584,Y  // 
      sta  LE54C+2  // 
      lda  DE770    // 
      cpy #$12      // 
      bmi  LE547    // 
      ldy #$00      // 
      sty  DE770    // 
      jmp  LE54C    // 
LE547: ldy #$00      // 
      sty  DE771    // 
LE54C: jmp  LE576    // 
DE54F: .byte  $4A, $6E, $70, $E7, $4A, $6E, $70, $E7 // 
       .byte  $4A, $6E, $70, $E7, $8D, $71, $E7, $60 // 
       .byte  $0A, $2E, $71, $E7, $0A, $2E, $71, $E7 // 
       .byte  $0A, $2E, $71, $E7, $0A, $2E, $71, $E7 // 
       .byte  $8D, $70, $E7, $60, $4A, $4A, $4A // 
LE576: lsr           // 
      sta  DE770    // 
      rts           // 
DE57B: .byte  $0A, $0A, $0A, $0A, $8D, $71, $E7, $60 // 
DE583: .byte  $73 // 
DE584: .byte  $E5, $74, $E5, $75, $E5, $76, $E5, $72 // 
       .byte  $E5, $6B, $E5, $67, $E5, $63, $E5, $5F // 
       .byte  $E5, $4F, $E5, $53, $E5, $57, $E5, $5B // 
       .byte  $E5, $7E, $E5, $7D, $E5, $7C, $E5, $7B // 
       .byte  $E5 // 
LE5A5: cpy #$00      // 
      bne  LE5AF    // 
      ldy  DE5C4,X  // 
      jmp  LE5C0    // 
LE5AF: cpy #$01      // 
      bne  LE5B9    // 
      ldy  DE5C7,X  // 
      jmp  LE5C0    // 
LE5B9: cpy #$02      // 
      bne  LE5C0    // 
      ldy  DE5CA,X  // 
LE5C0: lda  DE83D,Y  // 
      rts           // 
DE5C4: .byte  $00, $01, $02 // 
DE5C7: .byte  $01, $02, $00 // 
DE5CA: .byte  $02, $00, $01 // 
LE5CD: lda  DE00A,X  // 
      and #$40      // 
      beq  LE610    // 
      ldy #$0A      // 
      lda ($F7),Y   // 
      and #$07      // 
      cmp #$07      // 
      beq  LE604    // 
      tay           // 
      jsr  LE5A5    // 
      sta  DE770    // 
      ldy #$0A      // 
      lda ($F7),Y   // 
      lsr           // 
      lsr           // 
      lsr           // 
      tay           // 
      jsr  LE52C    // 
      lda  DE770    // 
      sta  DE03B,X  // 
      ldy #$09      // 
      lda ($F7),Y   // 
      clc           // 
      adc  DE771    // 
      sta  DE03E,X  // 
      jmp  LE610    // 
LE604: ldy #$00      // 
      sta  DE03B,X  // 
      ldy #$09      // 
      lda ($F7),Y   // 
      sta  DE03E,X  // 
LE610: rts           // 
LE611: ldy #$0B      // 
      lda ($F7),Y   // 
      and #$07      // 
      cmp #$07      // 
      beq  LE670    // 
      tay           // 
      jsr  LE5A5    // 
      bpl  LE647    // 
      sbc #$80      // 
      sta  DE770    // 
      ldy #$0B      // 
      lda ($F7),Y   // 
      lsr           // 
      lsr           // 
      lsr           // 
      tay           // 
      jsr  LE52C    // 
      lda  DE041,X  // 
      clc           // 
      adc  DE770    // 
      sta  DE04D,X  // 
      lda  DE044,X  // 
      adc  DE771    // 
      sta  DE050,X  // 
      jmp  LE67C    // 
LE647: lda #$80      // 
      sbc  DE83D,Y  // 
      sta  DE770    // 
      ldy #$0B      // 
      lda ($F7),Y   // 
      lsr           // 
      lsr           // 
      lsr           // 
      tay           // 
      jsr  LE52C    // 
      lda  DE041,X  // 
      sec           // 
      cpx  DE770    // 
      sta  DE04D,X  // 
      lda  DE044,X  // 
      cpx  DE771    // 
      sta  DE050,X  // 
      jmp  LE67C    // 
LE670: lda  DE041,X  // 
      sta  DE04D,X  // 
      lda  DE044,X  // 
      sta  DE050,X  // 
LE67C: rts           // 
LE67D: lda  DE013    // 
      and  DE07E,X  // 
      beq  LE6E1    // 
      ldy #$0D      // 
      lda ($F7),Y   // 
      and #$07      // 
      cmp #$07      // 
      beq  LE6D5    // 
      tay           // 
      jsr  LE5A5    // 
      sta  DE770    // 
      ldy #$0D      // 
      lda ($F7),Y   // 
      and #$F0      // 
      lsr           // 
      lsr           // 
      lsr           // 
      tay           // 
      jsr  LE52C    // 
      ldy #$0D      // 
      lda ($F7),Y   // 
      and #$08      // 
      bne  LE6BF    // 
      lda  DE770    // 
      sta  DE015    // 
      clc           // 
      ldy #$0C      // 
      lda ($F7),Y   // 
      adc  DE771    // 
      sta  DE016    // 
      jmp  LE6E1    // 
LE6BF: lda #$00      // 
      sec           // 
      cpx  DE770    // 
      sta  DE015    // 
      ldy #$0C      // 
      lda ($F7),Y   // 
      cpx  DE771    // 
      sta  DE016    // 
      jmp  LE6E1    // 
LE6D5: lda #$00      // 
      sta  DE015    // 
      ldy #$0C      // 
      lda ($F7),Y   // 
      sta  DE016    // 
LE6E1: ldx  DE003    // 
      rts           // 
LE6E5: ldy #$06      // 
      lda ($F7),Y   // 
      sta  DE770    // 
      beq  LE705    // 
      lda  DE044,X  // 
      cmp  DE04A,X  // 
      bcc  LE712    // 
      bne  LE741    // 
      lda  DE041,X  // 
      cmp  DE047,X  // 
      bcc  LE712    // 
      bne  LE741    // 
      jmp  LE711    // 
LE705: lda  DE04A,X  // 
      sta  DE044,X  // 
      lda  DE047,X  // 
      sta  DE041,X  // 
LE711: rts           // 
LE712: lda  DE035,X  // 
      clc           // 
      adc #$04      // 
      asl           // 
      tay           // 
      jsr  LE52C    // 
      clc           // 
      lda  DE041,X  // 
      adc  DE770    // 
      sta  DE041,X  // 
      lda  DE044,X  // 
      adc  DE771    // 
      sta  DE044,X  // 
      cmp  DE04A,X  // 
      bcs  LE705    // 
      beq  LE738    // 
      rts           // 
LE738: lda  DE041,X  // 
      cmp  DE047,X  // 
      bcs  LE705    // 
      rts           // 
LE741: lda  DE035,X  // 
      clc           // 
      adc #$04      // 
      asl           // 
      tay           // 
      jsr  LE52C    // 
      sec           // 
      lda  DE041,X  // 
      cpx  DE770    // 
      sta  DE041,X  // 
      lda  DE044,X  // 
      cpx  DE771    // 
      sta  DE044,X  // 
      cmp  DE04A,X  // 
      bcc  LE705    // 
      beq  LE767    // 
      rts           // 
LE767: lda  DE041,X  // 
      cmp  DE047,X  // 
      bcc  LE705    // 
      rts           // 
DE770: .byte  $00 // 
DE771: .byte  $00, $00, $00 // 
LE774: lda $FD       // 
      clc           // 
      ldy #$08      // 
      adc ($F7),Y   // 
      sta $FD       // 
      rts           // 
LE77E: lda $FD       // 
      sec           // 
      ldy #$08      // 
      sbc ($F7),Y   // 
      sta $FD       // 
      rts           // 
LE788: lda  DE83D,X  // 
      sta $FD       // 
      lda $D41B     // Oscillator 3 Random Number Generator  MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta  DE840    // 
      lda $D41C     // Envelope Generator 3 Output    MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta  DE841    // 
      ldy #$07      // 
      lda ($F7),Y   // 
      bmi  LE7B6    // 
      cmp #$00      // 
      beq  LE7E9    // 
      cmp #$01      // 
      beq  LE80C    // 
      cmp #$02      // 
      beq  LE7BD    // 
      cmp #$03      // 
      beq  LE7D2    // 
      rts           // 
LE7B0: lda $FD       // 
      sta  DE83D,X  // 
      rts           // 
LE7B6: lda #$00      // 
      sta $FD       // 
      jmp  LE7B0    // 
LE7BD: lda  DE82B,X  // 
      bpl  LE7CC    // 
      lda #$00      // 
      sta  DE82B,X  // 
      sta $FD       // 
      jmp  LE7B0    // 
LE7CC: jsr  LE774    // 
      jmp  LE7B0    // 
LE7D2: lda  DE82B,X  // 
      bpl  LE7E3    // 
      lda #$00      // 
      sta  DE82B,X  // 
      lda #$FF      // 
      sta $FD       // 
      jmp  LE7B0    // 
LE7E3: jsr  LE77E    // 
      jmp  LE7B0    // 
LE7E9: ldy  DE837,X  // 
      bmi  LE7FD    // 
      jsr  LE774    // 
      bcc  LE7B0    // 
      lda #$FF      // 
      sta  DE837,X  // 
      sta $FD       // 
      jmp  LE7B0    // 
LE7FD: jsr  LE77E    // 
      bcs  LE7B0    // 
      lda #$00      // 
      sta  DE837,X  // 
      sta $FD       // 
      jmp  LE7B0    // 
LE80C: lda  DE831,X  // 
      clc           // 
      ldy #$08      // 
      adc ($F7),Y   // 
      sta  DE831,X  // 
      bcc  LE828    // 
      lda $FD       // 
      bne  LE824    // 
      lda #$FF      // 
      sta $FD       // 
      jmp  LE7B0    // 
LE824: lda #$00      // 
      sta $FD       // 
LE828: jmp  LE7B0    // 
DE82B: .byte  $00, $00, $00, $00, $00, $00 // 
DE831: .byte  $00, $00, $00, $00, $00, $00 // 
DE837: .byte  $FF, $00, $00, $00, $00, $00 // 
DE83D: .byte  $B7, $EE, $97 // 
DE840: .byte  $AC // 
DE841: .byte  $5B, $FF, $01, $27, $01, $27, $01, $27 // 
       .byte  $01, $1F, $01, $20, $01, $24, $01, $21 // 
       .byte  $01, $24, $01, $22, $01, $24, $01, $23 // 
       .byte  $01, $24, $03, $08, $01, $01, $29, $01 // 
       .byte  $29, $01, $29, $01, $29, $01, $29, $01 // 
       .byte  $03, $01, $06, $01, $03, $01, $04, $01 // 
       .byte  $08, $01, $03, $01, $06, $01, $03, $01 // 
       .byte  $05, $01, $03, $01, $04, $01, $03, $01 // 
       .byte  $06, $01, $03, $01, $04, $01, $07, $01 // 
       .byte  $03, $01, $06, $01, $03, $01, $05, $01 // 
       .byte  $03, $01, $04, $01, $03, $01, $06, $01 // 
       .byte  $03, $01, $04, $01, $08, $01, $03, $01 // 
       .byte  $06, $01, $03, $01, $05, $01, $03, $01 // 
       .byte  $04, $01, $17, $01, $18, $01, $19, $01 // 
       .byte  $18, $01, $1A, $01, $17, $01, $18, $01 // 
       .byte  $19, $01, $18, $01, $1B, $01, $1B, $01 // 
       .byte  $1C, $01, $03, $01, $06, $01, $03, $01 // 
       .byte  $04, $01, $07, $01, $03, $01, $06, $01 // 
       .byte  $03, $01, $05, $01, $03, $01, $04, $02 // 
       .byte  $00, $01, $00, $01, $01, $03, $00, $02 // 
       .byte  $01, $02, $02, $02, $01, $26, $01, $26 // 
       .byte  $01, $26, $01, $20, $01, $25, $01, $26 // 
       .byte  $01, $26, $01, $21, $01, $25, $01, $26 // 
       .byte  $01, $26, $01, $22, $01, $25, $01, $26 // 
       .byte  $01, $26, $03, $23, $01, $02, $2D, $01 // 
       .byte  $23, $01, $25, $01, $26, $01, $26, $02 // 
       .byte  $06, $01, $23, $01, $28, $01, $10, $01 // 
       .byte  $10, $01, $11, $01, $10, $01, $0D, $01 // 
       .byte  $10, $01, $10, $01, $11, $01, $12, $01 // 
       .byte  $0E, $01, $09, $01, $0D, $01, $0A, $01 // 
       .byte  $0E, $01, $0B, $01, $0D, $01, $0C, $01 // 
       .byte  $0E, $01, $1D, $01, $1D, $01, $1D, $01 // 
       .byte  $1D, $01, $1D, $01, $1D, $01, $1E, $01 // 
       .byte  $13, $01, $15, $01, $15, $01, $14, $01 // 
       .byte  $13, $01, $15, $01, $13, $01, $14, $01 // 
       .byte  $13, $01, $15, $01, $15, $01, $14, $01 // 
       .byte  $13, $01, $15, $01, $13, $01, $16, $02 // 
       .byte  $00, $01, $00, $01, $01, $02, $02, $01 // 
       .byte  $03, $02, $00, $01, $02, $02, $00, $01 // 
       .byte  $00, $00, $01, $00, $00, $01, $00, $00 // 
       .byte  $01, $00, $01, $01, $01, $01, $01, $01 // 
       .byte  $03, $00, $02, $01, $00, $01, $01, $01 // 
       .byte  $00, $01, $01, $01, $02, $01, $03, $01 // 
       .byte  $03, $01, $03, $03, $13, $01, $01, $02 // 
       .byte  $01, $03, $01, $02, $01, $03, $01, $02 // 
       .byte  $01, $02, $01, $02, $01, $02, $02, $00 // 
       .byte  $01, $00, $00, $01, $00, $00, $01, $00 // 
       .byte  $00, $01, $01, $00, $01, $00, $00, $01 // 
       .byte  $01, $00, $01, $00, $00, $01, $01, $00 // 
       .byte  $01, $00, $00, $01, $00, $00, $01, $01 // 
       .byte  $00, $01, $00, $00, $01, $00, $00, $01 // 
       .byte  $00, $00 // 
DE9DB: .byte  $00 // 
DE9DC: .byte  $00, $04, $00, $0E, $00, $1E, $00, $2D // 
       .byte  $00, $37, $00, $46, $00, $63, $00, $80 // 
       .byte  $00, $92, $00, $A2, $00, $B2, $00, $C2 // 
       .byte  $00, $D2, $00, $FE, $00, $13, $01, $25 // 
       .byte  $01, $37, $01, $50, $01, $5B, $01, $66 // 
       .byte  $01, $73, $01, $7B, $01, $85, $01, $89 // 
       .byte  $01, $98, $01, $9D, $01, $C0, $01, $CD // 
       .byte  $01, $D6, $01, $FC, $01, $2C, $02, $2E // 
       .byte  $02, $35, $02, $3C, $02, $43, $02, $4A // 
       .byte  $02, $73, $02, $76, $02, $82, $02, $8E // 
       .byte  $02, $B3, $02, $C8, $02, $00, $00, $07 // 
       .byte  $00, $0F, $00, $4B, $00, $00, $00, $00 // 
       .byte  $00, $08, $00, $0D, $00, $14, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $06, $00, $00 // 
       .byte  $00, $04, $00, $00, $00, $08, $00, $00 // 
       .byte  $00, $00, $00, $04, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $8C, $45, $03, $FF, $80 // 
       .byte  $6A, $03, $82, $48, $03, $80, $6A, $03 // 
       .byte  $FF, $8C, $45, $03, $80, $6A, $03, $82 // 
       .byte  $48, $03, $48, $01, $48, $01, $48, $03 // 
       .byte  $FF, $85, $23, $03, $1A, $03, $21, $03 // 
       .byte  $23, $03, $8E, $46, $03, $40, $03, $FF // 
       .byte  $8F, $40, $0F, $36, $4D, $36, $0D, $30 // 
       .byte  $03, $FF, $8F, $30, $0F, $36, $4D, $36 // 
       .byte  $0D, $85, $23, $0F, $20, $0A, $23, $01 // 
       .byte  $FF, $85, $23, $03, $1A, $03, $8E, $46 // 
       .byte  $03, $40, $03, $8F, $40, $0F, $36, $4D // 
       .byte  $36, $0D, $85, $1A, $03, $23, $03, $26 // 
       .byte  $03, $25, $03, $1A, $03, $FF, $85, $1B // 
       .byte  $03, $16, $03, $8F, $10, $0F, $36, $4D // 
       .byte  $36, $0D, $30, $03, $85, $21, $03, $18 // 
       .byte  $03, $8F, $10, $0F, $36, $4D, $36, $0D // 
       .byte  $30, $03, $FF, $85, $1B, $03, $1A, $03 // 
       .byte  $18, $03, $16, $03, $21, $03, $1B, $03 // 
       .byte  $19, $03, $18, $03, $FF, $83, $43, $08 // 
       .byte  $46, $05, $48, $05, $46, $05, $43, $05 // 
       .byte  $41, $05, $43, $07, $FF, $83, $43, $08 // 
       .byte  $46, $05, $48, $05, $4A, $05, $48, $05 // 
       .byte  $41, $05, $43, $07, $FF, $83, $4A, $08 // 
       .byte  $53, $05, $4A, $05, $53, $05, $51, $05 // 
       .byte  $4A, $05, $4A, $07, $FF, $83, $56, $08 // 
       .byte  $58, $05, $56, $05, $53, $05, $51, $05 // 
       .byte  $53, $05, $53, $07, $FF, $87, $43, $01 // 
       .byte  $40, $01, $43, $01, $40, $01, $3A, $01 // 
       .byte  $30, $01, $3A, $01, $30, $01, $3B, $43 // 
       .byte  $36, $01, $30, $01, $8D, $46, $03, $40 // 
       .byte  $03, $87, $41, $43, $38, $01, $30, $01 // 
       .byte  $8D, $46, $03, $46, $01, $46, $01, $87 // 
       .byte  $FF, $87, $40, $05, $41, $05, $43, $01 // 
       .byte  $40, $01, $43, $01, $40, $01, $8D, $36 // 
       .byte  $03, $30, $06, $41, $05, $FF, $87, $40 // 
       .byte  $05, $41, $05, $43, $01, $40, $01, $43 // 
       .byte  $01, $40, $01, $40, $07, $45, $05, $FF // 
       .byte  $87, $43, $01, $40, $01, $43, $01, $40 // 
       .byte  $01, $3A, $01, $30, $01, $3A, $01, $30 // 
       .byte  $01, $FF, $43, $02, $40, $00, $46, $02 // 
       .byte  $40, $00, $40, $03, $45, $05, $38, $01 // 
       .byte  $30, $01, $38, $01, $30, $01, $38, $01 // 
       .byte  $30, $01, $FF, $43, $01, $40, $01, $43 // 
       .byte  $01, $40, $01, $40, $05, $FF, $8E, $46 // 
       .byte  $03, $40, $03, $8D, $36, $03, $30, $03 // 
       .byte  $FF, $8E, $46, $03, $40, $03, $8D, $36 // 
       .byte  $03, $36, $01, $36, $01, $FF, $30, $05 // 
       .byte  $8D, $36, $03, $30, $03, $FF, $30, $05 // 
       .byte  $8D, $36, $03, $36, $01, $36, $01, $FF // 
       .byte  $85, $26, $03, $FF, $26, $03, $36, $03 // 
       .byte  $26, $03, $28, $03, $2A, $03, $28, $03 // 
       .byte  $26, $03, $FF, $8E, $46, $03, $85, $FF // 
       .byte  $23, $03, $23, $03, $33, $03, $23, $03 // 
       .byte  $26, $03, $28, $03, $26, $03, $23, $03 // 
       .byte  $8E, $46, $03, $85, $23, $03, $33, $03 // 
       .byte  $23, $03, $26, $03, $28, $03, $2A, $03 // 
       .byte  $28, $03, $FF, $21, $03, $21, $03, $31 // 
       .byte  $03, $21, $03, $31, $03, $31, $03, $FF // 
       .byte  $26, $03, $20, $03, $26, $04, $20, $01 // 
       .byte  $FF, $90, $33, $0D, $30, $01, $3A, $00 // 
       .byte  $38, $00, $36, $00, $30, $00, $33, $01 // 
       .byte  $30, $01, $33, $01, $30, $01, $30, $0E // 
       .byte  $31, $03, $33, $01, $30, $01, $33, $02 // 
       .byte  $30, $0F, $36, $03, $30, $0E, $FF, $36 // 
       .byte  $4D, $36, $01, $33, $01, $30, $04, $36 // 
       .byte  $01, $30, $01, $36, $03, $33, $01, $30 // 
       .byte  $04, $33, $01, $30, $01, $38, $03, $33 // 
       .byte  $0A, $30, $0B, $3A, $03, $33, $0A, $30 // 
       .byte  $0B, $38, $03, $33, $0A, $30, $0B, $88 // 
       .byte  $36, $03, $33, $02, $30, $0A, $FF, $83 // 
       .byte  $FF, $43, $05, $46, $05, $48, $45, $FF // 
       .byte  $48, $05, $46, $05, $43, $45, $FF, $43 // 
       .byte  $05, $4A, $05, $48, $45, $FF, $43, $05 // 
       .byte  $41, $05, $43, $45, $FF, $84, $40, $03 // 
       .byte  $43, $01, $43, $01, $41, $01, $41, $01 // 
       .byte  $43, $01, $43, $01, $41, $01, $41, $01 // 
       .byte  $43, $01, $43, $01, $41, $01, $41, $01 // 
       .byte  $43, $01, $43, $01, $41, $01, $41, $01 // 
       .byte  $43, $01, $43, $01, $83, $FF, $40, $05 // 
       .byte  $FF, $8E, $46, $03, $40, $03, $8D, $36 // 
       .byte  $03, $30, $03, $81, $FF, $30, $05, $8F // 
       .byte  $30, $0F, $36, $4D, $36, $0D, $30, $03 // 
       .byte  $FF, $30, $05, $8E, $46, $03, $40, $03 // 
       .byte  $81, $41, $05, $43, $07, $8E, $46, $03 // 
       .byte  $40, $03, $81, $41, $05, $43, $05, $41 // 
       .byte  $05, $43, $05, $41, $05, $43, $05, $41 // 
       .byte  $05, $43, $05, $41, $05, $FF, $8E, $46 // 
       .byte  $01, $84, $41, $01, $43, $01, $43, $01 // 
       .byte  $8D, $36, $01, $84, $41, $01, $43, $01 // 
       .byte  $43, $01, $FF, $81, $FF, $00, $0F, $00 // 
       .byte  $07, $00, $07, $FF, $8A, $43, $47, $43 // 
       .byte  $47, $43, $07, $FF, $89, $4C, $47, $4C // 
       .byte  $07, $53, $43, $53, $02, $50, $00, $48 // 
       .byte  $43, $48, $02, $40, $00, $4A, $47, $4A // 
       .byte  $08, $45, $43, $45, $02, $40, $00, $4C // 
       .byte  $47, $4C, $07, $53, $43, $53, $02, $50 // 
       .byte  $00, $48, $43, $48, $00, $40, $00, $4A // 
       .byte  $41, $4A, $47, $4A, $06, $45, $45, $45 // 
       .byte  $02, $40, $00, $43, $04, $40, $01, $FF // 
       .byte  $8B, $25, $48, $25, $08, $21, $48, $21 // 
       .byte  $08, $1C, $48, $1C, $08, $23, $48, $23 // 
       .byte  $08, $25, $48, $25, $08, $21, $48, $21 // 
       .byte  $08, $1A, $48, $1A, $08, $21, $48, $21 // 
       .byte  $08, $FF, $00, $00, $FF, $9A, $31, $01 // 
       .byte  $31, $01, $2A, $03, $FF, $31, $03, $2A // 
       .byte  $03, $FF, $33, $01, $33, $01, $2A, $03 // 
       .byte  $FF, $33, $03, $2A, $03, $FF, $91, $65 // 
       .byte  $4E, $45, $03, $FF, $91, $76, $4E, $58 // 
       .byte  $01, $FF, $93, $67, $03, $60, $07, $FF // 
       .byte  $92, $46, $03, $40, $07, $FF, $82, $45 // 
       .byte  $03, $FF, $8F, $40, $0F, $41, $03, $FF // 
       .byte  $00, $0F, $96, $21, $4E, $48, $01, $FF // 
       .byte  $8E, $46, $01, $FF, $97, $48, $4E, $31 // 
       .byte  $0D, $8E, $46, $01, $FF, $94, $46, $03 // 
       .byte  $FF, $95, $4A, $03, $FF, $98, $51, $00 // 
       .byte  $50, $00, $FF, $99, $31, $06, $30, $05 // 
       .byte  $FF, $9B, $31, $08, $30, $03, $FF, $80 // 
       .byte  $03, $00, $00, $40, $00, $00, $03, $68 // 
       .byte  $00, $07, $90, $05, $07, $40, $0A, $07 // 
       .byte  $FF, $10, $FE, $00, $03, $49, $08, $47 // 
       .byte  $30, $2F, $07, $80, $07, $00, $00, $00 // 
       .byte  $00, $00, $FF, $00, $03, $37, $27, $00 // 
       .byte  $07, $40, $09, $09, $00, $00, $01, $00 // 
       .byte  $00, $27, $07, $60, $30, $00, $07, $10 // 
       .byte  $43, $00, $00, $00, $02, $00, $FF, $00 // 
       .byte  $00, $07, $07, $00, $07, $40, $09, $45 // 
       .byte  $FF, $10, $00, $00, $03, $07, $04, $60 // 
       .byte  $07, $00, $A0, $40, $09, $00, $00, $00 // 
       .byte  $01, $00, $FF, $00, $06, $07, $45, $00 // 
       .byte  $07, $40, $9A, $06, $F0, $10, $00, $1F // 
       .byte  $02, $4D, $0D, $40, $30, $23, $07, $40 // 
       .byte  $8A, $07, $00, $00, $01, $00, $00, $43 // 
       .byte  $01, $60, $40, $00, $07, $10, $AA, $02 // 
       .byte  $00, $00, $01, $00, $03, $34, $08, $07 // 
       .byte  $60, $00, $07, $80, $0C, $00, $FF, $10 // 
       .byte  $00, $00, $03, $0A, $00, $07, $07, $00 // 
       .byte  $A0, $40, $CB, $00, $0F, $10, $00, $00 // 
       .byte  $00, $35, $09, $50, $20, $00, $07, $10 // 
       .byte  $06, $00, $00, $00, $FE, $00, $03, $70 // 
       .byte  $00, $07, $70, $00, $07, $14, $05, $00 // 
       .byte  $00, $00, $FF, $00, $03, $11, $03, $33 // 
       .byte  $30, $00, $07, $80, $02, $00, $00, $00 // 
       .byte  $01, $00, $03, $70, $00, $07, $70, $00 // 
       .byte  $07, $80, $78, $00, $1F, $20, $00, $00 // 
       .byte  $03, $48, $02, $50, $B7, $0E, $C7, $10 // 
       .byte  $8A, $07, $00, $00, $03, $00, $03, $3F // 
       .byte  $07, $20, $50, $00, $07, $80, $46, $00 // 
       .byte  $00, $00, $FF, $7E, $FF, $00, $00, $07 // 
       .byte  $07, $00, $07, $14, $08, $0B, $00, $00 // 
       .byte  $FD, $00, $00, $60, $00, $07, $40, $00 // 
       .byte  $07, $80, $05, $00, $00, $00, $01, $00 // 
       .byte  $FF, $60, $00, $07, $57, $00, $07, $80 // 
       .byte  $05, $00, $90, $10, $FE, $00, $03, $70 // 
       .byte  $00, $07, $77, $3D, $07, $14, $06, $00 // 
       .byte  $00, $00, $FE, $00, $FF, $70, $00, $07 // 
       .byte  $77, $00, $07, $80, $92, $00, $00, $40 // 
       .byte  $01, $14, $FF, $00, $00, $07, $07, $00 // 
       .byte  $07, $80, $59, $04, $0F, $40, $00, $16 // 
       .byte  $FF, $00, $00, $07, $07, $00, $07, $80 // 
       .byte  $90, $00, $00, $00, $01, $00, $03, $5F // 
       .byte  $00, $07, $B0, $00, $07, $80, $89, $0A // 
       .byte  $00, $00, $03, $00, $02, $5C, $00, $07 // 
       .byte  $F0, $00, $07, $40, $09, $00, $00, $00 // 
       .byte  $FF, $00, $00, $45, $02, $20, $10, $00 // 
       .byte  $07, $80, $0A, $0A, $00, $00, $04, $00 // 
       .byte  $02, $4E, $00, $07, $B0, $00, $07 // 
DEF8B: .byte  $00 // 
DEF8C: .byte  $00, $0E, $00, $1C, $00, $2A, $00, $38 // 
       .byte  $00, $46, $00, $54, $00, $62, $00, $70 // 
       .byte  $00, $7E, $00, $8C, $00, $9A, $00, $A8 // 
       .byte  $00, $B6, $00, $C4, $00, $D2, $00, $E0 // 
       .byte  $00, $EE, $00, $FC, $00, $0A, $01, $18 // 
       .byte  $01, $26, $01, $34, $01, $42, $01, $50 // 
       .byte  $01, $5E, $01, $6C, $01, $7A, $01, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
//init music a= song number (0-2)
init_music:
	   tax           // 
      lda $F018,X   // 
      sta $FF       // 
      lda $FF       // 
      cmp #$06      // 
      beq  LF00D    // 
      rts           // 
LF00D: lda #$00      // 
      sta  DE004    // 
      sta  DE005    // 
      rts           // 
       .byte  $00, $00, $01, $04, $06, $00, $00, $00 // 
}