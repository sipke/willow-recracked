.var brkFile = createFile("vice.txt") 

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}

	.var current_screen =  $5F
	.var hero_sprites_backup = hero_princess_sprites_backup + $400


* = $0801
D801:  .byte  $0B, $08, $00, $00, $9E, $32, $30, $39 // 
       .byte  $36, $00, $00, $00, $03, $C0, $01, $CE // 
       .byte  $20, $00, $59, $90, $01, $32, $48, $02 // 
       .byte  $09, $44, $02, $FC, $24, $07, $80, $24 // 
       .byte  $07, $80, $C4, $09, $87, $02, $08, $F9 // 
       .byte  $02, $10, $09, $12, $10, $11, $52 // 
       
      lda $D011     // VIC Control Register 7 Raster Compare: (Bit 8) 6 Extended Color Text Mode 1 = Enable 5 Bit Map Mode. 1 = Enable 4 Blank Screen to Border Color: 0 = Blank 3 Select 24/25 Row Text Display: 1 = 25 Rows 2-0 Smooth Scroll to Y Dot-Position (0-7)    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ora #$10      // 
      sta $D011     // VIC Control Register 7 Raster Compare: (Bit 8) 6 Extended Color Text Mode 1 = Enable 5 Bit Map Mode. 1 = Enable 4 Blank Screen to Border Color: 0 = Blank 3 Select 24/25 Row Text Display: 1 = 25 Rows 2-0 Smooth Scroll to Y Dot-Position (0-7)    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
	  jsr trainer_code           // 
      //nop
      //nop           // 
      //nop           //      
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      lda #$00      // 
      sta  D43CB    // 
      sta  D43CC    // 
      lda #$EA      // 
      sta $0328     // 
      jmp  L0900    // 
D857:  .byte  $48, $02, $02, $44, $04, $7C, $24, $0F // 
       .byte  $80, $45, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00 // 
D8A0:  .byte  $6A, $95, $96, $99, $99, $96, $95, $6A // 
       .byte  $95, $65, $65, $65, $65, $65, $65, $95 // 
       .byte  $99, $A9, $A9, $99, $99, $99, $99, $55 // 
       .byte  $A9, $99, $95, $95, $95, $99, $A9, $55 // 
       .byte  $99, $A9, $A9, $99, $99, $99, $99, $55 // 
       .byte  $95, $95, $95, $95, $95, $95, $A9, $55 // 
       .byte  $99, $99, $99, $65, $99, $99, $99, $55 // 
       .byte  $99, $99, $99, $65, $99, $99, $99, $55 // 
       .byte  $99, $99, $99, $65, $99, $99, $99, $55 // 
       .byte  $99, $99, $99, $99, $99, $65, $65, $55 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
L0900: jsr  L9770    // 
      jsr  L4384    // 
      lda $DD02     // Data Direction Register - Port A  MOS 6526 Complex Interface Adapter
      ora #$03      // 
      sta $DD02     // Data Direction Register - Port A  MOS 6526 Complex Interface Adapter
      lda $DD00     // Data Port A (Serial Bus, RS-232, VIC Memory  Control) 7 Serial Bus Data Input 6 Serial Bus Clock Pulse Input 5 Serial Bus Data Output 4 Serial Bus Clock Pulse Output 3 Serial Bus ATN Signal Output 2 RS-232 Data Output (User Port) 1-0 VIC Chip System Memory Bank Select  (Default = 11)    MOS 6526 Complex Interface Adapter
      and #$FC      // 
      ora #$02      // 
      sta $DD00     // Data Port A (Serial Bus, RS-232, VIC Memory  Control) 7 Serial Bus Data Input 6 Serial Bus Clock Pulse Input 5 Serial Bus Data Output 4 Serial Bus Clock Pulse Output 3 Serial Bus ATN Signal Output 2 RS-232 Data Output (User Port) 1-0 VIC Chip System Memory Bank Select  (Default = 11)    MOS 6526 Complex Interface Adapter
      lda #$1D      // 
      sta $D018     // VIC Memory Control Register 7-4-2006 Video Matrix Base Address (inside VIC) 3-1-2006 Character Dot-Data Base 0 Select upper/lower Character Set    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda $D011     // VIC Control Register 7 Raster Compare: (Bit 8) 6 Extended Color Text Mode 1 = Enable 5 Bit Map Mode. 1 = Enable 4 Blank Screen to Border Color: 0 = Blank 3 Select 24/25 Row Text Display: 1 = 25 Rows 2-0 Smooth Scroll to Y Dot-Position (0-7)    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ora #$20      // 
      sta $D011     // VIC Control Register 7 Raster Compare: (Bit 8) 6 Extended Color Text Mode 1 = Enable 5 Bit Map Mode. 1 = Enable 4 Blank Screen to Border Color: 0 = Blank 3 Select 24/25 Row Text Display: 1 = 25 Rows 2-0 Smooth Scroll to Y Dot-Position (0-7)    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda $D016     // VIC Control Register 7-6-2006 Unused 5 ALWAYS SET THIS BIT TO 0 ! 4 Multi-Color Mode: 1 = Enable (Text or  Bit-Map) 3 Select 38/40 Column Text Display: 1 = 40 Cols 2-0 Smooth Scroll to X Pos    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ora #$10      // 
      sta $D016     // VIC Control Register 7-6-2006 Unused 5 ALWAYS SET THIS BIT TO 0 ! 4 Multi-Color Mode: 1 = Enable (Text or  Bit-Map) 3 Select 38/40 Column Text Display: 1 = 40 Cols 2-0 Smooth Scroll to X Pos    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
L092D: jmp  L3EA0    // 
L0930: lda #$00      // 
      sta $3F       // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $D01D     // Sprites 0-7 Expand 2x Horizontal (X)    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta  D43CB    // 
      sta  D43CC    // 
      sta $03F0     // 
      sta $02       // 
      ldx #$3F      // 
L0947: sta  D4D00,X  // 
      dex           // 
      bpl  L0947    // 
      ldx #$01      // 
      jsr  L438F    // 
      jsr  L0DB6    // 
      lda #$19      // 
      sta $50       // 
      lda #$05      // 
      sta $40       // 
      lda #$0F      // 
      sta $D418     // Select Filter Mode and Volume 7 Cut-Off Voice 3 Output: 1 = Off, 0 = On   6 Select Filter High-Pass Mode: 1 = On 5 Select Filter Band-Pass Mode: 1 = On 4 Select Filter Low-Pass Mode: 1 = On 3-0 Select Output Volume: 0-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$00      // 
      sta  D9997    // 
      nop           // 
      ldx $3F       // 
      lda  D4070,X  // 
      sta $57       // 
      lda  D4074,X  // 
      sta $58       // 
      jsr  L43CD    // 
      lda #$07      // 
      sta $D027     // Sprite 0 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$00      // 
      sta  D975C    // 
      lda #$21      // 
      sta  D878B    // 
      lda #$4E      // 
      sta  D878C    // 
      jsr  L9874    // 
      jmp  L9443    // 
D991:  .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $A9 // 
       .byte  $00, $EA, $EA, $EA, $EA, $EA, $EA, $A2 // 
       .byte  $01, $85, $02, $20, $8F, $43, $20, $B6 // 
       .byte  $0D, $A9, $0F, $8D, $18, $D4, $60, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00 // 
L09C0: lda  D4087    // 
      sta $57       // 
      lda #$99      // 
      sta $58       // 
      dec  D407E    // 
      bpl  L09E3    // 
      lda #$15      // 
      sta  D407E    // 
      ldy  D407F    // 
      dey           // 
      bpl  L09DB    // 
      ldy #$03      // 
L09DB: sty  D407F    // 
      lda ($57),Y   // 
      sta  D47FA    // 
L09E3: rts           // 
D9E4:  .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $75, $76, $02, $07 // 
       .byte  $60, $79, $9C, $79, $60, $00, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
L0A00: lda current_screen      // 
      tax           // 
      and #$0F      // 
      tay           // 
      lda  D7F40,X  // 
      sta $54       // 
      lda  D92C0,Y  // 
      sta $53       // 
      ldy #$00      // 
L0A12: tya           // 
      and #$07      // 
      tax           // 
      lda  D4200,X  // 
      sta $57       // 
      lda  D4210,Y  // 
      sta $58       // 
      lda  D4240,Y  // 
      sta $59       // 
      sta $2D       // 
      lda  D4270,Y  // 
      sta $5A       // 
      clc           // 
      adc #$94      // 
      sta $2E       // 
      sty $02       // 
      lda #$00      // 
      sta $55       // 
      lda ($53),Y   // 
      tax           // 
      and #$0F      // 
      tay           // 
      lda  D42A0,Y  // 
      sta $51       // 
      sta $A3       // 
      jsr  L0ABA    // 
      sta $52       // 
      sec           // 
      sbc #$3D      // 
      sta $A4       // 
      txa           // 
      lsr           // 
      tay           // 
      ror $55       // 
      lda  D4100,Y  // 
      sta $56       // 
      ldx #$00      // 
      lda #$03      // 
      sta $FF       // 
      sta $FE       // 
L0A60: ldy #$00      // 
L0A62: lda ($55,X)   // 
      sta ($57),Y   // 
      inc $55       // 
      iny           // 
      cpy #$20      // 
      bne  L0A62    // 
      dec $FF       // 
      bmi  L0A81    // 
      clc           // 
      lda $57       // 
      adc #$40      // 
      sta $57       // 
      lda $58       // 
      adc #$01      // 
      sta $58       // 
      jmp  L0A60    // 
L0A81: ldy #$00      // 
L0A83: lda ($51,X)   // 
      sta ($59),Y   // 
      lda ($A3,X)   // 
      sta ($2D),Y   // 
      inc $51       // 
      inc $A3       // 
      iny           // 
      cpy #$04      // 
      bne  L0A83    // 
      dec $FE       // 
      bmi  L0AAF    // 
      clc           // 
      lda $59       // 
      adc #$28      // 
      sta $59       // 
      sta $2D       // 
      lda $5A       // 
      adc #$00      // 
      sta $5A       // 
      clc           // 
      adc #$94      // 
      sta $2E       // 
      jmp  L0A81    // 
L0AAF: ldy $02       // 
      iny           // 
      cpy #$30      // 
      beq  L0AB9    // 
      jmp  L0A12    // 
L0AB9: rts           // 
L0ABA: txa           // 
      lsr           // 
      lsr           // 
      lsr           // 
      lsr           // 
      clc           // 
      adc #$8D      // 
      rts           // 
DAC3:  .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00 // 
L0B00: txa           // 
      asl           // 
      pha           // 
      tay           // 
      clc           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      adc #$01      // 
      pha           // 
      sec           // 
      sbc  L0BA0,X  // 
L0B0F: lsr           // 
      lsr           // 
      lsr           // 
      sta $5B       // 
      iny           // 
      sec           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sbc  DBA8,X   // 
      lsr           // 
      lsr           // 
      lsr           // 
      sta $5C       // 
      lda  DBB0,X   // 
      tax           // 
      jsr  L0C0E    // 
L0B28: lda ($57),Y   // 
      bne  L0B3C    // 
      jsr  L0C00    // 
      dex           // 
      bpl  L0B28    // 
L0B32: pla           // 
      tax           // 
      pla           // 
      tay           // 
      txa           // 
      sta $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      clc           // 
      rts           // 
L0B3C: pla           // 
      pla           // 
      sec           // 
      rts           // 
L0B40: txa           // 
      asl           // 
      pha           // 
      tay           // 
      sec           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sbc #$01      // 
      pha           // 
      sec           // 
      sbc  DBC0,X   // 
      bcc  L0B32    // 
      bcs  L0B0F    // 
      txa           // 
      asl           // 
      tay           // 
      iny           // 
      tya           // 
      pha           // 
      clc           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      adc #$01      // 
      pha           // 
      sec           // 
      sbc  DBC8,X   // 
L0B64: lsr           // 
      lsr           // 
      lsr           // 
      sta $5C       // 
      dey           // 
      sec           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sbc  DBD0,X   // 
      lsr           // 
      lsr           // 
      lsr           // 
      sta $5B       // 
      lda  DBB8,X   // 
      tax           // 
      jsr  L0C0E    // 
L0B7D: lda ($57),Y   // 
      bne  L0B3C    // 
      lda #$01      // 
      jsr  L0C02    // 
      dex           // 
      bpl  L0B7D    // 
      bmi  L0B32    // 
L0B8B: txa           // 
      asl           // 
      tay           // 
      iny           // 
      tya           // 
      pha           // 
      sec           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sbc #$01      // 
      pha           // 
      nop           // 
      sbc  DBD8,X   // 
      bcc  L0B32    // 
      bcs  L0B64    // 
L0BA0: php           // 
      brk           // 
DBA2:  .byte  $00, $08, $00, $08, $08, $00 // 
DBA8:  .byte  $32, $00, $00, $32, $00, $32, $32, $00 // 
DBB0:  .byte  $02, $00, $00, $02, $00, $02, $02, $00 // 
DBB8:  .byte  $01, $00, $00, $01, $00, $01, $01, $00 // 
DBC0:  .byte  $10, $00, $00, $10, $00, $10, $10, $00 // 
DBC8:  .byte  $1D, $00, $00, $1A, $00, $1D, $1D, $00 // 
DBD0:  .byte  $10, $00, $00, $10, $00, $10, $10, $00 // 
DBD8:  .byte  $32, $00, $00, $32, $00, $32, $32, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $08, $04, $02, $01 // 
DBF4:  .byte  $0D, $0A, $32, $2D // 
DBF8:  .byte  $00, $40 // 
DBFA:  .byte  $53, $8B, $00, $00 // 
DBFE:  .byte  $01, $FF // 
L0C00: lda #$28      // 
L0C02: clc           // 
      adc $57       // 
      sta $57       // 
      lda $58       // 
      adc #$00      // 
      sta $58       // 
      rts           // 
L0C0E: lda #$00      // 
      sta $57       // 
      lda #$44      // 
      sta $58       // 
      ldy $5C       // 
      beq  L0C20    // 
L0C1A: jsr  L0C00    // 
      dey           // 
      bne  L0C1A    // 
L0C20: lda $5B       // 
      jmp  L0C02    // 
L0C25: ldx $50       // 
L0C27: ldy #$FF      // 
L0C29: dey           // 
      bne  L0C29    // 
      dex           // 
      bne  L0C27    // 
      rts           // 
L0C30: lda $D01E     // Sprite to Sprite Collision Detect  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lsr           // 
      bcs  L0C37    // 
      rts           // 
L0C37: pla           // 
      pla           // 
      jmp  L0C98    // 
L0C3C: lda #$03      // 
      sta $FF       // 
L0C40: ldx #$FF      // 
L0C42: txa           // 
      and #$0F      // 
      sta $D020     // Border Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ldy #$FF      // 
L0C4A: dey           // 
      bne  L0C4A    // 
      dex           // 
      bne  L0C42    // 
      dec $FF       // 
      bne  L0C40    // 
      stx $D020     // Border Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      rts           // 
DC58:  .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
L0C98: lda $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      clc           // 
      adc #$06      // 
      cmp #$FA      // 
      bcs  L0CB3    // 
      sta $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ldx #$30      // 
      jsr  L0C27    // 
      jsr  L9575    // 
      jmp  L0C98    // 
DCB0:  .byte  $EA, $EA, $EA // 
L0CB3: lda #$1D      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jsr  L0C3C    // 
      lda  D43F4    // 
      sta $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ldx #$F6      // 
      txs           // 
      dec $40       // 
      jsr  L43CD    // 
      beq  L0CCE    // 
      jmp  L980C    // 
L0CCE: jmp  L3EA0    // 
DCD1:  .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00 // 
L0D00: ldx #$03      // 
L0D02: ldy #$01      // 
L0D04: lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp  D4130,X  // 
      beq  L0D14    // 
      dex           // 
      dey           // 
      bpl  L0D04    // 
      txa           // 
      bpl  L0D02    // 
      rts           // 
L0D14: txa           // 
      pha           // 
      ldx #$02      // 
L0D18: lda  D4098,X  // 
      sta  D409C,X  // 
      dex           // 
      bpl  L0D18    // 
      pla           // 
      tax           // 
      lda  D4134,X  // 
      sta $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda  D4140,Y  // 
      sta $02       // 
      lda  D4138,X  // 
      sta  L0D40    // 
      lda  D413C,X  // 
      sta  L0D3A    // 
L0D3A: sec           // 
      lda current_screen      // 
      sta  D409E    // 
L0D40: sbc $02       // 
      sta current_screen      // 
      ldx #$01      // 
L0D46: lda $D000,X   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta  D4098,X  // 
      dex           // 
      bpl  L0D46    // 
      lda #$8C      // 
      sta $48       // 
L0D53: lda #$00      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jsr  L0A00    // 
      lda #$00      // 
      sta $24       // 
      lda #$01      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jsr  L9300    // 
      rts           // 
DD68:  .byte  $00 // 
L0D69: lda current_screen      // 
      bne  L0D9B    // 
      lda  D975C    // 
      bmi  L0D9B    // 
      ldx #$03      // 
      jsr  L9900    // 
      bcc  L0D9B    // 
      lda #$FF      // 
      sta  D975C    // 
      lda #$00      // 
      sta  D878B    // 
      sta  D878C    // 
      ldx #$80      // 
      stx  D9997    // 
      dex           // 
      stx $48       // 
      ldx #$03      // 
      jsr  L0DAA    // 
      jsr  L9870    // 
      pla           // 
      pla           // 
      jmp  L9490    // 
L0D9B: rts           // 
DD9C:  .byte  $00, $00, $00, $00 // 
L0DA0: lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ora  DDF8,X   // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      rts           // 
L0DAA: lda  DDF8,X   // 
      eor #$FF      // 
      and $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      rts           // 
L0DB6: ldx #$3F      // 
L0DB8: lda  D93C0,X  // 
      sta  D42C0,X  // 
      dex           // 
      bpl  L0DB8    // 
      rts           // 
L0DC2: lda #$0A      // 
      ldx $60       // 
      beq  L0DCA    // 
      lda #$F4      // 
L0DCA: clc           // 
      adc $D004     // Sprite 2 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $D00C     // Sprite 6 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda $D005     // Sprite 2 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $D00D     // Sprite 6 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      rts           // 
DDD8:  .byte  $A9, $00, $8D, $6F, $43, $20, $44, $43 // 
       .byte  $A2, $04, $20, $40, $9D, $60 // 
L0DE6: ldx #$02      // 
L0DE8: lda  D409C,X  // 
      sta  D4098,X  // 
      dex           // 
      bpl  L0DE8    // 
      rts           // 
DDF2:  .byte  $00, $00, $00, $00, $00, $00 // 
DDF8:  .byte  $01, $02, $04, $08, $10, $20, $40, $80 // 
      jsr  L0F66    // 
      dec  DED5     // 
      beq  L0E0B    // 
      jmp $EA31     // 
L0E0B: lda #$10      // 
      sta  DED5     // 
      lda $FA       // 
      sta  DED6     // 
      lda $FB       // 
      sta  DED7     // 
L0E1A: lda  DECD     // 
      sta $FA       // 
      lda  DECE     // 
      sta $FB       // 
      ldy #$00      // 
      lda ($FA),Y   // 
      cmp #$01      // 
      bne  L0E3A    // 
      lda  DED8     // 
      sta  DECD     // 
      lda  DED9     // 
      sta  DECE     // 
      bne  L0E1A    // 
L0E3A: lda ($FA),Y   // 
      beq  L0E57    // 
      sta  DECF     // 
      sta $D400     // Voice 1: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      iny           // 
      lda ($FA),Y   // 
      sta  DED0     // 
      sta $D401     // Voice 1: Frequency Control - High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$40      // 
      sta $D404     // Voice 1: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 1 with Osc. 3 Output,  1 = On 1 Synchronize Osc. 1 with Osc. 3 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$41      // 
      sta $D404     // Voice 1: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 1 with Osc. 3 Output,  1 = On 1 Synchronize Osc. 1 with Osc. 3 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
L0E57: ldy #$00      // 
      clc           // 
      lda $FA       // 
      adc #$02      // 
      sta $FA       // 
      lda $FB       // 
      adc #$00      // 
      sta $FB       // 
      lda ($FA),Y   // 
      beq  L0E83    // 
      sta  DED1     // 
      sta $D407     // Voice 2: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      iny           // 
      lda ($FA),Y   // 
      sta  DED2     // 
      sta $D408     // Voice 2: Frequency Control - High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$40      // 
      sta $D40B     // Voice 2: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 2 with Osc. 1 Output,  1 = On 1 Synchronize Osc. 2 with Osc. 1 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$41      // 
      sta $D40B     // Voice 2: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 2 with Osc. 1 Output,  1 = On 1 Synchronize Osc. 2 with Osc. 1 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
L0E83: ldy #$00      // 
      clc           // 
      lda $FA       // 
      adc #$02      // 
      sta $FA       // 
      lda $FB       // 
      adc #$00      // 
      sta $FB       // 
      lda ($FA),Y   // 
      beq  L0EAF    // 
      sta  DED3     // 
      sta $D40E     // Voice 3: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      iny           // 
      lda ($FA),Y   // 
      sta  DED4     // 
      sta $D40F     // Voice 3: Frequency Control - High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$40      // 
      sta $D412     // Voice 3: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 2 with Osc. 1 Output,  1 = On 1 Synchronize Osc. 2 with Osc. 1 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$41      // 
      sta $D412     // Voice 3: Control Register 7 Select Random Noise Waveform, 1 = On 6 Select Pulse Waveform, 1 = On 5 Select Sawtooth Waveform, 1 = On 4 Select Triangle Waveform, 1 = On 3 Test Bit: 1 = Disable Oscillator 1 2 Ring Modulate Osc. 2 with Osc. 1 Output,  1 = On 1 Synchronize Osc. 2 with Osc. 1 Frequency,  1 = On 0 Gate Bit: 1 = Start Att/Dec/Sus,  0 = Start Release    MOS 6581 SOUND INTERFACE DEVICE (SID)
L0EAF: lda  DECD     // 
      clc           // 
      adc #$06      // 
      sta  DECD     // 
      lda  DECE     // 
      adc #$00      // 
      sta  DECE     // 
      lda  DED6     // 
      sta $FA       // 
      lda  DED7     // 
      sta $FB       // 
      jmp $EA31     // 
DECD:  .byte  $CC // 
DECE:  .byte  $B0 // 
DECF:  .byte  $4E // 
DED0:  .byte  $0D // 
DED1:  .byte  $EE // 
DED2:  .byte  $0B // 
DED3:  .byte  $DF // 
DED4:  .byte  $1D // 
DED5:  .byte  $07 // 
DED6:  .byte  $00 // 
DED7:  .byte  $A0 // 
DED8:  .byte  $00 // 
DED9:  .byte  $B0 // 
L0EDA: sei           // 
      ldx #$17      // 
      lda #$00      // 
L0EDF: sta $D400,X   // Voice 1: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      dex           // 
      bpl  L0EDF    // 
      lda #$0F      // 
      sta $D418     // Select Filter Mode and Volume 7 Cut-Off Voice 3 Output: 1 = Off, 0 = On   6 Select Filter High-Pass Mode: 1 = On 5 Select Filter Band-Pass Mode: 1 = On 4 Select Filter Low-Pass Mode: 1 = On 3-0 Select Output Volume: 0-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$03      // 
      sta $D403     // Voice 1: Pulse Waveform Width - High-Byte 
      sta $D40A     // Voice 2: Pulse Waveform Width - High-Byte 
      sta $D411     // Voice 3: Pulse Waveform Width - High-Byte 
      lda #$09      // 
      sta $D405     // Envelope Generator 1: Attack / Decay Cycle  Control  MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D40C     // Envelope Generator 2: Attack / Decay Cycle  Control 7-4-2006 Select Attack Cycle Duration: O-15 3-0 Select Decay Cycle Duration: 0-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D413     // Envelope Generator 2: Attack / Decay Cycle  Control 7-4-2006 Select Attack Cycle Duration: O-15 3-0 Select Decay Cycle Duration: 0-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$20      // 
      sta $D406     // Envelope Generator 1: Sustain / Release Cycle  Control  MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D40D     // Envelope Generator 2: Sustain / Release Cycle  Control 7-4-2006 Select Sustain Cycle Duration: O-15 3-0 Select Release Cycle Duration: O-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      sta $D414     // Envelope Generator 3: Sustain / Release Cycle  Control 7-4-2006 Select Sustain Cycle Duration: 0-15 3-0 Select Release Cycle Duration: 0-15      MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda #$00      // 
      sta  DED8     // 
      sta  DECD     // 
      lda #$B0      // 
      sta  DED9     // 
      sta  DECE     // 
      lda #$10      // 
      sta  DED5     // 
      sta  L0E0B+1  // 
      lda  DF4E     // 
      cmp #$02      // 
      bne  L0F42    // 
      lda #$B2      // 
      sta  DED8     // 
      sta  DECD     // 
      lda #$B1      // 
      sta  DED9     // 
      sta  DECE     // 
      lda #$0C      // 
      sta  DED5     // 
      sta  L0E0B+1  // 
L0F42: lda #$00      // 
      sta $0314     // 
      lda #$0E      // 
      sta $0315     // 
      cli           // 
      rts           // 
DF4E:  .byte  $01, $78, $A2, $17, $A9, $00, $9D, $00 // 
       .byte  $D4, $CA, $10, $FA, $A9, $31, $8D, $14 // 
       .byte  $03, $A9, $EA, $8D, $15, $03, $58, $60 // 
L0F66: clc           // 
      lda  DECF     // 
      adc  DFC7     // 
      sta $D400     // Voice 1: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DED0     // 
      adc #$00      // 
      sta $D401     // Voice 1: Frequency Control - High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      clc           // 
      lda  DED1     // 
      adc  DFC7     // 
      sta $D407     // Voice 2: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DED2     // 
      adc #$00      // 
      sta $D408     // Voice 2: Frequency Control - High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      clc           // 
      lda  DED3     // 
      adc  DFC7     // 
      sta $D40E     // Voice 3: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DED4     // 
      adc #$00      // 
      sta $D40F     // Voice 3: Frequency Control - High-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      lda  DFC8     // 
      bpl  L0FB4    // 
      lda  DFC7     // 
      sec           // 
      sbc #$10      // 
      sta  DFC7     // 
      cmp #$00      // 
      bne  L0FB3    // 
      lda #$00      // 
      sta  DFC8     // 
L0FB3: rts           // 
L0FB4: lda  DFC7     // 
      clc           // 
      adc #$10      // 
      sta  DFC7     // 
      cmp #$40      // 
      bne  L0FC6    // 
      lda #$80      // 
      sta  DFC8     // 
L0FC6: rts           // 
DFC7:  .byte  $00 // 
DFC8:  .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $22, $23, $20, $21, $26, $27, $24, $25 // 
       .byte  $2A, $2B, $28, $29, $00, $00, $00, $00 // 
DFF0:  .byte  $E0, $E4, $E8, $EC // 
DFF4:  .byte  $10, $14, $18, $1C, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $56, $56, $56 // 
       .byte  $56, $55, $55, $55, $D5, $DF, $DF, $97 // 
       .byte  $65, $65, $59, $59, $55, $5F, $5F, $5F // 
       .byte  $99, $67, $95, $95, $FF, $FF, $FF, $FF // 
       .byte  $7F, $5F, $5F, $FF, $AA, $AB, $AF, $AF // 
       .byte  $AB, $AA, $AA, $AE, $97, $A5, $6A, $6E // 
       .byte  $BB, $DA, $56, $9A, $AB, $ED, $EE, $BA // 
       .byte  $BA, $EF, $EE, $EE, $7F, $5F, $5F, $FF // 
       .byte  $AF, $DB, $D7, $D7, $95, $9D, $97, $95 // 
       .byte  $A5, $A9, $AA, $AA, $7D, $D7, $95, $A5 // 
       .byte  $A5, $95, $55, $55, $BA, $BA, $FA, $FA // 
       .byte  $FA, $FA, $BE, $BE, $F7, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $EA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $BF, $FF, $EB, $AB, $EB, $AB // 
       .byte  $EB, $AB, $AE, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $AF, $FF, $55, $55, $56 // 
       .byte  $56, $55, $56, $59, $FF, $66, $99, $66 // 
       .byte  $9A, $65, $99, $A6, $FF, $BD, $F9, $EE // 
       .byte  $7B, $FE, $96, $69, $DF, $7F, $57, $B7 // 
       .byte  $A5, $69, $D9, $F9, $AB, $AF, $AB, $BA // 
       .byte  $AB, $AE, $AA, $BB, $66, $AA, $79, $77 // 
       .byte  $6F, $5B, $6B, $9B, $AA, $EA, $F9, $5D // 
       .byte  $56, $A7, $A7, $A7, $57, $5F, $D7, $D7 // 
       .byte  $D7, $AA, $EE, $BB, $5D, $57, $5D, $57 // 
       .byte  $5D, $57, $55, $57, $97, $67, $97, $57 // 
       .byte  $97, $57, $57, $57, $5B, $5B, $5B, $5B // 
       .byte  $5B, $5B, $5A, $AB, $DD, $77, $DD, $77 // 
       .byte  $DD, $77, $6A, $AA, $AA, $AA, $AA, $AA // 
       .byte  $A9, $A5, $9F, $FF, $AF, $AB, $AF, $BF // 
       .byte  $FD, $55, $55, $56, $A5, $55, $55, $5F // 
       .byte  $FF, $FA, $EA, $AA, $55, $55, $A5, $BA // 
       .byte  $FE, $FF, $FF, $FF, $FF, $55, $55, $55 // 
       .byte  $55, $95, $55, $65, $FF, $AA, $A9, $A9 // 
       .byte  $A5, $A5, $96, $96, $FF, $FF, $BF, $BF // 
       .byte  $AF, $AF, $EB, $EB, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FB, $D7, $F5, $FD, $FF // 
       .byte  $FF, $AA, $EE, $BB, $55, $AA, $AA, $AA // 
       .byte  $AB, $AF, $AF, $AF, $55, $AA, $AA, $AA // 
       .byte  $EA, $FA, $FA, $FA, $EB, $AF, $BF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $EE, $BB, $EE, $BB // 
       .byte  $EE, $BB, $55, $55, $A5, $A5, $A5, $A5 // 
       .byte  $A5, $A5, $A5, $E5, $FA, $FA, $FA, $FA // 
       .byte  $FA, $FA, $FA, $FA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $55 // 
       .byte  $55, $A9, $AA, $AA, $FE, $FF, $FF, $57 // 
       .byte  $55, $55, $A9, $AA, $5A, $DA, $FE, $FF // 
       .byte  $AF, $AB, $AA, $56, $FF, $FF, $FF, $FF // 
       .byte  $BF, $AF, $6A, $5A, $7F, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $AA, $55, $55, $55 // 
       .byte  $55, $55, $5D, $77, $55, $59, $67, $75 // 
       .byte  $9D, $97, $B5, $5F, $FF, $FF, $BF, $AB // 
       .byte  $FA, $BF, $EF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AB, $AE, $B9, $BB, $BB, $EB // 
       .byte  $AE, $6A, $9A, $B6, $AA, $EA, $EA, $BB // 
       .byte  $AE, $AD, $AB, $AB, $6A, $AA, $6A, $9A // 
       .byte  $A6, $A9, $5A, $A6, $A6, $A6, $A6, $9A // 
       .byte  $A9, $AA, $AA, $AA, $97, $9B, $99, $99 // 
       .byte  $59, $65, $55, $55, $56, $56, $95, $A5 // 
       .byte  $69, $5A, $5A, $56, $A9, $DA, $E6, $E9 // 
       .byte  $EA, $EA, $AA, $EA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $BF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $A9, $AF, $FF, $FF, $57, $57, $5F, $7F // 
       .byte  $FE, $AA, $A9, $A5, $6A, $6A, $6A, $6A // 
       .byte  $FA, $BE, $AF, $AB, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AB, $AA, $EE, $FF, $FF, $FF, $FF // 
       .byte  $FB, $FF, $EE, $BA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $BF, $FF, $EF, $55, $57, $55, $5F // 
       .byte  $5D, $55, $7F, $57, $57, $F5, $57, $75 // 
       .byte  $5D, $D5, $7D, $77, $EA, $FE, $AA, $EE // 
       .byte  $FB, $EA, $EE, $AB, $AA, $EE, $AA, $FA // 
       .byte  $A8, $28, $C0, $A0, $75, $D7, $75, $5F // 
       .byte  $D7, $75, $DF, $7D, $AA, $96, $99, $66 // 
       .byte  $9A, $AA, $65, $9A, $77, $FD, $DF, $7F // 
       .byte  $DD, $7F, $DE, $7E, $5F, $6F, $7F, $BF // 
       .byte  $FF, $FF, $FF, $FF, $FB, $FE, $BB, $EE // 
       .byte  $FB, $BE, $EE, $BF, $59, $66, $96, $9A // 
       .byte  $56, $9A, $5B, $9A, $A5, $25, $80, $AF // 
       .byte  $EF, $AF, $AF, $AF, $55, $55, $00, $AB // 
       .byte  $AB, $AB, $AB, $AB, $55, $55, $55, $55 // 
       .byte  $55, $55, $5D, $57, $AA, $AA, $AA, $AA // 
       .byte  $BA, $AB, $AA, $BA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $EF, $FE, $BF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $BA, $AB, $EA, $AE // 
       .byte  $EA, $B3, $0A, $2F, $AB, $EE, $EF, $BB // 
       .byte  $9B, $AF, $FE, $BB, $EB, $DF, $BD, $EB // 
       .byte  $EF, $7D, $DF, $79, $EA, $AA, $EA, $AA // 
       .byte  $EA, $DA, $A6, $BA, $D5, $DA, $F9, $F6 // 
       .byte  $FD, $FD, $FF, $FF, $7D, $D7, $7D, $F7 // 
       .byte  $57, $FF, $57, $7D, $F7, $7B, $F7, $DD // 
       .byte  $7B, $FD, $7F, $7F, $AA, $6E, $BA, $D9 // 
       .byte  $AE, $DA, $6B, $BE, $55, $55, $00, $AB // 
       .byte  $AB, $AB, $AB, $AB, $EA, $E2, $0A, $6A // 
       .byte  $6A, $6A, $6A, $6A, $EF, $BE, $BB, $AF // 
       .byte  $0A, $2A, $8E, $8B, $B6, $6D, $BB, $6E // 
       .byte  $FF, $BB, $FF, $CF, $5A, $69, $AA, $6A // 
       .byte  $6A, $A6, $66, $59, $55, $77, $55, $75 // 
       .byte  $55, $D5, $5A, $AA, $5F, $DF, $FF, $7F // 
       .byte  $5A, $AA, $AA, $AA, $AB, $AB, $FF, $FF // 
       .byte  $55, $55, $55, $55, $9A, $AA, $6A, $6A // 
       .byte  $50, $50, $17, $77, $FF, $FF, $FF, $FF // 
       .byte  $00, $00, $A9, $A5, $FF, $FF, $FF, $FF // 
       .byte  $00, $00, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $00, $00, $95, $A5, $AB, $6B, $97, $5B // 
       .byte  $9B, $A7, $5B, $57, $FA, $FA, $FA, $FA // 
       .byte  $FA, $FA, $FA, $FA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FA, $FA, $FA, $FA // 
       .byte  $FA, $FA, $FA, $FA, $AB, $A5, $A5, $95 // 
       .byte  $9D, $97, $5D, $AA, $55, $A9, $A9, $AA // 
       .byte  $66, $99, $66, $FF, $55, $55, $55, $AA // 
       .byte  $66, $99, $66, $FF, $AA, $95, $95, $55 // 
       .byte  $95, $55, $56, $FF, $57, $57, $FF, $FF // 
       .byte  $AA, $AA, $AA, $AA, $D5, $DF, $FD, $F5 // 
       .byte  $95, $A9, $AA, $AA, $51, $73, $51, $50 // 
       .byte  $74, $51, $90, $A8, $BF, $FB, $3F, $BF // 
       .byte  $EF, $FF, $FF, $FD, $FF, $FF, $FF, $FF // 
       .byte  $00, $00, $A5, $95, $FF, $FF, $FF, $FF // 
       .byte  $00, $00, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $00, $00, $55, $95, $EF, $FE, $FE, $FA // 
       .byte  $2A, $2A, $AA, $AA, $EA, $EA, $EA, $EA // 
       .byte  $EA, $EA, $EA, $EA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $BF, $BF, $BF, $B5 // 
       .byte  $9D, $B7, $9D, $77, $AA, $AA, $AA, $AA // 
       .byte  $EA, $EA, $BA, $EA, $55, $A5, $A5, $AA // 
       .byte  $99, $66, $99, $FF, $56, $55, $56, $FE // 
       .byte  $DA, $6A, $AA, $AA, $BB, $F1, $77, $DC // 
       .byte  $FF, $B1, $FF, $AB, $BF, $2F, $7B, $AF // 
       .byte  $4B, $2F, $9B, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FE, $FB, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $EB, $A9, $AA, $BA, $BA // 
       .byte  $BA, $FE, $FE, $FF, $FB, $EE, $BA, $EB // 
       .byte  $EA, $BA, $EE, $F7, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $59, $95, $A5, $AB // 
       .byte  $AB, $AB, $A7, $A7, $AA, $AA, $AA, $55 // 
       .byte  $04, $04, $04, $55, $BE, $FA, $EA, $55 // 
       .byte  $10, $10, $1A, $55, $AA, $AA, $AA, $59 // 
       .byte  $95, $A5, $A5, $AB, $A7, $95, $55, $55 // 
       .byte  $55, $55, $55, $FF, $FF, $FF, $7F, $5D // 
       .byte  $55, $57, $57, $FF, $5F, $57, $57, $97 // 
       .byte  $5F, $57, $5F, $5F, $A9, $A9, $A9, $A9 // 
       .byte  $A9, $A9, $A9, $A9, $33, $33, $FF, $00 // 
       .byte  $FF, $00, $00, $00, $30, $30, $F0, $30 // 
       .byte  $F0, $30, $30, $FC, $33, $0B, $33, $0F // 
       .byte  $32, $CF, $F3, $3E, $AB, $AB, $AB, $AB // 
       .byte  $AB, $AF, $AC, $AC, $02, $02, $02, $02 // 
       .byte  $02, $AA, $02, $02, $57, $57, $57, $57 // 
       .byte  $57, $57, $57, $57, $0F, $3B, $CE, $3F // 
       .byte  $FC, $0B, $33, $03, $40, $45, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $00, $55, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $00, $FF, $55, $55 // 
       .byte  $55, $55, $5A, $AA, $00, $FF, $56, $5A // 
       .byte  $6A, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $57, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $F5 // 
       .byte  $5F, $7F, $DF, $F7, $A9, $A5, $55, $A5 // 
       .byte  $99, $99, $69, $69, $AA, $5A, $A5, $9A // 
       .byte  $96, $96, $A6, $5A, $AA, $AA, $AA, $5A // 
       .byte  $A5, $6A, $59, $59, $D6, $D6, $D9, $D9 // 
       .byte  $E5, $E5, $95, $9A, $A9, $A9, $69, $99 // 
       .byte  $A5, $A5, $5F, $FF, $A5, $9A, $96, $96 // 
       .byte  $A6, $5A, $F5, $FF, $99, $5A, $A5, $6A // 
       .byte  $59, $59, $99, $5A, $FA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $A5, $FF, $FF, $FF, $FF // 
       .byte  $FD, $D5, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FB, $7F, $5F, $57, $F5, $FF, $FB, $BF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $55, $55 // 
       .byte  $55, $55, $D5, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $57, $5D, $FF, $FF, $FF, $FF // 
       .byte  $BF, $BF, $AF, $BB, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $69, $66 // 
       .byte  $5A, $66, $69, $6A, $99, $69, $A6, $A6 // 
       .byte  $A6, $9A, $9A, $5A, $69, $6A, $66, $6A // 
       .byte  $5A, $66, $69, $6A, $FF, $7D, $DD, $F5 // 
       .byte  $7D, $FD, $F5, $7D, $6A, $69, $69, $66 // 
       .byte  $66, $5A, $59, $57, $66, $A9, $A9, $A7 // 
       .byte  $9F, $7F, $FF, $FF, $66, $6A, $DA, $F6 // 
       .byte  $FD, $FF, $FF, $FF, $EE, $FA, $BE, $FE // 
       .byte  $FA, $BE, $6E, $5A, $5A, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $FE, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AE, $AA, $AA, $AA, $A6, $6A // 
       .byte  $AA, $AF, $BF, $FF, $FF, $FF, $FF, $EF // 
       .byte  $AF, $BF, $FF, $FF, $FF, $FF, $FF, $FA // 
       .byte  $FA, $F7, $FD, $FE, $FF, $EB, $EB, $F7 // 
       .byte  $AF, $AD, $F7, $9A, $FF, $FF, $FF, $AF // 
       .byte  $AF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AB, $AA, $BD, $BE // 
       .byte  $AA, $AA, $AB, $AB, $75, $EE, $AE, $EF // 
       .byte  $FA, $BE, $6E, $6F, $BE, $7E, $AA, $AA // 
       .byte  $FA, $FA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AB, $BF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AB, $A9, $A9, $AA, $AA // 
       .byte  $AB, $AF, $FF, $FF, $AB, $6F, $7F, $FF // 
       .byte  $FF, $FF, $FF, $FF, $EF, $FF, $FF, $FF // 
       .byte  $EF, $FF, $FF, $FF, $AF, $AA, $AA, $AA // 
       .byte  $AA, $A9, $AA, $AB, $FF, $5F, $D7, $F5 // 
       .byte  $BD, $FF, $D5, $55, $BF, $FB, $FE, $FF // 
       .byte  $7F, $5F, $57, $55, $AA, $AA, $AA, $AA // 
       .byte  $6A, $AA, $AA, $EA, $55, $55, $55, $55 // 
       .byte  $55, $57, $5D, $7D, $55, $55, $55, $5D // 
       .byte  $D1, $77, $D5, $5D, $FF, $FF, $FF, $BF // 
       .byte  $EF, $CF, $BB, $3F, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $A8, $8E, $AB, $AF // 
       .byte  $BE, $FB, $F6, $FE, $8E, $BB, $EE, $B2 // 
       .byte  $BE, $3B, $EF, $7A, $FB, $EE, $3B, $E2 // 
       .byte  $FE, $BB, $EB, $2E, $FF, $FF, $FF, $BF // 
       .byte  $3F, $EF, $FF, $BF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $7F, $AF, $7B, $F5, $DD // 
       .byte  $FF, $FD, $FF, $FF, $FA, $AE, $F3, $7E // 
       .byte  $9E, $57, $DE, $D5, $3B, $BF, $EF, $BB // 
       .byte  $8F, $AF, $AF, $BB, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FE, $FF, $FF, $FF, $BF, $FF, $FF // 
       .byte  $FF, $FF, $FD, $D5, $FE, $FF, $FF, $FF // 
       .byte  $FF, $D7, $55, $55, $6A, $9A, $96, $AA // 
       .byte  $AA, $AA, $EA, $FF, $57, $55, $55, $55 // 
       .byte  $55, $D6, $7A, $5F, $55, $D5, $7F, $6A // 
       .byte  $AA, $AA, $AA, $FF, $FF, $FF, $AA, $55 // 
       .byte  $55, $55, $55, $AA, $FF, $FF, $AA, $55 // 
       .byte  $55, $55, $55, $AA, $AB, $AB, $AB, $AB // 
       .byte  $AB, $AB, $AB, $AB, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $C0, $C0, $C0, $C0 // 
       .byte  $C0, $C0, $C0, $C0, $0C, $0C, $0C, $0C // 
       .byte  $0C, $0C, $0C, $0C, $AB, $AB, $AF, $AF // 
       .byte  $AA, $AF, $AF, $AC, $00, $00, $FF, $FF // 
       .byte  $AA, $FF, $FF, $00, $40, $40, $55, $55 // 
       .byte  $AA, $55, $55, $00, $08, $08, $AA, $AA // 
       .byte  $FF, $AA, $AA, $00, $5F, $5F, $FF, $FF // 
       .byte  $FF, $FF, $FD, $55, $FF, $FF, $FF, $FF // 
       .byte  $F5, $D5, $55, $55, $FF, $FF, $FF, $57 // 
       .byte  $55, $55, $55, $55, $A0, $A8, $AA, $AA // 
       .byte  $EA, $EA, $FE, $FF, $55, $55, $FF, $AA // 
       .byte  $AA, $AA, $AA, $FF, $55, $55, $FF, $AA // 
       .byte  $AA, $AA, $AA, $FF, $55, $57, $FD, $A9 // 
       .byte  $AA, $AA, $AA, $FF, $BF, $FF, $FF, $FF // 
       .byte  $FF, $7E, $5B, $AF, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $20, $20, $20, $20 // 
       .byte  $20, $20, $20, $20, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $BF, $BF, $BF, $BF // 
       .byte  $BF, $BF, $BF, $BF, $00, $00, $FF, $FF // 
       .byte  $AA, $FF, $FF, $00, $20, $20, $AA, $AA // 
       .byte  $FF, $AA, $AA, $00, $00, $00, $AA, $AA // 
       .byte  $FF, $AA, $AA, $00, $BF, $BF, $AF, $AF // 
       .byte  $EF, $AF, $AF, $2F, $3F, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $55, $CF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $F5, $55, $00, $FF, $FF, $FF // 
       .byte  $FF, $FF, $7F, $55, $1A, $1A, $AA, $AA // 
       .byte  $AA, $AA, $BA, $FA, $FF, $FF, $FE, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $55, $F5 // 
       .byte  $FF, $7D, $77, $DD, $FF, $FF, $FF, $FA // 
       .byte  $AA, $BB, $EB, $BA, $FF, $FF, $BF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AB, $EF, $FF, $BF // 
       .byte  $AF, $A8, $A8, $A8, $AE, $BB, $EE, $AA // 
       .byte  $AA, $00, $DD, $DD, $EE, $BB, $EE, $AA // 
       .byte  $AA, $00, $DF, $DF, $BF, $AE, $AA, $AB // 
       .byte  $AF, $3F, $3F, $3F, $A8, $A8, $A8, $A8 // 
       .byte  $A8, $A8, $A8, $A8, $EE, $EE, $EE, $EE // 
       .byte  $EE, $EE, $EE, $EE, $EF, $EF, $E0, $EF // 
       .byte  $EF, $EF, $EF, $EF, $3F, $3F, $3F, $3F // 
       .byte  $3F, $3F, $3F, $3F, $A8, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $55, $66, $66, $E7, $FF // 
       .byte  $FF, $FF, $FF, $57, $65, $65, $E5, $F5 // 
       .byte  $FF, $FF, $FF, $FF, $3F, $3F, $3F, $3F // 
       .byte  $3F, $AB, $AA, $AA, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $5D, $5D, $D7, $AA, $AB, $AF, $BA // 
       .byte  $AB, $AA, $EA, $AA, $BB, $EE, $EF, $BA // 
       .byte  $EF, $AA, $FF, $FA, $AA, $AA, $AA, $AB // 
       .byte  $AA, $AA, $AF, $AA, $EF, $BE, $FA, $EE // 
       .byte  $EB, $BA, $BE, $EA, $57, $DF, $55, $DD // 
       .byte  $57, $5E, $7D, $D7, $DE, $FF, $FF, $7F // 
       .byte  $5F, $FF, $9F, $EB, $AB, $AA, $AE, $AA // 
       .byte  $AA, $AB, $AA, $AA, $EE, $BB, $EE, $BF // 
       .byte  $FE, $FF, $FF, $EB, $BE, $FB, $FF, $EF // 
       .byte  $FF, $FF, $FF, $FF, $7A, $FE, $FF, $FF // 
       .byte  $FF, $FF, $77, $DD, $D6, $59, $6A, $66 // 
       .byte  $99, $56, $6A, $55, $DF, $75, $D7, $5D // 
       .byte  $F5, $5D, $55, $55, $55, $95, $55, $95 // 
       .byte  $55, $F5, $7F, $77, $DD, $F5, $D7, $F5 // 
       .byte  $D5, $FD, $F5, $AF, $56, $AA, $A6, $5A // 
       .byte  $A9, $A6, $65, $9B, $75, $77, $DF, $7D // 
       .byte  $F5, $75, $D5, $55, $AA, $AA, $EA, $AB // 
       .byte  $EA, $AE, $AA, $BB, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FD, $77, $D7, $F7 // 
       .byte  $D7, $DF, $DF, $DD, $AA, $A9, $A6, $A9 // 
       .byte  $96, $B9, $E6, $E9, $BF, $BB, $AE, $BA // 
       .byte  $EB, $AE, $AB, $BA, $FA, $AA, $FA, $EE // 
       .byte  $FA, $BA, $AE, $BB, $7F, $FD, $F5, $75 // 
       .byte  $F5, $7D, $75, $BE, $BB, $AE, $AB, $AA // 
       .byte  $AA, $AA, $AA, $AA, $BB, $EE, $AA, $AA // 
       .byte  $AA, $AF, $BE, $FB, $FA, $AE, $AA, $AA // 
       .byte  $EA, $FA, $FE, $FF, $7D, $7A, $E6, $F9 // 
       .byte  $AA, $69, $AA, $8B, $95, $95, $95, $95 // 
       .byte  $A7, $BF, $B5, $D5, $DD, $D7, $67, $9D // 
       .byte  $57, $55, $95, $65, $DD, $57, $DD, $F5 // 
       .byte  $5D, $55, $55, $7D, $AA, $AA, $AB, $AB // 
       .byte  $AE, $AB, $BE, $EB, $AA, $97, $6E, $99 // 
       .byte  $A6, $9A, $66, $66, $FA, $AA, $6A, $AA // 
       .byte  $AA, $AA, $AA, $AA, $A9, $FA, $FE, $FE // 
       .byte  $FE, $FE, $FE, $FF, $EB, $FA, $FE, $EF // 
       .byte  $FF, $FF, $FF, $FF, $BA, $EA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FE, $FF // 
       .byte  $FF, $FF, $FF, $FE, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FE, $FE, $FB, $FF, $FF, $EF, $AB // 
       .byte  $AE, $AA, $EA, $FF, $FF, $FB, $FE, $FF // 
       .byte  $7F, $BF, $6F, $5B, $FF, $FF, $FE, $7F // 
       .byte  $7E, $7E, $DF, $1E, $FF, $FC, $F2, $F1 // 
       .byte  $CA, $69, $B7, $EF, $CF, $20, $5E, $AD // 
       .byte  $77, $FD, $DF, $FF, $D6, $FF, $FC, $0C // 
       .byte  $63, $BB, $FF, $FF, $FB, $4D, $4A, $7C // 
       .byte  $20, $CD, $FF, $F3, $9D, $FF, $15, $FA // 
       .byte  $3A, $FA, $FA, $FE, $EA, $EA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AE, $AB, $A6, $A9 // 
       .byte  $AA, $A6, $AA, $AA, $EF, $BF, $EE, $FB // 
       .byte  $BE, $FB, $BE, $EB, $AF, $AB, $22, $AA // 
       .byte  $AB, $AF, $AF, $2F, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $BA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $EA, $EB, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AF, $AF, $2F, $AB // 
       .byte  $2B, $BB, $E2, $3B, $EF, $BE, $BA, $BB // 
       .byte  $FF, $FB, $EF, $2E, $7D, $F7, $F7, $DF // 
       .byte  $7C, $F2, $42, $0A, $FF, $FF, $FF, $1F // 
       .byte  $57, $57, $D7, $95, $2D, $D5, $73, $75 // 
       .byte  $DF, $01, $7C, $F3, $EF, $2C, $CC, $B0 // 
       .byte  $CF, $35, $FF, $FF, $0A, $1A, $1E, $65 // 
       .byte  $5E, $F7, $FF, $FF, $BA, $AA, $BA, $BE // 
       .byte  $E9, $6E, $FF, $FF, $AA, $AA, $AA, $A8 // 
       .byte  $A8, $A2, $AA, $AA, $AA, $A9, $A9, $28 // 
       .byte  $88, $A2, $0A, $26, $AA, $A8, $AB, $EE // 
       .byte  $FA, $EA, $EB, $BB, $AA, $AA, $2A, $82 // 
       .byte  $A8, $CA, $A2, $22, $FF, $FF, $FC, $FF // 
       .byte  $FF, $FF, $FF, $FF, $2E, $23, $8A, $8A // 
       .byte  $8A, $2A, $AA, $A5, $DD, $D7, $5F, $DF // 
       .byte  $DF, $7F, $7B, $7F, $33, $3C, $3C, $CF // 
       .byte  $FF, $FF, $FF, $FF, $FD, $F5, $55, $55 // 
       .byte  $55, $55, $95, $A5, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $57, $55, $55, $55 // 
       .byte  $55, $55, $55, $6A, $A5, $55, $55, $55 // 
       .byte  $55, $5F, $FE, $AF, $EF, $FA, $FF, $FF // 
       .byte  $FF, $EF, $FF, $FF, $55, $FF, $AA, $FF // 
       .byte  $FF, $FF, $BF, $FF, $EB, $FF, $FF, $FF // 
       .byte  $FE, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FD, $55, $55, $55, $55 // 
       .byte  $55, $7F, $FA, $EF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $AA, $55, $AA, $AA, $AA, $AA, $AB // 
       .byte  $AF, $BD, $F7, $DF, $A5, $97, $5D, $75 // 
       .byte  $D5, $55, $55, $55, $7F, $F5, $F5, $F6 // 
       .byte  $F6, $F6, $F6, $F6, $AA, $AB, $AB, $AB // 
       .byte  $AB, $AB, $AB, $AB, $FF, $FF, $FF, $FF // 
       .byte  $FF, $AF, $5A, $A5, $FF, $FF, $FF, $FE // 
       .byte  $FA, $E9, $A6, $5A, $FA, $E9, $AA, $9A // 
       .byte  $AA, $AA, $AA, $AA, $EA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $55, $55, $55, $55 // 
       .byte  $75, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $57, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $FF, $55, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $FE, $5F, $F7, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $6A, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $EA, $EA, $6A // 
       .byte  $6A, $6A, $6A, $6A, $AB, $FA, $FA, $DA // 
       .byte  $DA, $DA, $DA, $FA, $5F, $97, $65, $59 // 
       .byte  $56, $55, $55, $55, $FF, $FF, $FF, $BF // 
       .byte  $AF, $6B, $9A, $A6, $FE, $EF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $5F, $97, $55, $59 // 
       .byte  $56, $55, $55, $55, $FF, $FF, $FF, $BF // 
       .byte  $AF, $6A, $AA, $A5, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $BA, $AA, $FB, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $BF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FD, $55, $55, $55 // 
       .byte  $55, $75, $55, $55, $AE, $FB, $AF, $BA // 
       .byte  $FF, $EB, $AF, $BB, $5A, $65, $55, $59 // 
       .byte  $96, $69, $5E, $A6, $BF, $EB, $BE, $EB // 
       .byte  $BB, $EE, $BF, $FF, $AA, $EA, $EA, $EA // 
       .byte  $FA, $AA, $BA, $6B, $AE, $AA, $AA, $AB // 
       .byte  $AE, $AA, $AA, $AA, $5B, $AA, $66, $AA // 
       .byte  $AA, $AA, $AA, $AA, $A9, $EA, $BA, $AE // 
       .byte  $AE, $AE, $AB, $AB, $9A, $9E, $6E, $AA // 
       .byte  $6A, $6A, $AA, $AA, $FF, $57, $55, $55 // 
       .byte  $55, $55, $AA, $AA, $FF, $FF, $FF, $7F // 
       .byte  $57, $55, $55, $95, $FD, $FD, $FD, $F5 // 
       .byte  $D5, $AE, $AA, $AA, $BF, $BF, $BD, $B5 // 
       .byte  $95, $55, $55, $56, $F5, $FF, $FF, $FF // 
       .byte  $FF, $F7, $FF, $FF, $5F, $95, $6A, $55 // 
       .byte  $55, $55, $55, $56, $55, $D5, $FD, $AF // 
       .byte  $FE, $FF, $FF, $FF, $5B, $6A, $BA, $EA // 
       .byte  $AA, $AA, $AE, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AB, $AF, $AA, $AA, $AA, $A6 // 
       .byte  $95, $55, $5D, $7F, $AA, $AA, $AA, $AA // 
       .byte  $AA, $EA, $FA, $FE, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AB, $EC, $AA, $5B, $7B, $5F, $57 // 
       .byte  $57, $57, $57, $57, $FB, $EA, $EA, $EA // 
       .byte  $EA, $EA, $EA, $EA, $A6, $AB, $AF, $A3 // 
       .byte  $9D, $97, $6C, $91, $FE, $EF, $FA, $FF // 
       .byte  $FE, $EF, $BB, $FF, $56, $56, $D6, $FF // 
       .byte  $FF, $FF, $FF, $FF, $97, $97, $9D, $DF // 
       .byte  $F7, $FF, $FF, $FF, $5A, $9A, $6E, $6A // 
       .byte  $AA, $6B, $9E, $AB, $AF, $BF, $EF, $BF // 
       .byte  $EF, $AF, $BB, $AB, $FF, $AF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $5F, $FD, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $7F, $5F, $97 // 
       .byte  $65, $55, $56, $55, $EF, $BA, $AB, $AE // 
       .byte  $AB, $6A, $6A, $6A, $AA, $AA, $BB, $8A // 
       .byte  $BF, $EE, $B3, $EE, $AA, $AA, $AE, $BE // 
       .byte  $EA, $8E, $AB, $F2, $AA, $AA, $AA, $AA // 
       .byte  $EC, $8F, $BA, $EB, $AA, $AA, $AA, $AA // 
       .byte  $EA, $BA, $EB, $AA, $5D, $37, $DC, $77 // 
       .byte  $CC, $5F, $FF, $1E, $3B, $B2, $EE, $AE // 
       .byte  $A8, $BB, $EB, $AE, $57, $74, $7F, $D2 // 
       .byte  $7F, $DF, $7F, $F4, $C7, $7D, $F4, $D5 // 
       .byte  $87, $BF, $E7, $EF, $FB, $E3, $FF, $FA // 
       .byte  $FC, $FA, $FF, $FF, $D5, $7F, $95, $69 // 
       .byte  $95, $55, $55, $55, $AA, $EA, $FB, $FB // 
       .byte  $BE, $BE, $BE, $AF, $EA, $EA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FE, $EA, $BF, $A5, $A5, $A5, $95 // 
       .byte  $56, $F6, $FF, $BF, $55, $55, $55, $55 // 
       .byte  $55, $55, $D5, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FE, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AE, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FE, $EF, $FF, $FF, $FF // 
       .byte  $FF, $FE, $AF, $FD, $FF, $FF, $FF, $FF // 
       .byte  $FF, $AF, $FB, $7E, $AA, $AA, $AA, $EA // 
       .byte  $AA, $AA, $AA, $EA, $FB, $BD, $F5, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $57, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $BA, $AA, $6A, $56 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FD, $D5, $55, $55, $FF, $FF, $F5, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $7F, $57 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $EA, $FE, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AB, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $BF, $FB, $FF, $FF, $EF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $BA, $AE, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $55, $57 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $9A, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $F5, $5F, $FE // 
       .byte  $AA, $AA, $AA, $AA, $AB, $EE, $AA, $55 // 
       .byte  $55, $55, $55, $55, $FB, $AA, $55, $55 // 
       .byte  $55, $55, $55, $55, $AA, $FF, $57, $55 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $A9, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $BF, $FF, $FF, $FF, $AA, $AA, $AF, $AF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $DF, $55, $55 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AB, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $7F, $FF, $FF // 
       .byte  $FF, $DF, $FD, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AB, $55, $55, $55, $55 // 
       .byte  $55, $55, $7F, $D5, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $BA, $AE, $AA, $AA, $AB, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $AF, $FD, $55 // 
       .byte  $55, $55, $55, $55, $A5, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $5B, $56, $55, $55 // 
       .byte  $55, $55, $55, $55, $AB, $FE, $AA, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $7F, $5F // 
       .byte  $57, $55, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $A5, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $A6, $AA, $9B, $FF, $FF, $EE, $22 // 
       .byte  $88, $99, $55, $55, $55, $72, $32, $0C // 
       .byte  $CE, $EE, $AA, $AA, $95, $55, $55, $57 // 
       .byte  $57, $57, $5F, $7F, $5F, $7F, $7F, $5F // 
       .byte  $95, $59, $56, $69, $FF, $AA, $FF, $FF // 
       .byte  $7F, $55, $55, $55, $FF, $AA, $FF, $FF // 
       .byte  $FE, $5F, $57, $5F, $55, $55, $55, $95 // 
       .byte  $55, $55, $95, $A5, $55, $55, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $7F, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FE, $FF, $FF, $FF, $FD // 
       .byte  $FF, $D5, $F6, $5A, $FA, $FE, $56, $5A // 
       .byte  $6A, $6A, $AA, $AA, $FE, $FF, $FE, $E9 // 
       .byte  $55, $55, $55, $55, $A9, $A5, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $A6, $AA, $BF, $55, $5D, $55, $75 // 
       .byte  $D6, $56, $6A, $AA, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $5A, $5A, $5A, $6A // 
       .byte  $6A, $AA, $AA, $AA, $55, $55, $95, $A9 // 
       .byte  $AA, $AA, $AA, $AB, $AA, $AA, $AA, $AA // 
       .byte  $6A, $5A, $5A, $6A, $AA, $AA, $A9, $A9 // 
       .byte  $A9, $A9, $A5, $A5, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $5D, $55, $55, $5A // 
       .byte  $56, $55, $55, $57, $D5, $D5, $55, $55 // 
       .byte  $55, $D5, $F5, $FD, $5A, $56, $56, $56 // 
       .byte  $5A, $5A, $5A, $56, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $95, $65, $55, $55 // 
       .byte  $55, $59, $65, $97, $55, $D5, $F5, $F5 // 
       .byte  $D5, $D5, $55, $55, $FF, $FF, $FF, $7F // 
       .byte  $7F, $7F, $7F, $FF, $AA, $AA, $AB, $AE // 
       .byte  $AB, $AF, $BB, $AF, $FF, $EE, $FB, $BA // 
       .byte  $AA, $EB, $BA, $EB, $FE, $BB, $EF, $AB // 
       .byte  $BE, $AB, $EE, $9B, $5A, $5A, $56, $D6 // 
       .byte  $D6, $76, $F6, $76, $D7, $77, $F5, $D9 // 
       .byte  $77, $D5, $7B, $F6, $55, $6F, $59, $DE // 
       .byte  $7E, $FB, $ED, $E7, $AA, $6B, $6E, $EB // 
       .byte  $BB, $EF, $BF, $FF, $5B, $6B, $6B, $6F // 
       .byte  $6F, $AF, $AF, $AF, $FA, $AE, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AF, $95, $95, $65, $69 // 
       .byte  $59, $5B, $FA, $FE, $AA, $A9, $A5, $A5 // 
       .byte  $95, $5D, $75, $D5, $AF, $AF, $AB, $AA // 
       .byte  $AA, $AA, $AB, $AB, $EA, $FA, $FE, $FF // 
       .byte  $FF, $FF, $FF, $FF, $F5, $DF, $7F, $FF // 
       .byte  $AF, $AB, $AA, $AA, $AA, $EA, $BA, $AA // 
       .byte  $AA, $AA, $6A, $5B, $AF, $AF, $AF, $BF // 
       .byte  $BF, $BF, $FF, $FF, $A1, $99, $AA, $89 // 
       .byte  $A6, $A8, $A2, $89, $55, $55, $5D, $55 // 
       .byte  $55, $55, $95, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $57, $55, $55, $55, $5D, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $CC, $F3 // 
       .byte  $FF, $FB, $EA, $FB, $AA, $2A, $EA, $BA // 
       .byte  $FF, $77, $55, $77, $FF, $FF, $FF, $FF // 
       .byte  $55, $99, $AA, $99, $AA, $AA, $AA, $AA // 
       .byte  $EA, $7A, $5E, $7E, $FB, $FF, $FB, $CC // 
       .byte  $F1, $D5, $FE, $D1, $BB, $FF, $D5, $55 // 
       .byte  $55, $D5, $95, $E5, $BB, $FF, $55, $55 // 
       .byte  $55, $55, $55, $55, $FA, $EA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $C8, $93, $87, $A1 // 
       .byte  $AA, $AA, $AA, $AA, $BA, $16, $4E, $23 // 
       .byte  $FF, $FF, $FF, $FF, $A9, $AA, $AA, $AA // 
       .byte  $6A, $FA, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AE, $AA, $AA, $7F, $5F, $5F, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FE // 
       .byte  $BF, $AE, $BF, $FA, $FF, $FB, $AE, $EB // 
       .byte  $BA, $EE, $AA, $BE, $AA, $AA, $EA, $BA // 
       .byte  $EA, $EE, $BA, $FB, $FE, $DE, $DE, $FE // 
       .byte  $FE, $CE, $FE, $C6, $D5, $F7, $5D, $D5 // 
       .byte  $75, $DD, $FD, $DB, $D5, $5D, $D5, $56 // 
       .byte  $95, $55, $B7, $9E, $56, $9A, $66, $5A // 
       .byte  $99, $E6, $EA, $99, $FF, $E1, $FF, $CF // 
       .byte  $ED, $F0, $CB, $EF, $DE, $F6, $FE, $FF // 
       .byte  $7F, $7F, $DF, $1F, $E9, $7D, $F7, $77 // 
       .byte  $77, $DF, $DF, $DF, $EA, $AA, $EA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $F7, $D5, $F7 // 
       .byte  $F7, $FF, $FF, $FE, $FF, $77, $55, $77 // 
       .byte  $77, $FF, $AA, $AA, $AA, $66, $55, $66 // 
       .byte  $66, $AA, $FF, $FF, $6A, $DA, $F6, $D6 // 
       .byte  $5A, $6A, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $EF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AB, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $BF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AE, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FE, $FB, $FF, $FF, $FF, $FB, $BF // 
       .byte  $B5, $F5, $D5, $55, $FF, $BB, $FF, $55 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $EA, $AA, $AA, $AA, $EF, $ED, $ED, $ED // 
       .byte  $FD, $FF, $FB, $FB, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FE, $AA, $A9, $AA, $AA // 
       .byte  $AA, $AA, $AA, $BF, $FF, $FF, $FF, $FD // 
       .byte  $F5, $D5, $55, $55, $F6, $D5, $55, $55 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AA, $EA // 
       .byte  $EA, $EA, $FA, $FA, $BF, $AF, $6F, $AA // 
       .byte  $9A, $AA, $A9, $AA, $55, $55, $55, $55 // 
       .byte  $55, $D5, $F5, $BD, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FA, $FA, $FE, $FE // 
       .byte  $FE, $FE, $FE, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $BA, $AA, $AA, $FE, $FF, $F7, $FD // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $D5, $F5 // 
       .byte  $FD, $FF, $FB, $FF, $55, $55, $55, $55 // 
       .byte  $55, $A5, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $57, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AB, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $EA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $A9, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $A6, $BA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $BA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AB, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $BA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $55, $55, $55, $55 // 
       .byte  $55, $55, $57, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $BF, $AF, $BF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FE, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $AA, $A8, $BF, $BF, $BF, $BF // 
       .byte  $BF, $BF, $BF, $BF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FA, $EA, $B8, $B8, $AA, $8A // 
       .byte  $E0, $FA, $7F, $DF, $20, $20, $AA, $AA // 
       .byte  $00, $AA, $FF, $FF, $AE, $AA, $AA, $A0 // 
       .byte  $0A, $AA, $FF, $FD, $A2, $8B, $2B, $AF // 
       .byte  $BF, $FF, $7F, $FF, $FA, $FF, $FE, $FF // 
       .byte  $FF, $FF, $FF, $FF, $EA, $FF, $AF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $EB, $FE, $AB, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $A9 // 
       .byte  $AA, $AE, $AB, $AB, $A9, $9A, $A6, $5A // 
       .byte  $A5, $BA, $E9, $E5, $9A, $A9, $5A, $A6 // 
       .byte  $9A, $9A, $66, $99, $55, $55, $55, $55 // 
       .byte  $75, $5D, $5F, $57, $57, $DF, $7F, $5F // 
       .byte  $7F, $FF, $FF, $FF, $BF, $AE, $AB, $AF // 
       .byte  $AB, $AA, $AA, $AA, $5A, $66, $59, $66 // 
       .byte  $9E, $7A, $F9, $DA, $AA, $AA, $AA, $BB // 
       .byte  $AF, $A8, $A8, $A8, $00, $40, $AA, $A8 // 
       .byte  $A0, $00, $00, $0C, $00, $01, $15, $05 // 
       .byte  $41, $45, $47, $4F, $6A, $6A, $A6, $6A // 
       .byte  $A6, $AA, $A6, $9A, $54, $54, $57, $55 // 
       .byte  $57, $5F, $7F, $FF, $C2, $FF, $FE, $FB // 
       .byte  $FB, $EE, $BF, $FF, $87, $8F, $FF, $FF // 
       .byte  $FF, $BF, $F3, $FF, $A6, $AA, $A8, $8A // 
       .byte  $A8, $8A, $A8, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FE, $FF, $FC, $FA, $AA, $AA, $BB, $EE // 
       .byte  $8B, $33, $AA, $BF, $AA, $4A, $64, $85 // 
       .byte  $9A, $59, $26, $98, $FF, $FF, $BF, $2F // 
       .byte  $BF, $EF, $EF, $FF, $AA, $A3, $AE, $A8 // 
       .byte  $BF, $AE, $A3, $BA, $89, $69, $94, $99 // 
       .byte  $BD, $9F, $67, $19, $AA, $49, $92, $56 // 
       .byte  $55, $57, $DF, $FD, $CE, $A2, $BA, $CA // 
       .byte  $DB, $6A, $B2, $AF, $89, $A5, $9A, $A4 // 
       .byte  $A1, $A6, $A8, $AA, $65, $15, $65, $95 // 
       .byte  $55, $97, $55, $55, $7D, $F5, $F5, $D5 // 
       .byte  $F5, $F5, $F5, $7D, $B8, $EE, $AA, $B2 // 
       .byte  $AB, $B2, $EA, $AA, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $7F, $55, $55, $55, $55 // 
       .byte  $55, $54, $5F, $FF, $69, $69, $61, $2C // 
       .byte  $C7, $F1, $DF, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $EA, $FA, $BF, $55, $44, $44, $55 // 
       .byte  $45, $47, $57, $47, $55, $44, $44, $55 // 
       .byte  $A9, $66, $DA, $F6, $55, $44, $44, $55 // 
       .byte  $A9, $66, $9A, $66, $55, $44, $44, $55 // 
       .byte  $FD, $77, $DF, $77, $46, $56, $46, $47 // 
       .byte  $55, $45, $44, $54, $FE, $D7, $69, $AA // 
       .byte  $02, $00, $20, $08, $A9, $66, $DA, $B6 // 
       .byte  $6D, $5B, $17, $06, $56, $99, $65, $99 // 
       .byte  $56, $99, $E5, $F9, $44, $44, $55, $45 // 
       .byte  $45, $55, $44, $44, $02, $02, $09, $A5 // 
       .byte  $A5, $09, $02, $02, $0A, $02, $82, $60 // 
       .byte  $60, $82, $02, $0A, $FE, $7F, $9F, $27 // 
       .byte  $27, $9F, $7F, $FF, $A8, $88, $8A, $AA // 
       .byte  $89, $8B, $AB, $8B, $08, $20, $00, $02 // 
       .byte  $AA, $69, $D7, $FF, $09, $2B, $A7, $9F // 
       .byte  $7F, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FE, $FF, $CC, $CC, $FF // 
       .byte  $FF, $FF, $7F, $DF, $FF, $CC, $CC, $FF // 
       .byte  $57, $DD, $F5, $FD, $FF, $CC, $CC, $FF // 
       .byte  $57, $DD, $75, $DD, $AA, $88, $88, $AA // 
       .byte  $56, $99, $65, $99, $56, $99, $65, $99 // 
       .byte  $56, $99, $65, $99, $FF, $FF, $BF, $EF // 
       .byte  $AB, $EE, $BA, $EE, $AB, $EF, $FB, $FF // 
       .byte  $FF, $FF, $BF, $EF, $AA, $AA, $80, $80 // 
       .byte  $8A, $82, $82, $A2, $FE, $BA, $68, $58 // 
       .byte  $58, $58, $56, $55, $A6, $09, $02, $52 // 
       .byte  $52, $02, $0B, $AF, $57, $DD, $75, $DD // 
       .byte  $57, $DE, $BA, $AA, $AA, $AA, $EA, $9A // 
       .byte  $5A, $5A, $55, $55, $AB, $AF, $BE, $BA // 
       .byte  $AA, $AA, $AA, $55, $AE, $AF, $AB, $AA // 
       .byte  $AA, $AA, $AA, $55, $AA, $AA, $EA, $EA // 
       .byte  $AA, $AA, $AA, $55, $FF, $D5, $D5, $FF // 
       .byte  $FF, $FF, $FF, $AA, $FF, $CC, $CC, $FF // 
       .byte  $57, $DD, $75, $DD, $FF, $CC, $CC, $FF // 
       .byte  $57, $DD, $75, $DD, $FF, $CC, $CC, $FF // 
       .byte  $57, $DD, $75, $DD, $AA, $88, $88, $AA // 
       .byte  $56, $99, $65, $99, $FF, $FE, $3E, $3F // 
       .byte  $3F, $3E, $3E, $3E, $FE, $BB, $AA, $AA // 
       .byte  $AA, $FF, $FF, $FF, $AB, $EE, $FE, $FF // 
       .byte  $FF, $AA, $AA, $AA, $AA, $AA, $A0, $A0 // 
       .byte  $A2, $A2, $A2, $A2, $3F, $03, $03, $03 // 
       .byte  $FF, $FF, $57, $57, $D5, $D5, $FF, $FF // 
       .byte  $FF, $E9, $E6, $EA, $5A, $5A, $AA, $AA // 
       .byte  $AA, $FA, $7A, $FA, $A2, $02, $02, $02 // 
       .byte  $AA, $AA, $55, $55, $FD, $AA, $AA, $FF // 
       .byte  $FF, $FF, $FF, $55, $7F, $AB, $AB, $EB // 
       .byte  $EB, $EB, $FF, $55, $A5, $FF, $FF, $FA // 
       .byte  $FA, $FA, $AA, $55, $AA, $55, $55, $AA // 
       .byte  $AA, $AA, $AA, $FF, $FF, $CC, $CC, $FF // 
       .byte  $57, $DD, $75, $DD, $FF, $CC, $CC, $FF // 
       .byte  $57, $DD, $75, $DD, $FF, $CC, $CC, $FF // 
       .byte  $57, $DF, $7F, $FF, $AA, $88, $88, $AA // 
       .byte  $AA, $A9, $A5, $99, $FF, $FF, $0F, $0F // 
       .byte  $CF, $0F, $0F, $3F, $57, $DF, $7F, $FF // 
       .byte  $FF, $FD, $F5, $DD, $FF, $FD, $F5, $DD // 
       .byte  $57, $DD, $75, $DD, $56, $99, $65, $99 // 
       .byte  $56, $99, $65, $99, $55, $56, $5A, $76 // 
       .byte  $7D, $7F, $FF, $FF, $56, $99, $66, $9A // 
       .byte  $56, $9A, $EF, $FF, $AA, $82, $00, $14 // 
       .byte  $14, $00, $82, $EB, $FE, $B9, $A5, $95 // 
       .byte  $95, $95, $55, $55, $AA, $FA, $FA, $AA // 
       .byte  $AA, $AA, $AA, $55, $AA, $AB, $AF, $AE // 
       .byte  $AA, $AA, $AA, $55, $EB, $EB, $AA, $AA // 
       .byte  $AA, $AA, $AA, $55, $FF, $BF, $AF, $EF // 
       .byte  $FF, $FF, $FF, $55, $55, $FF, $FF, $FF // 
       .byte  $EF, $EB, $FA, $FE, $55, $FF, $FF, $FF // 
       .byte  $FF, $FE, $FA, $FB, $FF, $55, $55, $55 // 
       .byte  $95, $95, $55, $55, $55, $FF, $FF, $FF // 
       .byte  $FF, $EA, $EA, $FF, $FF, $FE, $F8, $F8 // 
       .byte  $F8, $E8, $9A, $56, $F5, $0D, $03, $A3 // 
       .byte  $A3, $03, $0E, $FB, $55, $65, $B9, $FE // 
       .byte  $BB, $EF, $BB, $FE, $55, $55, $5F, $5F // 
       .byte  $DF, $BF, $FF, $FF, $BB, $EF, $BB, $FE // 
       .byte  $BB, $EF, $BB, $FE, $EE, $BA, $EE, $AB // 
       .byte  $EF, $BF, $FF, $FF, $DF, $7F, $FF, $FF // 
       .byte  $FF, $F7, $DF, $57, $F3, $C3, $C3, $CF // 
       .byte  $C0, $C0, $FF, $FF, $EF, $BF, $FF, $FF // 
       .byte  $FF, $CC, $CC, $FF, $FE, $FA, $EE, $AB // 
       .byte  $FF, $CC, $CC, $FF, $EE, $BA, $EE, $AB // 
       .byte  $FF, $CC, $CC, $FF, $DD, $75, $DD, $57 // 
       .byte  $FF, $CC, $CC, $FF, $AA, $FF, $FF, $FF // 
       .byte  $FF, $55, $55, $FE, $AA, $FF, $D7, $D7 // 
       .byte  $D7, $57, $57, $BF, $AA, $FF, $5F, $5F // 
       .byte  $5F, $55, $55, $FA, $AA, $FF, $FF, $FF // 
       .byte  $FF, $55, $55, $FF, $AB, $AB, $FF, $FF // 
       .byte  $03, $03, $03, $3F, $BF, $B7, $BD, $AA // 
       .byte  $AA, $AA, $95, $95, $F5, $B5, $F5, $55 // 
       .byte  $55, $55, $A5, $A5, $55, $55, $FF, $FF // 
       .byte  $03, $03, $03, $F3, $3D, $3D, $3D, $3F // 
       .byte  $3F, $3D, $FD, $FF, $FF, $FF, $FF, $AA // 
       .byte  $AA, $AA, $BB, $FE, $55, $55, $55, $FF // 
       .byte  $FF, $FD, $DD, $57, $F3, $F3, $F3, $F3 // 
       .byte  $F0, $F0, $FF, $FF, $EE, $BA, $EE, $AB // 
       .byte  $FF, $CC, $CC, $FF, $DD, $75, $DD, $57 // 
       .byte  $FF, $CC, $CC, $FF, $DD, $75, $DD, $57 // 
       .byte  $FF, $CC, $CC, $FF, $DD, $75, $DD, $57 // 
       .byte  $FF, $CC, $CC, $FF, $55, $FF, $FF, $FF // 
       .byte  $FF, $AF, $AF, $FF, $AA, $FF, $FF, $FF // 
       .byte  $F7, $F5, $FD, $FF, $AA, $FF, $FF, $FF // 
       .byte  $FF, $FF, $7D, $7D, $55, $AA, $AA, $AA // 
       .byte  $BA, $FA, $EA, $AA, $FF, $FF, $BF, $BE // 
       .byte  $B9, $A5, $A9, $AA, $55, $75, $EF, $AB // 
       .byte  $EF, $BB, $EE, $AB, $69, $82, $00, $3C // 
       .byte  $3C, $00, $82, $EA, $55, $55, $95, $95 // 
       .byte  $95, $A5, $B9, $EE, $2A, $0A, $0A, $8A // 
       .byte  $0A, $0A, $AA, $AA, $BB, $AF, $AB, $AA // 
       .byte  $AA, $EA, $BA, $FE, $77, $DD, $77, $FD // 
       .byte  $77, $5F, $57, $55, $59, $65, $99, $56 // 
       .byte  $99, $65, $99, $56, $EE, $BA, $EE, $AB // 
       .byte  $FF, $CC, $CC, $FF, $EE, $BA, $EE, $AB // 
       .byte  $FF, $CC, $CC, $FF, $FF, $BF, $EF, $AB // 
       .byte  $FF, $CC, $CC, $FF, $99, $A5, $A9, $AA // 
       .byte  $AA, $88, $88, $AA, $57, $45, $45, $55 // 
       .byte  $45, $45, $55, $45, $55, $55, $55, $55 // 
       .byte  $D5, $D5, $D5, $F5, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $56, $56, $56, $56 // 
       .byte  $56, $56, $56, $56, $45, $57, $45, $47 // 
       .byte  $55, $47, $45, $57, $F5, $F5, $FD, $FD // 
       .byte  $BD, $BF, $BF, $EF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $56, $56, $56, $56 // 
       .byte  $56, $56, $56, $56, $45, $46, $55, $46 // 
       .byte  $45, $56, $45, $46, $6A, $5A, $66, $96 // 
       .byte  $66, $59, $6A, $9A, $95, $95, $95, $A5 // 
       .byte  $A5, $A5, $A9, $A9, $56, $56, $56, $56 // 
       .byte  $56, $56, $56, $56, $FF, $CE, $CF, $FE // 
       .byte  $CF, $CE, $FF, $CE, $BF, $AC, $BC, $EC // 
       .byte  $BC, $AC, $BC, $EC, $FE, $0F, $0F, $C3 // 
       .byte  $C3, $FF, $FF, $00, $FE, $F6, $F6, $B6 // 
       .byte  $B6, $B6, $B6, $B6, $CF, $FE, $CF, $CE // 
       .byte  $FF, $CE, $CF, $FE, $BC, $AF, $BF, $EF // 
       .byte  $BF, $AF, $BE, $EE, $00, $F0, $F0, $F0 // 
       .byte  $F0, $F0, $B0, $BF, $E7, $E7, $E7, $E7 // 
       .byte  $E7, $E7, $F7, $F7, $CF, $CD, $FF, $CD // 
       .byte  $CF, $FD, $CF, $CD, $7D, $5D, $7D, $DD // 
       .byte  $7D, $5D, $7D, $DD, $6A, $6A, $6A, $56 // 
       .byte  $56, $56, $56, $56, $F7, $EB, $EB, $EB // 
       .byte  $EB, $EB, $EB, $EB, $FF, $CD, $CF, $FD // 
       .byte  $CF, $CD, $FF, $CD, $7D, $5D, $7D, $DD // 
       .byte  $7D, $5D, $7D, $DD, $56, $56, $56, $56 // 
       .byte  $6A, $6A, $6A, $6A, $D7, $D7, $D7, $D7 // 
       .byte  $D7, $D7, $FB, $FB, $CF, $FE, $CF, $CE // 
       .byte  $FF, $CE, $CF, $FE, $BE, $AF, $BF, $EF // 
       .byte  $BF, $AF, $BC, $EC, $D0, $50, $50, $50 // 
       .byte  $50, $50, $00, $00, $F7, $E7, $E7, $E7 // 
       .byte  $E7, $E7, $E7, $E7, $CF, $CD, $FF, $CD // 
       .byte  $CF, $FD, $CF, $CD, $7C, $5C, $7C, $DC // 
       .byte  $7C, $5C, $7F, $DF, $FF, $FF, $C3, $C3 // 
       .byte  $0F, $0F, $FD, $FD, $79, $79, $79, $F9 // 
       .byte  $F9, $F9, $F9, $FD, $AA, $8B, $8A, $AB // 
       .byte  $8A, $8B, $AA, $8B, $75, $5D, $77, $D7 // 
       .byte  $77, $5F, $7F, $DF, $A9, $A5, $A5, $A5 // 
       .byte  $95, $95, $95, $55, $56, $56, $56, $56 // 
       .byte  $56, $56, $56, $56, $8A, $A9, $8A, $89 // 
       .byte  $AA, $89, $8A, $AA, $EA, $EA, $E9, $A9 // 
       .byte  $A9, $A5, $A5, $A5, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $56, $56, $56, $56 // 
       .byte  $56, $56, $56, $56, $8A, $8A, $AA, $8A // 
       .byte  $8A, $AA, $89, $89, $95, $95, $95, $55 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $56, $56, $56, $56 // 
       .byte  $56, $56, $56, $56, $D5, $D5, $D5, $D5 // 
       .byte  $D5, $D5, $D5, $D5, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $55, $55 // 
       .byte  $56, $56, $56, $5A, $6A, $A2, $A2, $AA // 
       .byte  $A2, $A2, $AA, $A2, $95, $95, $95, $95 // 
       .byte  $95, $95, $95, $95, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $5A, $5A, $6A, $6A // 
       .byte  $6B, $AB, $AB, $AE, $A2, $6A, $A2, $62 // 
       .byte  $AA, $62, $A2, $6A, $D5, $D5, $D5, $D5 // 
       .byte  $D5, $D5, $D5, $D5, $AB, $AB, $AB, $AF // 
       .byte  $AF, $AF, $BF, $BF, $FE, $FA, $EE, $EB // 
       .byte  $EE, $BA, $FE, $FB, $A2, $62, $AA, $62 // 
       .byte  $A2, $6A, $A2, $62, $7F, $6F, $6F, $6D // 
       .byte  $6D, $6D, $6D, $6D, $7F, $F0, $F0, $C3 // 
       .byte  $C3, $FF, $FF, $00, $FD, $35, $3D, $37 // 
       .byte  $3D, $35, $3D, $37, $FF, $73, $F3, $7F // 
       .byte  $F3, $73, $FF, $73, $79, $79, $79, $79 // 
       .byte  $79, $79, $75, $75, $00, $0F, $0F, $0F // 
       .byte  $0F, $0F, $0E, $FE, $3D, $F5, $FD, $F7 // 
       .byte  $FD, $F5, $7D, $77, $F3, $7F, $F3, $73 // 
       .byte  $FF, $73, $F3, $7F, $EF, $D7, $D7, $D7 // 
       .byte  $D7, $D7, $D7, $D7, $FD, $FD, $FD, $D5 // 
       .byte  $D5, $D5, $D5, $D5, $7D, $75, $7D, $77 // 
       .byte  $7D, $75, $7D, $77, $F3, $73, $FF, $73 // 
       .byte  $F3, $7F, $F3, $73, $D7, $D7, $D7, $D7 // 
       .byte  $D7, $D7, $EF, $EF, $D5, $D5, $D5, $D5 // 
       .byte  $FD, $FD, $FD, $FD, $7D, $75, $7D, $77 // 
       .byte  $7D, $75, $7D, $77, $FF, $73, $F3, $7F // 
       .byte  $F3, $73, $FF, $73, $DF, $DB, $DB, $DB // 
       .byte  $DB, $DB, $DB, $DB, $0E, $0F, $0F, $0F // 
       .byte  $0F, $0F, $00, $00, $EB, $AF, $AB, $AE // 
       .byte  $AB, $AF, $2B, $2E, $F3, $7F, $F3, $73 // 
       .byte  $FF, $73, $F3, $7F, $6D, $6D, $6D, $6F // 
       .byte  $6F, $6F, $6F, $7F, $FF, $FF, $C3, $C3 // 
       .byte  $F0, $F0, $7F, $7F, $3D, $35, $3D, $37 // 
       .byte  $3D, $35, $FD, $F7, $F3, $73, $FF, $73 // 
       .byte  $F3, $7F, $F3, $73, $D5, $D5, $D5, $D5 // 
       .byte  $D5, $D5, $D5, $D5, $7F, $5F, $5F, $5F // 
       .byte  $57, $57, $57, $55, $5D, $75, $DD, $D7 // 
       .byte  $DD, $F5, $FD, $F7, $AA, $62, $A2, $6A // 
       .byte  $A2, $62, $AA, $62, $95, $95, $95, $95 // 
       .byte  $95, $95, $95, $95, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AB, $AB, $6B, $6A // 
       .byte  $6A, $5A, $5A, $5A, $A2, $6A, $A2, $62 // 
       .byte  $AA, $62, $A2, $AA, $95, $95, $95, $95 // 
       .byte  $95, $95, $95, $95, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $56, $56, $56, $55 // 
       .byte  $55, $55, $55, $55, $A2, $A2, $AA, $A2 // 
       .byte  $A2, $AA, $62, $62, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $5D, $5B // 
       .byte  $79, $D7, $5D, $D7, $55, $55, $55, $5D // 
       .byte  $B7, $BE, $56, $DF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $AF, $FF, $AA, $AB, $AD, $A9 // 
       .byte  $AB, $AE, $AA, $BB, $BE, $9A, $DF, $BB // 
       .byte  $AE, $FF, $BB, $AE, $6E, $7B, $EF, $BF // 
       .byte  $EF, $FE, $DF, $DF, $AB, $FE, $7B, $6F // 
       .byte  $BB, $A6, $B7, $EB, $FF, $FE, $FB, $FF // 
       .byte  $FE, $FF, $FF, $FE, $FF, $A7, $B6, $EB // 
       .byte  $EE, $FB, $EA, $AE, $FF, $EF, $BD, $FD // 
       .byte  $AA, $BE, $EB, $BF, $FA, $EE, $AA, $BA // 
       .byte  $FA, $AA, $EA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FD, $EA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $95, $5E, $D5, $E5, $A5, $A9, $BD // 
       .byte  $FF, $FF, $FF, $F7, $AA, $AF, $7F, $7F // 
       .byte  $FF, $FF, $FF, $FF, $AA, $22, $22, $AA // 
       .byte  $56, $99, $65, $99, $AA, $22, $22, $AA // 
       .byte  $FE, $BB, $EF, $BB, $AA, $22, $22, $AA // 
       .byte  $FE, $B9, $E5, $95, $AA, $22, $22, $AA // 
       .byte  $62, $62, $6A, $62, $FD, $77, $DF, $77 // 
       .byte  $FD, $76, $DA, $6A, $56, $9B, $6F, $BD // 
       .byte  $F6, $DA, $E8, $60, $FF, $D7, $69, $AA // 
       .byte  $80, $00, $08, $20, $E2, $EA, $E2, $62 // 
       .byte  $AA, $A2, $22, $2A, $AA, $A9, $A7, $9C // 
       .byte  $9C, $A7, $A9, $AA, $A0, $80, $82, $09 // 
       .byte  $09, $82, $80, $A0, $80, $80, $60, $5A // 
       .byte  $5A, $60, $80, $80, $22, $22, $AA, $A2 // 
       .byte  $A2, $AA, $22, $22, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $BF, $60, $E8, $DA, $F6 // 
       .byte  $FD, $FF, $FF, $FF, $20, $08, $00, $80 // 
       .byte  $AA, $69, $D7, $FF, $2A, $22, $A2, $AA // 
       .byte  $62, $E2, $EA, $E2, $CD, $FD, $CD, $CE // 
       .byte  $FF, $CF, $CC, $FC, $55, $69, $BE, $FF // 
       .byte  $03, $00, $30, $0C, $55, $55, $55, $95 // 
       .byte  $E5, $F9, $3D, $0E, $AB, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $CC, $CC, $FF, $CF // 
       .byte  $CF, $FF, $CC, $CC, $02, $02, $09, $A5 // 
       .byte  $A5, $09, $02, $02, $0F, $03, $C3, $70 // 
       .byte  $70, $C3, $03, $0F, $FF, $7F, $9F, $27 // 
       .byte  $27, $9F, $7F, $FE, $A8, $88, $8A, $AA // 
       .byte  $8B, $89, $A9, $89, $08, $20, $00, $02 // 
       .byte  $AA, $EB, $7D, $56, $09, $2B, $A7, $9E // 
       .byte  $79, $E5, $99, $56, $F9, $E5, $99, $56 // 
       .byte  $99, $65, $99, $56, $89, $A9, $89, $8A // 
       .byte  $AA, $88, $88, $AA, $F9, $E5, $99, $56 // 
       .byte  $AA, $88, $88, $AA, $99, $65, $99, $56 // 
       .byte  $AA, $88, $88, $AA, $99, $65, $99, $56 // 
       .byte  $AA, $88, $88, $AA, $BF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $A9 // 
       .byte  $A7, $9F, $BC, $70, $55, $69, $BE, $FF // 
       .byte  $C0, $00, $0C, $30, $73, $7F, $73, $B3 // 
       .byte  $FF, $F3, $33, $3F, $AA, $A9, $A7, $9C // 
       .byte  $9C, $A7, $A9, $AA, $A0, $80, $82, $09 // 
       .byte  $09, $82, $80, $A0, $80, $80, $60, $5A // 
       .byte  $5A, $60, $80, $80, $33, $33, $FF, $F3 // 
       .byte  $F3, $FF, $33, $33, $6A, $DA, $76, $FD // 
       .byte  $77, $DF, $77, $FD, $70, $BC, $9F, $A7 // 
       .byte  $E9, $7A, $DE, $57, $20, $08, $00, $80 // 
       .byte  $AA, $EB, $7D, $55, $3F, $33, $F3, $FF // 
       .byte  $B3, $73, $7F, $73, $EE, $BA, $EE, $AB // 
       .byte  $FF, $33, $33, $FF, $99, $65, $99, $56 // 
       .byte  $AA, $22, $22, $AA, $95, $E5, $B9, $FE // 
       .byte  $AA, $22, $22, $AA, $62, $6A, $62, $62 // 
       .byte  $AA, $22, $22, $AA, $AA, $AF, $AB, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $EB, $FF // 
       .byte  $BF, $BB, $FF, $EE, $AA, $AA, $EB, $FF // 
       .byte  $FE, $BA, $FF, $EE, $55, $F5, $D5, $55 // 
       .byte  $55, $55, $55, $55, $96, $A6, $6A, $6A // 
       .byte  $50, $5F, $5C, $5C, $FF, $77, $FF, $FF // 
       .byte  $00, $AA, $20, $20, $FF, $77, $FF, $FF // 
       .byte  $00, $AA, $20, $20, $BE, $BA, $AB, $AB // 
       .byte  $0F, $5F, $1F, $1F, $F4, $F5, $F4, $F4 // 
       .byte  $F5, $F4, $F4, $F4, $10, $55, $03, $00 // 
       .byte  $55, $0C, $03, $3E, $20, $AA, $88, $94 // 
       .byte  $55, $59, $05, $5D, $1F, $5F, $1F, $1F // 
       .byte  $1F, $1F, $9F, $DF, $5C, $5C, $5C, $5C // 
       .byte  $5E, $5C, $5E, $AA, $01, $45, $4D, $15 // 
       .byte  $13, $55, $55, $7D, $91, $59, $14, $65 // 
       .byte  $46, $62, $55, $55, $5F, $2F, $5F, $EF // 
       .byte  $5F, $67, $5F, $55, $55, $55, $55, $55 // 
       .byte  $56, $59, $59, $56, $55, $55, $5F, $77 // 
       .byte  $55, $DF, $75, $D7, $AA, $AA, $AE, $BF // 
       .byte  $EE, $FE, $EB, $BA, $FF, $FF, $FF, $7F // 
       .byte  $7F, $5F, $D7, $7F, $F5, $DF, $76, $FB // 
       .byte  $65, $D7, $DF, $77, $DD, $BD, $B7, $EF // 
       .byte  $FB, $FA, $7E, $FA, $F5, $DE, $57, $DF // 
       .byte  $7F, $FE, $BA, $AB, $D7, $FF, $9F, $5F // 
       .byte  $B7, $95, $DF, $F5, $FF, $AF, $F9, $EE // 
       .byte  $FB, $EE, $FB, $FB, $D7, $9F, $EF, $BF // 
       .byte  $BA, $FB, $AB, $AE, $EF, $EB, $EB, $FA // 
       .byte  $FA, $FA, $FB, $EB, $EE, $BA, $EF, $FB // 
       .byte  $FF, $FF, $FF, $FF, $AE, $AB, $AA, $AA // 
       .byte  $AA, $AA, $AB, $FF, $BF, $EF, $FF, $FF // 
       .byte  $FE, $FA, $AA, $AA, $96, $96, $96, $95 // 
       .byte  $5F, $FF, $FF, $EA, $AA, $AB, $AF, $FF // 
       .byte  $FF, $FF, $FE, $FA, $FF, $FF, $FD, $FF // 
       .byte  $F7, $FD, $F7, $FD, $56, $99, $66, $A9 // 
       .byte  $AA, $69, $96, $69, $7F, $5F, $77, $D5 // 
       .byte  $55, $75, $7F, $F7, $FF, $FF, $BF, $EF // 
       .byte  $BF, $FF, $EF, $BF, $96, $6A, $A6, $5B // 
       .byte  $A7, $9A, $A9, $66, $77, $DD, $5D, $FD // 
       .byte  $BF, $EE, $FA, $EB, $7D, $5F, $D5, $F7 // 
       .byte  $EF, $9D, $F7, $F7, $DF, $F7, $FF, $DF // 
       .byte  $77, $DF, $FF, $77, $BA, $BE, $AA, $FB // 
       .byte  $AE, $BA, $AA, $AA, $EB, $EF, $EB, $FA // 
       .byte  $FA, $FA, $FA, $EB, $FE, $FB, $FE, $FB // 
       .byte  $FE, $FE, $FE, $FF, $EF, $FF, $EF, $BF // 
       .byte  $BF, $FF, $EF, $BF, $55, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $A5, $69, $69, $E7, $FF // 
       .byte  $FF, $FF, $FF, $57, $55, $55, $D5, $F5 // 
       .byte  $FD, $FF, $FF, $FF, $FF, $BF, $FF, $FF // 
       .byte  $FF, $BF, $AB, $AA, $FF, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $FF, $A5, $A9, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $7F, $55 // 
       .byte  $D5, $FB, $EE, $BB, $FF, $FB, $EA, $AA // 
       .byte  $AA, $77, $DD, $77, $DF, $D7, $F5, $FD // 
       .byte  $FF, $FF, $FF, $FF, $FD, $F7, $DD, $55 // 
       .byte  $55, $FA, $FA, $FA, $DD, $77, $DD, $55 // 
       .byte  $55, $AA, $00, $00, $DD, $77, $DD, $55 // 
       .byte  $55, $AA, $80, $80, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $5F, $5F, $5F, $5F // 
       .byte  $5F, $5F, $5F, $5F, $00, $00, $00, $AA // 
       .byte  $20, $20, $AA, $00, $55, $7F, $7F, $7F // 
       .byte  $7F, $7F, $7F, $7F, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AB, $FF, $A5, $A5, $A5, $A5 // 
       .byte  $A7, $BF, $FF, $FA, $55, $00, $00, $0A // 
       .byte  $AA, $AF, $FF, $FF, $7F, $7F, $7F, $AA // 
       .byte  $AA, $FF, $FF, $FF, $55, $55, $55, $FF // 
       .byte  $FF, $99, $66, $99, $AA, $96, $5A, $6A // 
       .byte  $AA, $AA, $AA, $EA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $66, $99, $66, $AA // 
       .byte  $AA, $FF, $C3, $C3, $65, $99, $66, $AA // 
       .byte  $AA, $FD, $3D, $3D, $A9, $A5, $96, $5A // 
       .byte  $6A, $AA, $AE, $BB, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $EA, $C3, $C3, $C3, $FF // 
       .byte  $C0, $C0, $C0, $C0, $3D, $3C, $3E, $F9 // 
       .byte  $3A, $29, $AE, $89, $D1, $47, $DC, $DD // 
       .byte  $33, $FD, $7F, $33, $2A, $BA, $2A, $BA // 
       .byte  $EA, $FA, $2A, $BE, $55, $40, $43, $FF // 
       .byte  $FF, $BF, $AB, $AA, $77, $DC, $3F, $17 // 
       .byte  $FC, $FF, $FF, $BF, $FF, $BB, $CC, $EE // 
       .byte  $FB, $AE, $FA, $FF, $FA, $8E, $EE, $3B // 
       .byte  $EE, $EE, $EA, $FA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $A0, $00, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $2A, $02, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $F0, $FC, $F0, $00 // 
       .byte  $C3, $CF, $CF, $3F, $00, $02, $00, $CA // 
       .byte  $20, $A8, $C8, $C0, $20, $80, $00, $08 // 
       .byte  $80, $00, $20, $2A, $3F, $00, $A0, $08 // 
       .byte  $00, $02, $2A, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $A0, $80, $A0, $F0, $FF, $FF, $FF // 
       .byte  $C0, $00, $00, $00, $08, $02, $0A, $E8 // 
       .byte  $00, $00, $00, $0A, $A0, $80, $00, $00 // 
       .byte  $02, $02, $2B, $AF, $FC, $FC, $C0, $C3 // 
       .byte  $0F, $FF, $FF, $FF, $2A, $2A, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AB, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $A0, $AA, $AA, $AA, $AA // 
       .byte  $AA, $A0, $00, $0C, $AA, $AA, $AA, $AA // 
       .byte  $2A, $0A, $00, $00, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $0C, $CC, $00, $08, $20 // 
       .byte  $A0, $82, $00, $00, $00, $00, $00, $05 // 
       .byte  $13, $44, $15, $15, $00, $14, $41, $0D // 
       .byte  $51, $D5, $55, $55, $17, $5F, $5F, $57 // 
       .byte  $5F, $5F, $77, $7F, $03, $33, $3F, $CF // 
       .byte  $FF, $FF, $EA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AB, $BF, $FF, $FF, $AA, $AA, $AB, $FB // 
       .byte  $FF, $FF, $FF, $FF, $FF, $AF, $FF, $FF // 
       .byte  $BF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $65, $55, $55, $55 // 
       .byte  $55, $59, $55, $00, $9B, $54, $54, $64 // 
       .byte  $94, $54, $54, $00, $7B, $AA, $AA, $BA // 
       .byte  $AA, $A0, $8A, $2A, $76, $54, $54, $50 // 
       .byte  $05, $15, $47, $45, $FF, $FF, $FF, $BF // 
       .byte  $FC, $F3, $33, $0C, $54, $51, $45, $17 // 
       .byte  $55, $55, $75, $55, $3F, $CF, $F3, $F3 // 
       .byte  $FC, $75, $DA, $6A, $A2, $A2, $62, $A5 // 
       .byte  $5F, $FF, $FF, $FF, $CF, $CF, $CE, $CF // 
       .byte  $CF, $CF, $CF, $CF, $3E, $C9, $E5, $E5 // 
       .byte  $E5, $E5, $E5, $A5, $AA, $AA, $AA, $AE // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FB, $FF, $FF, $FF, $00, $45, $45, $4D // 
       .byte  $C5, $45, $45, $45, $9F, $1F, $9F, $9F // 
       .byte  $9F, $9F, $9F, $9F, $FF, $FF, $BF, $FF // 
       .byte  $FF, $FF, $FF, $FE, $FF, $EF, $FF, $FF // 
       .byte  $FF, $BF, $FF, $FF, $EE, $AA, $AA, $0A // 
       .byte  $A0, $A8, $A3, $A2, $67, $A8, $A8, $A0 // 
       .byte  $A0, $8A, $02, $A8, $77, $55, $55, $55 // 
       .byte  $5D, $57, $55, $00, $76, $54, $54, $54 // 
       .byte  $54, $54, $D0, $00, $CF, $CF, $CF, $AF // 
       .byte  $5A, $55, $55, $55, $FF, $FF, $FE, $EC // 
       .byte  $F3, $A3, $5B, $56, $3F, $0F, $33, $E8 // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AE // 
       .byte  $2A, $8A, $83, $20, $55, $55, $55, $55 // 
       .byte  $55, $75, $55, $75, $55, $55, $55, $55 // 
       .byte  $55, $55, $75, $55, $BC, $63, $5B, $5B // 
       .byte  $5A, $5B, $5B, $5B, $A2, $A2, $B2, $E2 // 
       .byte  $A2, $A2, $A2, $A2, $55, $55, $5D, $55 // 
       .byte  $55, $57, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $57, $55, $55, $5E, $5C, $5E, $5E // 
       .byte  $5E, $5E, $5E, $5E, $40, $11, $51, $53 // 
       .byte  $51, $54, $54, $54, $67, $AA, $AA, $AA // 
       .byte  $AA, $A6, $AA, $00, $ED, $A8, $A8, $B8 // 
       .byte  $E8, $A8, $A8, $00, $E6, $55, $55, $65 // 
       .byte  $55, $55, $55, $00, $ED, $FD, $FC, $FC // 
       .byte  $BC, $FC, $F0, $00, $FF, $FF, $FF, $7F // 
       .byte  $FF, $FF, $7F, $00, $3F, $3F, $3E, $3F // 
       .byte  $3F, $0F, $03, $00, $FE, $FF, $FE, $BF // 
       .byte  $EF, $FC, $FC, $00, $3F, $3F, $3F, $3A // 
       .byte  $3F, $3F, $3F, $00, $FF, $FF, $EE, $FF // 
       .byte  $FF, $FF, $FF, $FF, $F3, $F3, $FC, $FC // 
       .byte  $FF, $FF, $FF, $82, $FE, $FF, $FF, $FF // 
       .byte  $3F, $3F, $3E, $80, $F8, $FC, $FC, $FC // 
       .byte  $FC, $FC, $F0, $00, $00, $FF, $FF, $FB // 
       .byte  $BF, $FF, $FF, $FF, $00, $0F, $CF, $CF // 
       .byte  $CF, $CF, $CF, $C0, $0F, $FF, $FF, $FE // 
       .byte  $FC, $FC, $F8, $03, $C8, $2A, $2A, $2A // 
       .byte  $AE, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FD, $F5, $D5, $55, $55, $55, $55, $D5 // 
       .byte  $D5, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FE, $FA, $EA, $EA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FE, $FE // 
       .byte  $FE, $FA, $F6, $F5, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FC, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FC, $C0, $00, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FA, $EA, $EA, $E9 // 
       .byte  $E9, $A9, $AA, $AA, $E0, $F4, $FD, $FF // 
       .byte  $FF, $FF, $FF, $FD, $00, $00, $00, $C0 // 
       .byte  $C0, $B0, $B0, $B0, $FF, $FE, $FE, $FE // 
       .byte  $F5, $F5, $D5, $D5, $DD, $5F, $FF, $FF // 
       .byte  $FF, $BF, $AF, $A5, $AF, $EE, $AE, $BE // 
       .byte  $FA, $EA, $AB, $FC, $B0, $B0, $B0, $B0 // 
       .byte  $C0, $C0, $00, $00, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FC // 
       .byte  $C0, $00, $00, $00, $FE, $FA, $2A, $2A // 
       .byte  $0A, $0A, $02, $02, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $7F, $5F, $5F // 
       .byte  $57, $57, $57, $57, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $03, $0F, $0F, $0F // 
       .byte  $3F, $3B, $3B, $EF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FE, $EB, $FD, $FD, $F5, $F5 // 
       .byte  $D5, $55, $FF, $FF, $00, $00, $00, $00 // 
       .byte  $00, $03, $0F, $FF, $EA, $A9, $AA, $AA // 
       .byte  $AA, $AA, $AA, $69, $EA, $AA, $AA, $AB // 
       .byte  $AF, $BF, $FF, $FF, $AA, $AA, $56, $5A // 
       .byte  $5A, $5A, $69, $A5, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FE, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $EF, $EF // 
       .byte  $FF, $BF, $BF, $FF, $AA, $AA, $AA, $AE // 
       .byte  $AA, $AA, $AE, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $57, $55 // 
       .byte  $55, $5D, $55, $55, $D5, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $75, $55, $55 // 
       .byte  $75, $55, $55, $51, $FF, $FF, $FE, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FC, $FF, $FF, $FF // 
       .byte  $FF, $BF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $F3, $FF, $FF, $CF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FB, $FF, $FF, $BF, $FF, $FF // 
       .byte  $FF, $FF, $FC, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $BA, $FF, $FF, $EF, $EF // 
       .byte  $FF, $EF, $FF, $FB, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FB, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $65, $55, $59, $55 // 
       .byte  $55, $55, $55, $55, $FB, $FF, $FF, $FE // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $BF, $FF, $BF, $55, $D5, $55, $55 // 
       .byte  $55, $55, $55, $55, $FF, $3F, $FE, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FB, $FF, $FF // 
       .byte  $FF, $FE, $FF, $FF, $55, $55, $55, $55 // 
       .byte  $75, $55, $5D, $55, $FF, $FF, $F3, $FF // 
       .byte  $FF, $FF, $FF, $FF, $BF, $FF, $FF, $FF // 
       .byte  $FF, $FB, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $57, $55, $55, $75, $55 // 
       .byte  $55, $D5, $55, $55, $55, $56, $56, $56 // 
       .byte  $46, $57, $5B, $5B, $FF, $FF, $FD, $E9 // 
       .byte  $99, $AA, $AE, $AB, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $BA, $AA, $55, $55, $54, $55 // 
       .byte  $55, $55, $55, $45, $5B, $5F, $6F, $6F // 
       .byte  $6F, $BF, $BF, $BD, $AA, $AA, $AA, $29 // 
       .byte  $A6, $9A, $6A, $88, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $8A, $AA // 
       .byte  $A2, $98, $AA, $A6, $EA, $EA, $EA, $7E // 
       .byte  $57, $55, $45, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $56, $55, $55, $BF, $FF, $FF, $BF // 
       .byte  $FF, $FB, $FF, $FF, $57, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $AA, $EA, $AA, $BA // 
       .byte  $AA, $AB, $AA, $AA, $AA, $2A, $8A, $AA // 
       .byte  $A2, $AA, $E8, $AB, $6F, $9B, $5B, $56 // 
       .byte  $55, $95, $05, $95, $FF, $CF, $FF, $FF // 
       .byte  $FF, $FF, $BF, $7F, $55, $55, $55, $75 // 
       .byte  $55, $55, $15, $57, $55, $55, $55, $5D // 
       .byte  $55, $55, $55, $55, $85, $6B, $AA, $AA // 
       .byte  $28, $AA, $AA, $AA, $BF, $9F, $AF, $AF // 
       .byte  $A7, $AB, $AB, $E9, $FF, $FF, $FF, $FF // 
       .byte  $CF, $FF, $FF, $FF, $5D, $55, $55, $55 // 
       .byte  $55, $D5, $55, $55, $3F, $F3, $FF, $FF // 
       .byte  $FF, $FD, $55, $55, $3F, $FF, $FF, $F5 // 
       .byte  $55, $51, $56, $55, $55, $55, $47, $55 // 
       .byte  $55, $75, $55, $55, $55, $55, $55, $55 // 
       .byte  $75, $55, $55, $55, $FC, $FF, $FF, $3F // 
       .byte  $FB, $FF, $EF, $FF, $FB, $FF, $BF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $EF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $BF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $2A, $AA, $AB, $FD, $55, $55, $D5 // 
       .byte  $D5, $F5, $55, $56, $AA, $AB, $AF, $BF // 
       .byte  $AF, $BF, $FF, $F7, $AA, $AE, $AA, $AA // 
       .byte  $EA, $AA, $AA, $AA, $AB, $AB, $AF, $AB // 
       .byte  $AA, $AB, $AB, $AF, $FF, $7F, $5F, $55 // 
       .byte  $55, $57, $5F, $5E, $FF, $7F, $FF, $BF // 
       .byte  $F7, $FF, $7F, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FA, $AA, $5A, $A5, $EA, $E9, $EA, $AA // 
       .byte  $AA, $A9, $6A, $5A, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AE, $AA, $AB, $AA // 
       .byte  $AA, $AA, $AA, $AA, $55, $55, $55, $55 // 
       .byte  $75, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $55, $55, $57, $55 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $DF, $FF, $FF // 
       .byte  $FE, $FF, $FD, $FF, $D5, $F5, $F5, $D5 // 
       .byte  $55, $D5, $FD, $FD, $EA, $FE, $FF, $FF // 
       .byte  $FA, $FE, $FF, $FF, $FF, $FF, $BF, $FF // 
       .byte  $FF, $FF, $FF, $BF, $55, $D5, $55, $55 // 
       .byte  $55, $55, $5D, $55, $DF, $FF, $FA, $EA // 
       .byte  $AA, $EA, $FA, $FF, $55, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $5F, $5F, $57, $57 // 
       .byte  $55, $55, $55, $55, $FF, $FF, $FF, $FF // 
       .byte  $EF, $FF, $FF, $FF, $65, $55, $55, $55 // 
       .byte  $55, $55, $55, $55, $AF, $AB, $6A, $AA // 
       .byte  $A6, $A9, $AA, $A5, $FF, $FF, $FF, $BF // 
       .byte  $AF, $AB, $AA, $AA, $FB, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $BF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FD, $55, $55, $55 // 
       .byte  $55, $75, $55, $55, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $AA, $AA, $AA, $AA, $AA, $AA, $AA // 
       .byte  $AA, $69, $66, $5A, $FD, $FD, $F5, $DD // 
       .byte  $75, $F5, $7D, $75, $AA, $AA, $AA, $6A // 
       .byte  $6A, $69, $66, $5A, $FF, $FD, $F7, $DD // 
       .byte  $7D, $F7, $F7, $F7, $EE, $EE, $EB, $EE // 
       .byte  $FA, $EE, $EE, $EB, $99, $65, $A5, $69 // 
       .byte  $65, $99, $65, $A5, $5F, $77, $7D, $7F // 
       .byte  $7F, $5D, $7D, $77, $EE, $EF, $BF, $BF // 
       .byte  $AF, $FB, $FE, $FF, $BB, $AF, $BB, $BB // 
       .byte  $BE, $B9, $A5, $95, $7D, $76, $DA, $6B // 
       .byte  $AB, $AB, $AF, $BF, $77, $77, $5F, $5F // 
       .byte  $7D, $76, $5A, $6B, $FA, $F9, $E5, $95 // 
       .byte  $55, $55, $7F, $FF, $AA, $AA, $AB, $AA // 
       .byte  $BF, $FF, $FF, $FF, $AB, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $AA, $AA, $AA, $AA // 
       .byte  $AA, $A7, $9E, $AA, $AA, $AB, $B6, $A9 // 
       .byte  $66, $B9, $DA, $65, $AA, $BA, $D9, $AE // 
       .byte  $7B, $EE, $E6, $99, $FF, $FF, $FF, $7B // 
       .byte  $EF, $BB, $EE, $BF, $66, $5B, $65, $D6 // 
       .byte  $59, $66, $65, $9A, $96, $A9, $F6, $BE // 
       .byte  $6F, $9B, $6A, $9A, $AA, $67, $AF, $BE // 
       .byte  $F9, $EA, $FA, $F9, $EB, $7E, $FA, $EE // 
       .byte  $FF, $EB, $BE, $FB, $9A, $69, $9A, $A5 // 
       .byte  $AA, $A9, $AA, $AA, $7E, $FF, $7E, $FE // 
       .byte  $FE, $FE, $FF, $FF, $BF, $BD, $BF, $BD // 
       .byte  $FF, $BF, $AF, $AF, $EF, $BB, $EE, $FB // 
       .byte  $EB, $EF, $FF, $FF, $55, $55, $55, $55 // 
       .byte  $57, $57, $7F, $FD, $55, $56, $6A, $6B // 
       .byte  $AC, $B3, $CF, $FF, $FD, $FF, $EA, $AB // 
       .byte  $2A, $AA, $A5, $95, $55, $95, $A5, $F9 // 
       .byte  $F9, $79, $DE, $95, $70, $07, $0E, $70 // 
       .byte  $07, $0E, $E0, $03, $8E, $E0, $03, $8E // 
       .byte  $C0, $01, $8E, $C0, $01, $8E, $C0, $01 // 
       .byte  $8E, $C0, $01, $84, $C1, $41, $84, $40 // 
       .byte  $81, $04, $41, $41, $04, $26, $32, $04 // 
       .byte  $28, $0A, $04, $10, $04, $04, $20, $02 // 
       .byte  $04, $60, $03, $04, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $38, $03, $80, $38 // 
       .byte  $03, $80, $38, $03, $80, $38, $03, $80 // 
       .byte  $38, $03, $80, $38, $03, $80, $38, $03 // 
       .byte  $80, $10, $01, $00, $10, $01, $00, $10 // 
       .byte  $01, $00, $10, $01, $00, $10, $01, $00 // 
       .byte  $10, $01, $00, $10, $31, $03, $10, $F1 // 
       .byte  $0F, $3F, $F3, $FF, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $07, $03, $80, $08 // 
       .byte  $83, $80, $08, $87, $00, $10, $47, $00 // 
       .byte  $10, $46, $00, $10, $46, $00, $30, $66 // 
       .byte  $00, $30, $66, $00, $30, $66, $0A, $30 // 
       .byte  $62, $04, $10, $42, $0A, $10, $41, $31 // 
       .byte  $10, $41, $40, $08, $80, $80, $08, $81 // 
       .byte  $00, $07, $03, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $38, $01, $F0, $38 // 
       .byte  $01, $C8, $1C, $01, $C4, $1C, $01, $C4 // 
       .byte  $0C, $01, $C2, $0C, $01, $C2, $0C, $01 // 
       .byte  $C1, $0C, $00, $81, $0C, $00, $81, $08 // 
       .byte  $00, $81, $08, $00, $81, $90, $00, $C1 // 
       .byte  $50, $00, $A1, $20, $00, $A2, $10, $00 // 
       .byte  $94, $18, $00, $88, $00, $00, $80, $00 // 
       .byte  $00, $80, $00, $00, $80, $00, $00, $80 // 
       .byte  $00, $00, $80, $FF, $02, $23, $FF, $01 // 
       .byte  $43, $F8, $00, $83, $38, $01, $40, $38 // 
       .byte  $02, $20, $38, $04, $10, $38, $08, $08 // 
       .byte  $38, $08, $08, $10, $90, $04, $10, $90 // 
       .byte  $04, $10, $98, $FC, $10, $1F, $0C, $10 // 
       .byte  $3C, $1C, $10, $5C, $1C, $10, $0E, $38 // 
       .byte  $10, $0E, $38, $10, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $BF, $FF, $FF, $3F // 
       .byte  $82, $0F, $33, $82, $03, $03, $82, $00 // 
       .byte  $03, $82, $00, $03, $82, $00, $03, $82 // 
       .byte  $00, $01, $02, $18, $01, $03, $E0, $01 // 
       .byte  $1F, $80, $01, $1F, $00, $01, $17, $00 // 
       .byte  $01, $07, $00, $01, $07, $03, $01, $07 // 
       .byte  $0F, $01, $0F, $FF, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $3C, $0E, $01, $03 // 
       .byte  $0E, $01, $00, $8E, $01, $00, $4F, $01 // 
       .byte  $E0, $4F, $01, $E0, $8F, $81, $E1, $0E // 
       .byte  $C1, $46, $04, $61, $58, $04, $21, $48 // 
       .byte  $04, $11, $46, $04, $11, $43, $04, $09 // 
       .byte  $43, $04, $09, $41, $84, $07, $41, $C4 // 
       .byte  $03, $41, $C4, $03, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $AD, $87, $40, $85 // 
       .byte  $57, $A9, $99, $85, $58, $CE, $7E, $40 // 
       .byte  $10, $15, $A9, $15, $8D, $7E, $40, $AC // 
       .byte  $7F, $40, $88, $10, $02, $A0, $03, $8C // 
       .byte  $7F, $40, $B1, $57, $8D, $FA, $47, $60 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $75, $76, $02, $07 // 
       .byte  $60, $79, $9C, $79, $60, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $15, $00, $00 // 
       .byte  $0A, $00, $00, $1D, $00, $00, $36, $00 // 
       .byte  $00, $1C, $00, $00, $2A, $00, $00, $55 // 
       .byte  $00, $00, $6B, $80, $00, $D5, $80, $10 // 
       .byte  $2B, $80, $18, $7E, $00, $0F, $F5, $00 // 
       .byte  $03, $AA, $00, $00, $15, $00, $00, $2B // 
       .byte  $00, $00, $33, $00, $00, $37, $00, $00 // 
       .byte  $73, $80, $00, $70, $00, $00, $30, $00 // 
       .byte  $00, $78, $00, $00, $00, $2A, $00, $00 // 
       .byte  $75, $00, $00, $2A, $00, $00, $7D, $00 // 
       .byte  $00, $38, $00, $00, $2A, $00, $00, $57 // 
       .byte  $00, $00, $AF, $00, $01, $D7, $00, $01 // 
       .byte  $AF, $00, $00, $DE, $00, $00, $BA, $00 // 
       .byte  $00, $E4, $00, $01, $EA, $00, $03, $D7 // 
       .byte  $00, $07, $27, $00, $1C, $67, $00, $00 // 
       .byte  $EE, $00, $00, $E6, $00, $00, $72, $00 // 
       .byte  $00, $E0, $00, $00, $00, $A8, $00, $01 // 
       .byte  $5C, $20, $00, $AE, $30, $01, $7C, $10 // 
       .byte  $00, $38, $18, $00, $DC, $18, $01, $AA // 
       .byte  $30, $03, $55, $30, $01, $AB, $E0, $01 // 
       .byte  $D7, $C0, $00, $AA, $80, $00, $54, $00 // 
       .byte  $00, $AA, $00, $00, $D6, $00, $01, $CC // 
       .byte  $00, $01, $CC, $00, $01, $EC, $00, $00 // 
       .byte  $CE, $00, $01, $8E, $00, $00, $1C, $00 // 
       .byte  $00, $1C, $00, $00, $00, $A8, $C0, $01 // 
       .byte  $58, $60, $00, $AC, $70, $01, $78, $30 // 
       .byte  $00, $38, $30, $00, $54, $70, $01, $AA // 
       .byte  $30, $01, $D7, $60, $01, $AB, $E0, $01 // 
       .byte  $F5, $C0, $00, $E8, $80, $01, $54, $00 // 
       .byte  $00, $A8, $00, $00, $5C, $00, $01, $EC // 
       .byte  $00, $03, $CC, $00, $01, $CE, $00, $01 // 
       .byte  $E6, $00, $00, $CE, $00, $01, $8C, $00 // 
       .byte  $00, $0E, $00, $00, $00, $15, $00, $00 // 
       .byte  $1A, $00, $00, $35, $00, $00, $1A, $00 // 
       .byte  $00, $1C, $00, $00, $2A, $00, $00, $75 // 
       .byte  $00, $00, $7A, $80, $00, $35, $00, $10 // 
       .byte  $5A, $80, $18, $DD, $00, $0F, $DA, $00 // 
       .byte  $03, $9D, $00, $00, $2A, $00, $00, $15 // 
       .byte  $00, $00, $2A, $80, $00, $33, $80, $00 // 
       .byte  $73, $80, $00, $77, $00, $00, $30, $00 // 
       .byte  $00, $70, $00, $00, $00, $2A, $00, $00 // 
       .byte  $54, $00, $00, $6A, $00, $00, $74, $00 // 
       .byte  $00, $38, $00, $00, $2A, $00, $00, $55 // 
       .byte  $00, $00, $AA, $80, $01, $D5, $00, $01 // 
       .byte  $AA, $00, $00, $D4, $00, $00, $AA, $00 // 
       .byte  $00, $54, $00, $01, $AE, $00, $03, $57 // 
       .byte  $00, $07, $67, $00, $1C, $67, $00, $00 // 
       .byte  $E3, $80, $00, $F3, $80, $00, $73, $00 // 
       .byte  $00, $E0, $00, $00, $00, $50, $00, $00 // 
       .byte  $AC, $20, $00, $56, $30, $00, $BC, $10 // 
       .byte  $00, $78, $18, $00, $5C, $18, $01, $AA // 
       .byte  $30, $03, $55, $70, $03, $AA, $E0, $03 // 
       .byte  $55, $C0, $07, $29, $80, $00, $54, $00 // 
       .byte  $00, $A8, $00, $00, $D4, $00, $00, $EC // 
       .byte  $00, $01, $EC, $00, $03, $EC, $00, $01 // 
       .byte  $9E, $00, $00, $8E, $00, $00, $1C, $00 // 
       .byte  $00, $1C, $00, $00, $00, $2A, $30, $00 // 
       .byte  $15, $18, $00, $2B, $88, $00, $17, $0C // 
       .byte  $00, $0A, $0C, $00, $15, $1C, $00, $2A // 
       .byte  $8C, $00, $55, $D8, $00, $6A, $F8, $00 // 
       .byte  $D5, $70, $00, $EA, $00, $00, $95, $00 // 
       .byte  $00, $2A, $00, $00, $15, $00, $00, $3B // 
       .byte  $00, $00, $3F, $00, $00, $7B, $80, $00 // 
       .byte  $F3, $80, $00, $E3, $00, $00, $73, $00 // 
       .byte  $00, $13, $80, $00, $A6, $36, $00, $F0 // 
       .byte  $C0, $F0, $30, $00, $30, $FE, $F0, $30 // 
       .byte  $06, $36, $06, $D0, $06, $06, $00, $00 // 
       .byte  $0E, $1E, $06, $06, $0E, $06, $06, $06 // 
       .byte  $F6, $06, $06, $06, $00, $06, $8E, $00 // 
       .byte  $06, $00, $00, $46, $46, $00, $00, $C0 // 
       .byte  $06, $06, $F6, $00, $06, $06, $06, $B6 // 
       .byte  $00, $06, $06, $06, $F6, $A6, $06, $00 // 
       .byte  $06, $06, $FE, $06, $06, $5C, $36, $00 // 
       .byte  $F6, $CE, $0E, $A0, $06, $1E, $0E, $1E // 
       .byte  $06, $C7, $A7, $00, $00, $F0, $30, $30 // 
       .byte  $30, $1E, $FE, $06, $6E, $3E, $1E, $FE // 
       .byte  $00, $F7, $37, $1E, $06, $07, $07, $07 // 
       .byte  $06, $01, $00, $00, $06, $1E, $1E, $1E // 
       .byte  $06, $00, $00, $30, $07, $07, $07, $00 // 
       .byte  $1E, $01, $01, $00, $1C, $00, $1C, $00 // 
       .byte  $10, $1C, $00, $00, $06, $1E, $0E, $1E // 
       .byte  $00, $06, $00, $1E, $06, $06, $06, $06 // 
       .byte  $06, $06, $06, $36, $1C, $1C, $1C, $06 // 
       .byte  $00, $00, $00, $06, $06, $06, $00, $06 // 
       .byte  $06, $00, $00, $00, $06, $06, $06, $06 // 
       .byte  $00, $F6, $06, $00, $00, $00, $50, $E0 // 
       .byte  $00, $00, $00, $D0, $06, $36, $C6, $00 // 
       .byte  $06, $00, $06, $06, $00, $00, $00, $00 // 
       .byte  $00, $00, $30, $00 // 
D3CC0: .byte  $20, $24, $C0, $C4, $60, $64, $00, $04 // 
       .byte  $A0, $A4, $40, $44 // 
D3CCC: .byte  $D8, $D8, $D8, $D8, $D9, $D9, $DA, $DA // 
       .byte  $DA, $DA, $DB, $DB, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
L3D00: jsr  L9D00    // 
      lda  D43F8    // 
      sta  L0E0B+1  // 
      lda  D43F1    // 
      sta $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda  D43F0    // 
      clc           // 
      adc  D43F7    // 
      sta $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda  D43F2    // 
      sta current_screen      // 
      lda #$12      // 
      sta $D01C     // Sprites 0-7 Multi-Color Mode Select:  1 = M.C.M.  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$01      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jsr  L0A00    // 
      jsr  L9300    // 
      lda #$07      // 
      sta $D027     // Sprite 0 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jmp  L9936    // 
D3D36: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00 // 
L3DA0: lda #$0F      // 
      sta $D418     // Select Filter Mode and Volume 7 Cut-Off Voice 3 Output: 1 = Off, 0 = On   6 Select Filter High-Pass Mode: 1 = On 5 Select Filter Band-Pass Mode: 1 = On 4 Select Filter Low-Pass Mode: 1 = On 3-0 Select Output Volume: 0-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      rts           // 
D3DA6: .byte  $EA, $EA, $A9, $05, $8D, $B5, $3D, $A9 // 
       .byte  $FF, $8D, $B4, $3D, $60, $06, $FE, $05 // 
       .byte  $CE, $B4, $3D, $D0, $F7, $CE, $B5, $3D // 
       .byte  $10, $F2, $A9, $05, $8D, $B5, $3D, $AC // 
       .byte  $B3, $3D, $C8, $C0, $10, $90, $02, $A0 // 
       .byte  $0F, $8C, $18, $D4, $8C, $B3, $3D, $60 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00 // 
L3E00: lda $C5       // 
      cmp #$1D      // 
      bne  L3E18    // 
L3E06: lda $C5       // 
      cmp #$40      // 
      bne  L3E06    // 
L3E0C: lda $C5       // 
      cmp #$16      // 
      bne  L3E0C    // 
L3E12: lda $C5       // 
      cmp #$40      // 
      bne  L3E12    // 
L3E18: rts           // 
D3E19: .byte  $00, $00, $00, $00, $00, $00, $00 // 
L3E20: ldx #$00      // 
      stx $59       // 
      lda #$3C      // 
      sta $5A       // 
L3E28: lda  D3CC0,X  // 
      sta $57       // 
      lda  D3CCC,X  // 
      sta $58       // 
      stx $02       // 
      lda #$03      // 
      sta $FF       // 
L3E38: ldy #$00      // 
      ldx #$00      // 
L3E3C: lda ($59,X)   // 
      sta ($57),Y   // 
      inc $59       // 
      iny           // 
      cpy #$04      // 
      bne  L3E3C    // 
      dec $FF       // 
      bmi  L3E52    // 
      lda #$28      // 
      jsr  L3E5A    // 
      bcs  L3E38    // 
L3E52: ldx $02       // 
      inx           // 
      cpx #$0C      // 
      bne  L3E28    // 
      rts           // 
L3E5A: clc           // 
      adc $57       // 
      sta $57       // 
      lda $58       // 
      adc #$00      // 
      sta $58       // 
      sec           // 
      rts           // 
D3E67: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00 // 
L3E94: rts           // 
L3E95: lda $028D     // SHFLAG Flag: Keyb'rd SHIFT Key/CTRL Key/C= Key 
      and #$03      // 
      cmp #$03      // 
      bne  L3E94    // 
      pla           // 
      pla           // 
L3EA0: lda #$00      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$41      // 
      sta current_screen      // 
      jsr  L0A00    // 
      jsr  L9DBC    // 
      lda #$01      // 
      sta  DF4E     // 
      jsr  L0EDA    // 
L3EB7: lda $DC00     // Data Port A (Keyboard, Joystick, Paddles,  Light-Pen)   7-0 Write Keyboard Column Values for Keyboard  Scan 7-6-2006 Read Paddles on Port A / B (01 = Port A,  10 = Port B) 4 Joystick A Fire Button: 1 = Fire 3-2-2006 Paddle Fire Buttons 3-0 Joystick A Direction (0-15)    MOS 6526 Complex Interface Adapter (CIA) #2
      and #$10      // 
      beq  L3EC4    // 
      lda $C5       // 
      cmp #$40      // 
      beq  L3EB7    // 
L3EC4: jsr  L9D00    // 
      sec           // 
      jsr  L96C0    // 
      ldx #$F6      // 
      txs           // 
      jmp  L0930    // 
D3ED1: .byte  $9A, $20, $00, $9D, $4C, $30, $09, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $A9 // 
       .byte  $01, $8D, $4E, $0F, $20, $DA, $0E, $60 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $02 // 
       .byte  $22, $22, $22, $22, $22, $22, $02, $0A // 
       .byte  $8A, $8A, $8A, $8A, $8A, $8A, $02, $02 // 
       .byte  $A2, $A2, $02, $2A, $2A, $22, $02, $02 // 
       .byte  $22, $A2, $82, $A2, $A2, $22, $02, $2A // 
       .byte  $2A, $2A, $22, $22, $02, $A2, $A2, $02 // 
       .byte  $2A, $2A, $02, $A2, $A2, $22, $02, $02 // 
       .byte  $2A, $2A, $02, $22, $22, $22, $02, $02 // 
       .byte  $22, $A2, $A2, $A2, $A2, $A2, $A2, $02 // 
       .byte  $22, $22, $8A, $22, $22, $22, $02, $02 // 
       .byte  $22, $22, $02, $A2, $A2, $A2, $02 // 
D3F50: .byte  $A9, $65, $65, $65, $65, $65, $65, $55 // 
       .byte  $99, $99, $99, $A9, $99, $99, $99, $55 // 
       .byte  $A9, $95, $95, $A5, $95, $95, $A9, $55 // 
D3F68: .byte  $65, $65, $99, $99, $A9, $99, $99, $55 // 
       .byte  $A5, $99, $99, $99, $99, $99, $A5, $55 // 
       .byte  $99, $99, $99, $99, $99, $65, $65, $55 // 
       .byte  $A9, $95, $95, $A5, $95, $95, $A9, $55 // 
       .byte  $A9, $99, $99, $99, $99, $99, $99, $55 // 
       .byte  $A9, $65, $65, $65, $65, $65, $65, $55 // 
       .byte  $99, $99, $99, $99, $99, $99, $A9, $55 // 
       .byte  $A5, $99, $99, $A5, $99, $99, $99, $55 // 
       .byte  $A9, $95, $95, $A5, $95, $95, $A9, $55 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
/**
treasure positions       
**/       
D3FC0: .byte  $79, $00, $00, $00, $00, $B5, $00, $00 // 
       .byte  $00, $53, $00, $00, $39, $B9, $93, $00 // 
       .byte  $93, $73, $33, $D9, $B3, $00, $00, $00 // 
       .byte  $00, $73, $00, $B9, $00, $B5, $53, $00 // 
       .byte  $D5, $00, $97, $00, $37, $37, $35, $00 // 
       .byte  $00, $95, $75, $00, $39, $00, $00, $00 // 
       .byte  $00, $00, $D9, $00, $00, $77, $33, $00 // 
       .byte  $00, $00, $35, $75, $97, $00, $00, $00 // 
       .byte  $22, $23, $20, $21, $26, $27, $24, $25 // 
       .byte  $2A, $2B, $28, $29, $00, $00, $00, $00 // 
       .byte  $38, $39, $3A, $3B, $00, $00, $00, $00 // 
       .byte  $00, $00, $01, $00, $01, $00, $00, $00 // 
       .byte  $3C, $3D, $3E, $3F, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
D4030: .byte  $00, $08, $10, $18, $20, $28, $30, $38 // 
       .byte  $40, $48, $00, $00, $90, $98, $A0, $00 // 
D4040: .byte  $30, $31, $32, $33, $00, $00, $00, $00 // 
D4048: .byte  $10, $20, $00, $00, $38, $3C, $20, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
D4060: .byte  $30, $32, $33 // 
D4063: .byte  $48, $92, $88, $92, $C8, $92, $00, $00 // 
       .byte  $00, $00, $00, $00, $00 // 
D4070: .byte  $00, $00, $45, $00 // 
D4074: .byte  $43, $98, $94, $00 // 
D4078: .byte  $7A, $7B, $7C, $7D, $7E, $7F // 
D407E: .byte  $08 // 
D407F: .byte  $01 // 
D4080: .byte  $01, $03, $08, $07, $05, $0F, $00 // 
D4087: .byte  $F8 // 
D4088: .byte  $F4, $F8, $FC, $00 // 
D408C: .byte  $08, $0C, $0A, $00 // 
D4090: .byte  $05, $01, $01, $02, $02, $00, $00, $00 // 
D4098: .byte  $F0, $59, $38, $59 // 
D409C: .byte  $FA, $5B // 
D409E: .byte  $30, $02 // 
D40A0: .byte  $0A, $F4, $0A, $F4 // 
D40A4: .byte  $00, $00, $01, $01 // 
D40A8: .byte  $79, $7F, $77, $78, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $60, $D8, $60, $D8, $69, $68, $1B, $90 // 
       .byte  $F0, $90, $00, $00, $00, $00, $00, $00 // 
       .byte  $60, $D8, $20, $90, $F0, $60, $F0, $A0 // 
       .byte  $0F, $03, $07, $05, $03, $00, $00, $00 // 
       .byte  $60, $38, $3C, $38, $3C, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
D4100: .byte  $10, $11, $12, $13, $14, $15, $16, $17 // 
       .byte  $18, $19, $1A, $1B, $1C, $1D, $1E, $1F // 
       .byte  $20, $21, $22, $23, $24, $25, $26, $27 // 
       .byte  $28, $29, $2A, $2B, $2C, $2D, $2E, $2F // 
       .byte  $30, $31, $32, $33, $34, $35, $36, $37 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
D4130: .byte  $10, $20, $FE, $E2 // 
D4134: .byte  $FA, $E0, $15, $26 // 
D4138: .byte  $E5, $E5, $65, $65 // 
D413C: .byte  $38, $38, $18, $18 // 
D4140: .byte  $01, $08, $00, $00, $00, $00, $00, $00 // 
       .byte  $01, $02, $02, $08, $10, $20, $40, $80 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
//write copyright text on bitmap       
L4160: ldx #$4F      // 
L4162: lda  D8A0,X   // 
      sta  start_bitmap_mem+$1848+320,X  // 
      dex           // 
      bpl  L4162    // 
      ldx #$0A      // 
      lda #$03      // 
L416F: sta  D4708+40,X  // 
      dex           // 
      bpl  L416F    // 
      rts           // 
D4176: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00 // 
/* enemy sprite positions per screen       
	higher four bits + 16 -> sprite x position   
	lower four bits x 16 + 40 -> sprite y position 
*/       
D41C0: .byte  $77, $99, $19, $19, $E5, $59, $59, $55 // 
       .byte  $73, $E9, $55, $E9, $53, $59, $73, $77 // 
       .byte  $D1, $15, $95, $79, $17, $E5, $D5, $33 // 
       .byte  $99, $B5, $33, $53, $13, $B9, $19, $13 // 
       .byte  $00, $E3, $E3, $E3, $B3, $E3, $DB, $77 // 
       .byte  $B1, $99, $53, $B1, $13, $E9, $95, $33 // 
       .byte  $E9, $13, $13, $BB, $B9, $E3, $37, $37 // 
       .byte  $93, $93, $00, $53, $00, $77, $E3, $37 // 
D4200: .byte  $00, $20, $40, $60, $80, $A0, $C0, $E0 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
D4210: .byte  $60, $60, $60, $60, $60, $60, $60, $60 // 
       .byte  $65, $65, $65, $65, $65, $65, $65, $65 // 
       .byte  $6A, $6A, $6A, $6A, $6A, $6A, $6A, $6A // 
       .byte  $6F, $6F, $6F, $6F, $6F, $6F, $6F, $6F // 
       .byte  $74, $74, $74, $74, $74, $74, $74, $74 // 
       .byte  $79, $79, $79, $79, $79, $79, $79, $79 // 
D4240: .byte  $00, $04, $08, $0C, $10, $14, $18, $1C // 
       .byte  $A0, $A4, $A8, $AC, $B0, $B4, $B8, $BC // 
       .byte  $40, $44, $48, $4C, $50, $54, $58, $5C // 
       .byte  $E0, $E4, $E8, $EC, $F0, $F4, $F8, $FC // 
       .byte  $80, $84, $88, $8C, $90, $94, $98, $9C // 
       .byte  $20, $24, $28, $2C, $30, $34, $38, $3C // 
D4270: .byte  $44, $44, $44, $44, $44, $44, $44, $44 // 
       .byte  $44, $44, $44, $44, $44, $44, $44, $44 // 
       .byte  $45, $45, $45, $45, $45, $45, $45, $45 // 
       .byte  $45, $45, $45, $45, $45, $45, $45, $45 // 
       .byte  $46, $46, $46, $46, $46, $46, $46, $46 // 
       .byte  $47, $47, $47, $47, $47, $47, $47, $47 // 
D42A0: .byte  $00, $10, $20, $30, $40, $50, $60, $70 // 
       .byte  $80, $90, $A0, $B0, $C0, $D0, $E0, $F0 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
/*location definitions 64 bytes
bit 7: enemy on screen?
bit 6: enemy has sword?
bit 3: sword 
*/

D42C0: .byte  $CB, $90, $C0, $C0, $C0, $9A, $E0, $E0 // 
       .byte  $C0, $CC, $E0, $E0, $ED, $9D, $CC, $90 // 
       .byte  $99, $ED, $EB, $C8, $9D, $90, $E0, $90 // 
       .byte  $E0, $EC, $C0, $EA, $90, $9D, $EB, $C0 // 
       .byte  $0D, $E0, $C9, $E0, $CD, $9C, $ED, $90 // 
       .byte  $90, $EC, $CD, $90, $9D, $C0, $90, $90 // 
       .byte  $C0, $90, $ED, $90, $90, $ED, $CB, $00 // 
       .byte  $90, $E0, $0D, $CA, $0D, $C0, $E0, $C0 // 
L4300: ldy #$03      // 
      ldx #$00      // 
      lda $DC00     // Data Port A (Keyboard, Joystick, Paddles,  Light-Pen)   7-0 Write Keyboard Column Values for Keyboard  Scan 7-6-2006 Read Paddles on Port A / B (01 = Port A,  10 = Port B) 4 Joystick A Fire Button: 1 = Fire 3-2-2006 Paddle Fire Buttons 3-0 Joystick A Direction (0-15)    MOS 6526 Complex Interface Adapter (CIA) #2
      lda $DC00     // Data Port A (Keyboard, Joystick, Paddles,  Light-Pen)   7-0 Write Keyboard Column Values for Keyboard  Scan 7-6-2006 Read Paddles on Port A / B (01 = Port A,  10 = Port B) 4 Joystick A Fire Button: 1 = Fire 3-2-2006 Paddle Fire Buttons 3-0 Joystick A Direction (0-15)    MOS 6526 Complex Interface Adapter (CIA) #2
L430A: lsr           // 
      bcc  L4312    // 
      dey           // 
      bpl  L430A    // 
      bmi  L4330    // 
L4312: lda  DFF0,Y   // 
      sta $0340     // 
      lda  DFF4,Y   // 
      sta $0341     // 
      sty  D4343    // 
      lda  DBF8,Y   // 
      sta  L4327+1  // 
L4327: jsr  L0B00    // 
      bcs  L432F    // 
      jsr  L4344    // 
L432F: rts           // 
L4330: ldy #$03      // 
      bit $3F       // 
      bpl  L4338    // 
      ldy #$01      // 
L4338: lda $C5       // 
      cmp  DBF4,Y   // 
      beq  L4312    // 
      dey           // 
      bpl  L4338    // 
      rts           // 
D4343: .byte  $00 // 
L4344: lda #$8C      // 
      sta $4F       // 
      dec  D436F    // 
      bpl  L436B    // 
      lda #$06      // 
      sta  D436F    // 
      ldy  D4370    // 
      dey           // 
      bpl  L435A    // 
      ldy #$03      // 
L435A: sty  D4370    // 
      lda $0340     // 
      clc           // 
      adc  D4371    // 
      sta $4E       // 
      lda ($4E),Y   // 
      sta  D47F8    // 
L436B: rts           // 
D436C: .byte  $EA, $EA, $60 // 
D436F: .byte  $04 // 
D4370: .byte  $01 // 
D4371: .byte  $00 // 
L4372: ldx #$02      // 
L4374: lda  D4381,X  // 
      sta  D4098,X  // 
      sta $D000,X   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      dex           // 
      bpl  L4374    // 
      rts           // 
D4381: .byte  $F0, $59, $38 // 
L4384: ldx #$17      // 
      lda #$00      // 
L4388: sta $D400,X   // Voice 1: Frequency Control - Low-Byte  MOS 6581 SOUND INTERFACE DEVICE (SID)
      dex           // 
      bpl  L4388    // 
      rts           // 
L438F: sed           // 
      clc           // 
L4391: adc  D43CB,X  // 
      sta  D43CB,X  // 
      lda #$00      // 
      dex           // 
      bpl  L4391    // 
      cld           // 
      ldx #$17      // 
      lda  D43CC    // 
      pha           // 
      and #$0F      // 
      jsr  L43B5    // 
      pla           // 
      lsr           // 
      lsr           // 
      lsr           // 
      lsr           // 
      jsr  L43B5    // 
      lda  D43CB    // 
      and #$0F      // 
L43B5: tay           // 
      lda  D4030,Y  // 
      sta $57       // 
      lda #$3F      // 
      sta $58       // 
//write score on bitmap
      ldy #$07      // 
L43C1: lda ($57),Y   // 
      sta  start_bitmap_mem+$13d0,X  // 
      dex           // 
      dey           // 
      bpl  L43C1    // 
      rts           // 
D43CB: .byte  $00 // 
D43CC: .byte  $00 // 
L43CD: ldx $40       // 
      lda  D4030,X  // 
      sta $57       // 
      lda #$3F      // 
      sta $58       // 
//write number of lives on bitmap      
      ldy #$07      // 
L43DA: lda ($57),Y   // 
      sta  start_bitmap_mem+$EE0,Y  // 
      dey           // 
      bpl  L43DA    // 
      txa           // 
      rts           // 
D43E4: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
D43F0: .byte  $C4 // 
D43F1: .byte  $5B // 
D43F2: .byte  $30 // 
D43F3: .byte  $01 // 
D43F4: .byte  $EA // 
D43F5: .byte  $08 // 
D43F6: .byte  $5F // 
D43F7: .byte  $C2 // 
D43F8: .byte  $0C, $00, $00, $00, $00, $00, $00, $00 // 
//start of video matrix, 1000 bytes
       .byte  $60, $6E, $6E, $63, $E0, $E0, $E0, $E6 // 
       .byte  $E0, $E0, $E0, $E6, $E0, $E0, $E0, $E6 // 
       .byte  $E0, $E0, $E0, $E6, $E0, $E0, $E0, $E6 // 
       .byte  $E0, $E0, $E0, $E6, $E6, $36, $36, $36 // 
       .byte  $01, $E0, $E6, $E6, $01, $E0, $E6, $E6 // 
       .byte  $63, $E6, $6E, $E6, $E6, $0E, $0E, $E6 // 
       .byte  $3E, $36, $0E, $A6, $0E, $E0, $E0, $E6 // 
       .byte  $E6, $0E, $0E, $E6, $3E, $36, $0E, $A6 // 
       .byte  $0E, $E0, $E0, $E6, $63, $E6, $E6, $E6 // 
       .byte  $E6, $E6, $E6, $E6, $06, $06, $E0, $E0 // 
       .byte  $60, $E6, $E6, $E6, $36, $E6, $E3, $36 // 
       .byte  $30, $E3, $E6, $36, $6E, $E6, $E6, $36 // 
       .byte  $36, $E6, $E3, $36, $30, $E3, $E6, $36 // 
       .byte  $6E, $E6, $E6, $36, $E3, $E6, $E6, $76 // 
       .byte  $E6, $06, $96, $E6, $06, $01, $0E, $E0 // 
       .byte  $E6, $E6, $E6, $E6, $63, $63, $63, $E6 // 
       .byte  $6E, $6E, $63, $E3, $63, $63, $63, $6E // 
       .byte  $63, $63, $63, $E6, $6E, $6E, $63, $E3 // 
       .byte  $63, $63, $63, $6E, $06, $E6, $E6, $E6 // 
       .byte  $E0, $E0, $E0, $E6, $E0, $E0, $E0, $E0 // 
       .byte  $60, $30, $E3, $36, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $30, $20, $36, $36 // 
       .byte  $E6, $0E, $06, $E6, $0E, $0E, $0E, $E0 // 
       .byte  $60, $3E, $E3, $36, $00 // 
D44CD: .byte  $04, $04, $04, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $36 // 
       .byte  $60, $36, $E6, $90, $E6, $E6, $E0, $E6 // 
       .byte  $0E, $0E, $E0, $6E, $E6, $36, $36, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $30 // 
       .byte  $03, $0E, $E6, $90, $E6, $E6, $E6, $E0 // 
       .byte  $E0, $E0, $E6, $DE, $0E, $23, $E6, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $6E // 
       .byte  $30, $E0, $E0, $0E, $0E, $0E, $6E, $0E // 
       .byte  $0E, $06, $E0, $1E, $0E, $0E, $E3, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $63 // 
       .byte  $0E, $E0, $E0, $E0, $E0, $E0, $6E, $6E // 
       .byte  $6E, $E6, $E6, $E0, $E0, $E6, $E3, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $3E // 
       .byte  $E0, $E0, $E0, $0E, $0C, $0C, $CE, $0C // 
       .byte  $0C, $0C, $E0, $E0, $E0, $E6, $3E, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00 // 
D45A2: .byte  $0E, $0E, $0E, $0E, $0E, $0E, $0E, $0E // 
       .byte  $0E, $00, $3E, $E0, $E0, $E0, $0E, $0C // 
       .byte  $0C, $0C, $0C, $0C, $C0, $06, $0E, $0E // 
       .byte  $60, $E3, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $E3, $0E, $06, $E0, $07, $06 // 
       .byte  $01, $63, $00, $E0, $60, $60, $1E, $0E // 
       .byte  $0E, $E3, $05, $07, $70, $57, $01, $01 // 
       .byte  $7E, $7D, $E5, $01, $01, $E7, $01, $01 // 
       .byte  $7E, $7D, $E5, $01, $01, $E7, $57, $05 // 
       .byte  $57, $25, $63, $0E, $E0, $E0, $70, $E0 // 
       .byte  $E0, $E0, $E0, $E0, $E0, $26, $E0, $E0 // 
       .byte  $E6, $E3, $5C, $5C, $5C, $5C, $E0, $E0 // 
       .byte  $E0, $E7, $E5, $E0, $E0, $E0, $E0, $E0 // 
       .byte  $E0, $E7, $E5, $E0, $E0, $E0, $50, $5C // 
       .byte  $5C, $57, $3E, $E0, $E0, $E0, $E0, $E0 // 
       .byte  $9E, $6E, $60, $E0, $E0, $E6, $E0, $E0 // 
       .byte  $E6, $3E, $C5, $C5, $EC, $E5, $E0, $E0 // 
       .byte  $E0, $5E, $0E, $E0, $E0, $E0, $E0, $E0 // 
       .byte  $E0, $5E, $0E, $E0, $E0, $E0, $90, $EC // 
       .byte  $E5, $E5, $3E, $E0, $E0, $E0, $10, $04 // 
       .byte  $10, $10, $E0, $EC, $0E, $E6, $0E, $0E // 
       .byte  $60, $E3, $70, $75, $C0, $E0, $E5, $75 // 
       .byte  $95, $E0, $05, $0E, $03, $E9, $E5, $75 // 
       .byte  $95, $E0, $05, $0E, $03, $E9, $05, $0C // 
       .byte  $04, $75, $E3, $0E, $06, $E0, $10, $17 // 
       .byte  $90, $00, $00, $00, $CE, $64, $E0, $E0 // 
       .byte  $30, $6E, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $6E, $30, $E0, $E0, $07, $06 // 
       .byte  $60, $60, $06, $06, $60, $10, $06, $E0 // 
       .byte  $36, $36 // 
D46AC: .byte  $06, $06, $06, $06, $06, $06, $06, $06 // 
       .byte  $06, $06, $06, $06, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $30, $30, $E0, $E6, $E6, $E0, $6E, $D0 // 
       .byte  $E6, $E6, $1E, $10, $E6, $36, $E3, $36 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $36, $E3, $36, $E6, $0E, $C0, $E0, $E0 // 
       .byte  $E0, $0E, $DE, $E0, $36, $36, $E3, $36 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
D4708: .byte  $03, $03, $03, $03, $03, $03, $03, $03 // 
       .byte  $03, $03, $03, $00, $36, $E3, $36, $36 // 
       .byte  $0E, $01, $0E, $0E, $0E, $E6, $E6, $E6 // 
       .byte  $3E, $3E, $3E, $E3, $6E, $6E, $3E, $6E // 
       .byte  $E6, $E6, $E6, $E6, $6E, $E6, $E6, $63 // 
       .byte  $6E, $6E, $3E, $6E, $E6, $E6, $E6, $E6 // 
       .byte  $6E, $E6, $E6, $63, $06, $E3, $3E, $3E // 
       .byte  $50, $E0, $E0, $E0, $E0, $E0, $E0, $E6 // 
       .byte  $E0, $E6, $E0, $E6, $E6, $3E, $36, $3E // 
       .byte  $03, $E6, $6E, $30, $E6, $3E, $36, $36 // 
       .byte  $E6, $3E, $36, $3E, $03, $E6, $6E, $30 // 
       .byte  $E6, $3E, $36, $36, $E3, $E6, $E6, $E0 // 
       .byte  $E6, $E0, $D0, $E6, $E0, $E6, $E0, $E0 // 
       .byte  $36, $36, $E6, $E6, $06, $0E, $E0, $0E // 
       .byte  $E0, $06, $E0, $E0, $06, $06, $60, $E6 // 
       .byte  $06, $0E, $E0, $0E, $E0, $06, $E0, $E0 // 
       .byte  $06, $06, $60, $E6, $63, $E3, $36, $3E // 
       .byte  $D6, $E6, $E6, $E6, $E6, $E6, $E6, $E6 // 
       .byte  $36, $E6, $E6, $E6, $3E, $0E, $0E, $E0 // 
       .byte  $0E, $E0, $E0, $E0, $0E, $0E, $0E, $E6 // 
       .byte  $3E, $0E, $0E, $E0, $0E, $E0, $E0, $E0 // 
       .byte  $0E, $0E, $0E, $E6, $0E, $E6, $36, $36 // 
       .byte  $D6, $E6, $E6, $E6, $E6, $E6, $E6, $E6 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
D47E0: .byte  $0D, $0D, $0D, $0D, $0D, $0D, $0D, $0D // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
//sprite pointers	   
D47F8: .byte  $54 // 
D47F9: .byte  $55 // 
D47FA: .byte  $56 // 
D47FB: .byte  $57 // 
D47FC: .byte  $58 // 
D47FD: .byte  $59 // 
D47FE: .byte  $5A // 
D47FF: .byte  $7F, $00, $3C, $00, $00, $7E, $00, $00 // 
       .byte  $E3, $00, $00, $C9, $00, $03, $E0, $80 // 
       .byte  $07, $F3, $80, $0F, $92, $00, $1F, $6C // 
       .byte  $80, $3E, $F0, $40, $3E, $FF, $40, $36 // 
       .byte  $FF, $E0, $37, $7F, $E0, $6E, $9F, $90 // 
       .byte  $6E, $EF, $10, $5D, $F0, $08, $5D, $F8 // 
       .byte  $08, $BD, $F8, $04, $7D, $BC, $04, $FD // 
       .byte  $D2, $02, $1F, $5E, $02, $3C, $F8, $00 // 
       .byte  $00, $00, $3C, $00, $00, $7E, $00, $00 // 
       .byte  $E3, $00, $00, $C9, $00, $03, $E0, $80 // 
       .byte  $07, $F3, $80, $0F, $92, $00, $1F, $6C // 
       .byte  $40, $3E, $E0, $40, $3E, $FE, $20, $3E // 
       .byte  $FF, $F0, $3F, $7F, $E0, $6F, $7F, $90 // 
       .byte  $6D, $9F, $10, $6D, $F0, $10, $DD, $F8 // 
       .byte  $08, $BB, $E8, $08, $BB, $EC, $08, $F7 // 
       .byte  $F6, $04, $7F, $CE, $04, $E9, $F0, $00 // 
       .byte  $FF, $00, $3C, $00, $00, $7E, $00, $00 // 
       .byte  $E3, $00, $00, $C9, $00, $03, $E0, $80 // 
       .byte  $07, $F3, $80, $0F, $12, $00, $1E, $EC // 
       .byte  $20, $3E, $FE, $20, $3D, $FF, $F0, $3D // 
       .byte  $FF, $A0, $3C, $FF, $A0, $7F, $1F, $20 // 
       .byte  $6F, $E0, $20, $5B, $F8, $20, $BB, $FC // 
       .byte  $20, $BB, $DC, $20, $BB, $DE, $20, $F7 // 
       .byte  $FF, $20, $6F, $E7, $20, $1C, $F0, $20 // 
       .byte  $00, $00, $3C, $00, $00, $7E, $00, $00 // 
       .byte  $E3, $00, $00, $C9, $00, $03, $E0, $80 // 
       .byte  $07, $F3, $80, $0E, $32, $00, $1D, $DC // 
       .byte  $40, $3B, $E0, $40, $3B, $FF, $80, $3B // 
       .byte  $FF, $80, $3C, $FF, $80, $7F, $0F, $00 // 
       .byte  $77, $F1, $00, $B7, $71, $00, $B7, $79 // 
       .byte  $00, $F7, $79, $00, $EE, $FA, $00, $EF // 
       .byte  $F2, $00, $1E, $72, $00, $1E, $FA, $00 // 
       .byte  $FF, $00, $3C, $00, $00, $7E, $00, $00 // 
       .byte  $87, $00, $00, $93, $00, $01, $03, $C0 // 
       .byte  $01, $C7, $E0, $00, $4C, $70, $01, $3B // 
       .byte  $B8, $02, $07, $BC, $02, $FF, $DC, $07 // 
       .byte  $FF, $DC, $07, $FF, $BC, $09, $F8, $7E // 
       .byte  $08, $F7, $F6, $10, $0F, $BA, $10, $1F // 
       .byte  $BA, $20, $1B, $BD, $20, $37, $DE, $40 // 
       .byte  $6F, $DF, $40, $7F, $F8, $00, $1F, $3C // 
       .byte  $00, $00, $3C, $00, $00, $7E, $00, $00 // 
       .byte  $87, $00, $00, $93, $00, $01, $03, $C0 // 
       .byte  $01, $C7, $E0, $00, $4E, $70, $02, $3D // 
       .byte  $B8, $02, $07, $DC, $04, $7F, $DC, $0F // 
       .byte  $FF, $DC, $07, $FF, $3C, $09, $FE, $FE // 
       .byte  $08, $F1, $EE, $08, $0F, $EE, $10, $1D // 
       .byte  $EE, $10, $1B, $F7, $10, $3B, $BB, $20 // 
       .byte  $77, $BF, $20, $7F, $FE, $00, $0F, $97 // 
       .byte  $FF, $00, $3C, $00, $00, $7E, $00, $00 // 
       .byte  $87, $00, $00, $93, $00, $01, $03, $C0 // 
       .byte  $01, $C7, $E0, $00, $4E, $70, $04, $3D // 
       .byte  $B8, $04, $03, $BC, $0F, $FF, $DC, $05 // 
       .byte  $FF, $DC, $05, $FF, $3C, $04, $F8, $FE // 
       .byte  $04, $07, $DE, $04, $1D, $DE, $04, $3B // 
       .byte  $DF, $04, $3B, $AF, $04, $77, $77, $04 // 
       .byte  $FF, $7F, $04, $E7, $F4, $04, $0F, $38 // 
       .byte  $00, $00, $3C, $00, $00, $7E, $00, $00 // 
       .byte  $87, $00, $00, $93, $00, $01, $03, $C0 // 
       .byte  $01, $C7, $E0, $00, $4E, $70, $02, $3D // 
       .byte  $B8, $02, $03, $DC, $01, $FF, $DC, $01 // 
       .byte  $FF, $EC, $01, $FF, $EC, $00, $F0, $DE // 
       .byte  $00, $8F, $36, $00, $8E, $F7, $00, $9D // 
       .byte  $F7, $00, $9D, $BB, $00, $5B, $DB, $00 // 
       .byte  $4F, $FF, $00, $4E, $78, $00, $5F, $78 // 
       .byte  $FF, $00, $38, $00, $00, $5C, $00, $00 // 
       .byte  $86, $00, $00, $AA, $00, $00, $92, $00 // 
       .byte  $01, $45, $00, $03, $AB, $00, $06, $D7 // 
       .byte  $80, $0F, $6E, $80, $0E, $AD, $80, $0C // 
       .byte  $75, $80, $04, $7D, $80, $06, $5E, $80 // 
       .byte  $05, $EF, $00, $05, $EE, $00, $0B, $EE // 
       .byte  $00, $0A, $F7, $00, $0B, $FB, $00, $11 // 
       .byte  $DF, $00, $13, $CE, $00, $10, $0F, $00 // 
       .byte  $00, $00, $38, $00, $00, $5C, $00, $00 // 
       .byte  $86, $00, $00, $AA, $00, $00, $92, $00 // 
       .byte  $01, $45, $00, $03, $AB, $00, $06, $D6 // 
       .byte  $80, $07, $6D, $80, $07, $AD, $80, $05 // 
       .byte  $35, $80, $05, $15, $80, $06, $13, $80 // 
       .byte  $07, $0F, $00, $07, $9B, $00, $03, $9B // 
       .byte  $00, $02, $BD, $00, $03, $BD, $00, $01 // 
       .byte  $BF, $00, $00, $B7, $00, $01, $AE, $00 // 
       .byte  $00, $00, $1C, $00, $00, $2E, $00, $00 // 
       .byte  $43, $00, $00, $55, $00, $00, $49, $00 // 
       .byte  $00, $23, $80, $00, $D5, $80, $01, $6B // 
       .byte  $C0, $03, $B7, $40, $03, $DA, $E0, $02 // 
       .byte  $8A, $E0, $02, $86, $E0, $03, $85, $C0 // 
       .byte  $03, $C7, $80, $02, $ED, $80, $03, $75 // 
       .byte  $80, $01, $76, $80, $01, $FA, $80, $00 // 
       .byte  $FA, $C0, $01, $FB, $C0, $03, $F0, $00 // 
       .byte  $00, $00, $1C, $00, $00, $2E, $00, $00 // 
       .byte  $43, $00, $00, $55, $00, $00, $49, $00 // 
       .byte  $00, $62, $80, $00, $F5, $80, $01, $6B // 
       .byte  $40, $03, $B7, $40, $02, $CA, $E0, $02 // 
       .byte  $8A, $E0, $03, $86, $E0, $03, $C6, $C0 // 
       .byte  $03, $E5, $80, $01, $77, $80, $01, $7A // 
       .byte  $80, $01, $BB, $80, $01, $DD, $80, $00 // 
       .byte  $7D, $C0, $00, $F6, $C0, $01, $F1, $00 // 
       .byte  $00, $00, $1C, $00, $00, $2E, $00, $00 // 
       .byte  $5F, $00, $00, $5F, $00, $00, $41, $00 // 
       .byte  $00, $3E, $00, $01, $FF, $80, $03, $FF // 
       .byte  $C0, $07, $BF, $C0, $07, $7F, $C0, $07 // 
       .byte  $5B, $C0, $03, $FF, $C0, $03, $DB, $C0 // 
       .byte  $03, $BB, $C0, $01, $BB, $40, $01, $BB // 
       .byte  $20, $01, $7B, $A0, $01, $FD, $A0, $00 // 
       .byte  $EF, $10, $00, $E7, $90, $01, $E0, $10 // 
       .byte  $00, $00, $1C, $00, $00, $2E, $00, $00 // 
       .byte  $5F, $00, $00, $5F, $00, $00, $41, $00 // 
       .byte  $00, $3E, $00, $00, $FF, $00, $01, $FF // 
       .byte  $80, $03, $FF, $C0, $07, $7F, $C0, $07 // 
       .byte  $5B, $C0, $06, $FF, $C0, $06, $DB, $C0 // 
       .byte  $07, $DB, $80, $03, $DB, $00, $03, $BD // 
       .byte  $00, $01, $BD, $80, $01, $7F, $80, $00 // 
       .byte  $FF, $00, $00, $F7, $00, $00, $E7, $80 // 
       .byte  $00, $00, $38, $00, $00, $5C, $00, $00 // 
       .byte  $BE, $00, $00, $BE, $00, $00, $82, $00 // 
       .byte  $00, $7C, $00, $01, $FE, $00, $03, $FF // 
       .byte  $00, $07, $FF, $00, $06, $FF, $80, $0D // 
       .byte  $B7, $80, $0D, $FF, $80, $0B, $DF, $80 // 
       .byte  $07, $BF, $80, $07, $B7, $00, $03, $B6 // 
       .byte  $00, $03, $B7, $00, $03, $FB, $00, $01 // 
       .byte  $DA, $00, $01, $EE, $00, $03, $EF, $00 // 
       .byte  $00, $00, $38, $00, $00, $5C, $00, $00 // 
       .byte  $BE, $00, $00, $BE, $00, $00, $82, $00 // 
       .byte  $00, $7C, $00, $01, $FE, $00, $03, $FF // 
       .byte  $00, $07, $7F, $00, $0F, $7F, $80, $0E // 
       .byte  $B7, $80, $0E, $FF, $80, $0D, $FD, $80 // 
       .byte  $05, $BE, $80, $07, $BF, $00, $03, $BE // 
       .byte  $00, $03, $77, $00, $01, $7B, $00, $01 // 
       .byte  $DA, $00, $03, $EE, $00, $01, $0F, $00 // 
       .byte  $00, $00, $1F, $FC, $00, $1F, $FC, $00 // 
       .byte  $0F, $FC, $00, $0F, $FC, $00, $07, $FC // 
       .byte  $00, $07, $F8, $1E, $03, $F8, $3F, $FD // 
       .byte  $F0, $1F, $FF, $F8, $3F, $FF, $FC, $1F // 
       .byte  $FF, $FC, $3F, $FF, $F8, $1F, $07, $E0 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $FF, $C0, $00, $FF, $C0, $00 // 
       .byte  $FF, $C0, $00, $FF, $80, $00, $FF, $80 // 
       .byte  $00, $7F, $80, $00, $7F, $80, $00, $3F // 
       .byte  $00, $00, $1F, $80, $00, $7F, $80, $00 // 
       .byte  $FF, $00, $01, $FF, $00, $01, $FE, $00 // 
       .byte  $0F, $FC, $00, $1F, $F0, $00, $3F, $80 // 
       .byte  $00, $3F, $80, $00, $35, $80, $00, $35 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $03, $FF, $80, $03, $FF, $80, $03 // 
       .byte  $FF, $00, $01, $FF, $00, $01, $FF, $00 // 
       .byte  $00, $FE, $00, $00, $FE, $00, $00, $7C // 
       .byte  $00, $00, $FE, $00, $01, $FE, $00, $01 // 
       .byte  $FE, $00, $01, $FE, $00, $00, $FE, $00 // 
       .byte  $00, $7C, $00, $00, $FE, $00, $01, $FE // 
       .byte  $00, $01, $FF, $00, $01, $FF, $00, $00 // 
       .byte  $6B, $00, $00, $6B, $00, $00, $2A, $00 // 
       .byte  $00, $07, $FF, $00, $07, $FF, $00, $07 // 
       .byte  $FF, $00, $03, $FF, $00, $03, $FE, $00 // 
       .byte  $01, $FE, $00, $01, $FC, $00, $01, $FC // 
       .byte  $00, $00, $FC, $00, $00, $FC, $00, $00 // 
       .byte  $FC, $00, $00, $F8, $00, $00, $F8, $00 // 
       .byte  $00, $F8, $00, $00, $FF, $00, $00, $FF // 
       .byte  $80, $01, $FD, $80, $01, $FD, $80, $03 // 
       .byte  $CE, $80, $01, $87, $00, $00, $03, $80 // 
       .byte  $00 // 
D4D00: .byte  $00, $00, $00 // 
D4D03: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $06, $0A, $00 // 
       .byte  $0C, $15, $00, $0C, $1A, $00, $0C, $3D // 
       .byte  $00, $0C, $3E, $80, $0C, $0E, $00, $06 // 
       .byte  $0E, $00, $07, $F5, $00, $03, $EA, $00 // 
       .byte  $00, $14, $00, $00, $2A, $00, $00, $F5 // 
       .byte  $00, $01, $FE, $00, $01, $E7, $00, $00 // 
       .byte  $F6, $00, $00, $3E, $00, $00, $1E, $00 // 
       .byte  $00, $1E, $00, $00, $16, $00, $00, $0E // 
       .byte  $00, $00, $1F, $00, $01, $00, $0F, $00 // 
       .byte  $00, $78, $00, $00, $E2, $80, $01, $CD // 
       .byte  $40, $01, $96, $80, $00, $CF, $40, $00 // 
       .byte  $6E, $00, $00, $55, $00, $00, $2A, $00 // 
       .byte  $00, $95, $00, $00, $FA, $00, $00, $74 // 
       .byte  $00, $00, $2A, $00, $00, $74, $00, $00 // 
       .byte  $F8, $00, $00, $E8, $00, $00, $78, $00 // 
       .byte  $00, $3C, $00, $00, $1E, $00, $00, $1C // 
       .byte  $00, $00, $3C, $00, $9D, $01, $0D, $00 // 
       .byte  $03, $0E, $80, $07, $1D, $40, $06, $0E // 
       .byte  $80, $06, $06, $40, $06, $07, $00, $03 // 
       .byte  $0E, $40, $03, $F6, $E0, $01, $6A, $E0 // 
       .byte  $00, $F5, $60, $00, $0A, $40, $00, $15 // 
       .byte  $40, $00, $0A, $00, $00, $34, $00, $00 // 
       .byte  $3E, $40, $00, $7F, $E0, $00, $F7, $F0 // 
       .byte  $00, $E7, $10, $00, $C0, $00, $01, $C0 // 
       .byte  $00, $07, $80, $00, $DD, $00, $15, $00 // 
       .byte  $00, $0A, $80, $00, $0F, $40, $00, $1A // 
       .byte  $80, $00, $0F, $40, $C0, $05, $E0, $70 // 
       .byte  $1A, $E0, $3E, $F5, $70, $0F, $FA, $F0 // 
       .byte  $01, $C5, $E0, $00, $02, $80, $00, $05 // 
       .byte  $40, $00, $1A, $C0, $00, $3D, $80, $00 // 
       .byte  $7B, $80, $00, $73, $00, $00, $3B, $00 // 
       .byte  $00, $1B, $80, $00, $1D, $C0, $00, $30 // 
       .byte  $E0, $00, $01, $E0, $1F, $00, $A8, $00 // 
       .byte  $01, $50, $00, $02, $F0, $00, $01, $58 // 
       .byte  $00, $02, $F0, $00, $07, $A0, $03, $07 // 
       .byte  $58, $0F, $0E, $AE, $7C, $0F, $5F, $F0 // 
       .byte  $07, $A3, $80, $01, $40, $00, $02, $A0 // 
       .byte  $00, $03, $58, $00, $01, $BC, $00, $01 // 
       .byte  $DE, $00, $00, $CE, $00, $00, $DC, $00 // 
       .byte  $01, $D8, $00, $03, $B8, $00, $07, $0C // 
       .byte  $00, $07, $80, $00, $00, $00, $B0, $80 // 
       .byte  $01, $70, $C0, $02, $B8, $E0, $01, $70 // 
       .byte  $60, $02, $60, $60, $00, $E0, $60, $02 // 
       .byte  $70, $C0, $07, $6F, $C0, $07, $56, $80 // 
       .byte  $06, $AF, $00, $02, $50, $00, $02, $A8 // 
       .byte  $00, $00, $50, $00, $00, $2C, $00, $02 // 
       .byte  $7C, $00, $07, $FE, $00, $0F, $EF, $00 // 
       .byte  $08, $E7, $00, $00, $03, $00, $00, $03 // 
       .byte  $80, $00, $01, $E0, $00, $00, $F0, $00 // 
       .byte  $00, $1E, $00, $01, $47, $00, $02, $B3 // 
       .byte  $80, $01, $69, $80, $02, $F3, $00, $00 // 
       .byte  $76, $00, $00, $AA, $00, $00, $54, $00 // 
       .byte  $00, $A9, $00, $00, $5F, $00, $00, $2E // 
       .byte  $00, $00, $54, $00, $00, $2E, $00, $00 // 
       .byte  $1F, $00, $00, $17, $00, $00, $1E, $00 // 
       .byte  $00, $3C, $00, $00, $78, $00, $00, $38 // 
       .byte  $00, $00, $3C, $00, $00, $00, $50, $60 // 
       .byte  $00, $A8, $30, $00, $58, $30, $00, $BC // 
       .byte  $30, $01, $7C, $30, $00, $70, $30, $00 // 
       .byte  $70, $60, $00, $AF, $E0, $00, $57, $C0 // 
       .byte  $00, $28, $00, $00, $54, $00, $00, $AF // 
       .byte  $00, $00, $7F, $80, $00, $E7, $80, $00 // 
       .byte  $6F, $00, $00, $7C, $00, $00, $78, $00 // 
       .byte  $00, $78, $00, $00, $68, $00, $00, $70 // 
       .byte  $00, $00, $F8, $00, $FF, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $50, $5E, $AE // 
       .byte  $80, $BE, $5C, $AC, $50, $AC, $AC, $4C // 
       .byte  $F0, $00, $AC, $00, $40, $00, $00, $35 // 
       .byte  $00, $35, $FF, $FC, $00, $05, $1F, $0C // 
       .byte  $00, $08, $05, $58, $F0, $A0, $F0, $F0 // 
       .byte  $00, $F0, $00, $00, $00, $F0, $A8, $00 // 
       .byte  $00, $05, $95, $A5, $00, $40, $F7, $A7 // 
       .byte  $50, $57, $C7, $3C, $3D, $5D, $AC, $FB // 
       .byte  $FC, $B5, $55, $BC, $F5, $00, $05, $00 // 
       .byte  $00, $05, $05, $05, $05, $05, $08, $35 // 
       .byte  $02, $35, $F7, $07, $07, $05, $05, $00 // 
       .byte  $00, $05, $05, $00, $05, $02, $05, $05 // 
       .byte  $05, $07, $02, $05, $00, $08, $07, $07 // 
       .byte  $07, $03, $02, $02, $02, $03, $03, $01 // 
       .byte  $00, $00, $05, $05, $35, $07, $07, $05 // 
       .byte  $00, $02, $02, $02, $02, $03, $01, $03 // 
       .byte  $05, $05, $1B, $05, $00, $30, $30, $F0 // 
       .byte  $00, $50, $30, $35, $F0, $F1, $A7, $37 // 
       .byte  $30, $30, $96, $A0, $37, $06, $06, $06 // 
       .byte  $05, $01, $06, $06, $05, $06, $00, $06 // 
       .byte  $05, $08, $08, $00, $00, $60, $0E, $05 // 
       .byte  $00, $20, $05, $F5, $F6, $A0, $A5, $35 // 
       .byte  $06, $DE, $55, $F5, $55, $00, $1E, $00 // 
       .byte  $00, $01, $01, $01, $00, $05, $05, $B5 // 
       .byte  $00, $00, $19, $00, $A5, $00, $30, $00 // 
       .byte  $00, $F4, $0C, $04, $05, $05, $05, $05 // 
       .byte  $05, $00, $05, $05, $00, $35, $05, $00 // 
       .byte  $00, $05, $05, $05, $00, $05, $05, $05 // 
       .byte  $00, $05, $05, $05, $00, $07, $37, $30 // 
       .byte  $F0, $96, $F1, $06, $06, $06, $06, $01 // 
       .byte  $00, $05, $05, $05, $00, $B7, $07, $07 // 
       .byte  $00, $07, $A0, $70, $00, $06, $00, $B0 // 
       .byte  $50, $05, $05, $05, $A0, $F0, $A7, $E0 // 
       .byte  $A0, $A7, $F0, $50, $70, $57, $A0, $50 // 
       .byte  $90, $A7, $B5, $B5, $50, $F0, $05, $A5 // 
       .byte  $D0, $95, $F0, $00, $A0, $F5, $90, $F0 // 
       .byte  $00, $00, $65, $AC, $F0, $BC, $A5, $F5 // 
       .byte  $00, $00, $FC, $A0, $A5, $FC, $D5, $05 // 
       .byte  $55, $FC, $BC, $B5, $95, $E5, $1C, $1C // 
       .byte  $50, $00, $E5, $00, $00, $F0, $70, $50 // 
       .byte  $00, $E0, $30, $B0, $50, $3C, $05, $FC // 
       .byte  $35, $00, $F5, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $F1, $05, $08 // 
       .byte  $08, $00, $08, $00, $00, $00, $00, $00 // 
       .byte  $03, $03, $03, $03, $06, $35, $00, $D8 // 
       .byte  $30, $78, $39, $98, $D9, $C5, $A5, $35 // 
       .byte  $96, $A6, $F3, $36, $33, $A8, $28, $75 // 
       .byte  $50, $F9, $89, $50, $C0, $C3, $C5, $05 // 
       .byte  $05, $03, $03, $33, $06, $05, $06, $30 // 
       .byte  $95, $35, $0C, $0C, $05, $00, $00, $00 // 
       .byte  $00, $03, $05, $03, $A6, $04, $07, $34 // 
       .byte  $95, $37, $07, $00, $00, $05, $05, $00 // 
       .byte  $00, $03, $03, $05, $00, $05, $05, $05 // 
       .byte  $05, $00, $05, $00, $00, $00, $1C, $1C // 
       .byte  $0C, $05, $00, $35, $35, $A3, $A3, $06 // 
       .byte  $F0, $03, $F3, $F3, $56, $F3, $A5, $33 // 
       .byte  $06, $F5, $F5, $E5, $F0, $06, $F3, $D3 // 
       .byte  $B6, $53, $96, $E0, $A0, $B3, $A6, $A6 // 
       .byte  $C3, $B0, $B0, $E0, $A5, $36, $03, $06 // 
       .byte  $00, $06, $06, $06, $06, $03, $00, $06 // 
       .byte  $06, $05, $00, $08, $00, $05, $03, $AE // 
       .byte  $F0, $A5, $75, $F5, $0E, $05, $35, $C5 // 
       .byte  $05, $05, $05, $00, $00, $35, $96, $F7 // 
       .byte  $07, $06, $07, $07, $07, $06, $03, $07 // 
       .byte  $07, $00, $05, $03, $00, $05, $03, $03 // 
       .byte  $03, $03, $03, $03, $00, $05, $05, $19 // 
       .byte  $00, $03, $05, $19, $00, $00, $06, $06 // 
       .byte  $06, $05, $05, $03, $05, $05, $F5, $05 // 
       .byte  $05, $19, $05, $05, $06, $33, $33, $F3 // 
       .byte  $05, $F5, $03, $03, $08, $05, $03, $03 // 
       .byte  $05, $05, $05, $03, $09, $00, $03, $03 // 
       .byte  $03, $06, $03, $03, $03, $06, $03, $03 // 
       .byte  $03, $06, $33, $35, $F5, $05, $33, $03 // 
       .byte  $35, $C5, $03, $35, $05, $C6, $A3, $03 // 
       .byte  $30, $30, $33, $E0, $A3, $06, $30, $90 // 
       .byte  $F0, $0E, $06, $06, $06, $06, $06, $06 // 
       .byte  $06, $06, $06, $06, $06, $00, $09, $03 // 
       .byte  $00, $03, $03, $03, $03, $03, $03, $03 // 
       .byte  $33, $03, $33, $03, $03, $A6, $F2, $A2 // 
       .byte  $F6, $F2, $72, $F0, $F2, $F8, $A9, $65 // 
       .byte  $A6, $F5, $A5, $B5, $A6, $60, $55, $08 // 
       .byte  $00, $05, $C9, $C9, $05, $39, $99, $F9 // 
       .byte  $05, $05, $05, $05, $05, $F3, $F3, $A3 // 
       .byte  $AE, $FE, $F3, $33, $F3, $53, $93, $C3 // 
       .byte  $A3, $A3, $63, $93, $A3, $06, $06, $06 // 
       .byte  $00, $00, $06, $06, $30, $3E, $33, $F6 // 
       .byte  $0E, $FE, $0E, $1E, $03, $06, $06, $06 // 
       .byte  $00, $06, $1E, $06, $00, $06, $06, $03 // 
       .byte  $00, $03, $03, $1E, $06, $C6, $06, $06 // 
       .byte  $00, $06, $56, $06, $00, $03, $03, $03 // 
       .byte  $1E, $7E, $1E, $1E, $03, $03, $03, $06 // 
       .byte  $03, $03, $06, $1E, $06, $1E, $06, $06 // 
       .byte  $06, $06, $06, $06, $06, $03, $03, $D3 // 
       .byte  $A3, $06, $03, $03, $56, $06, $1E, $06 // 
       .byte  $F6, $06, $06, $66, $06, $93, $F3, $03 // 
       .byte  $1E, $03, $06, $1E, $1E, $1E, $1E, $1E // 
       .byte  $00, $06, $06, $06, $00, $03, $06, $00 // 
       .byte  $30, $1E, $36, $90, $F0, $00, $00, $00 // 
       .byte  $00, $06, $06, $06, $03, $06, $D6, $A6 // 
       .byte  $06, $06, $06, $00, $B6, $F6, $06, $30 // 
       .byte  $06, $66, $A6, $1E, $36, $06, $06, $06 // 
       .byte  $03, $1E, $06, $1E, $00, $00, $1E, $30 // 
       .byte  $00, $30, $F0, $00, $00, $06, $03, $00 // 
       .byte  $00, $00, $03, $1E, $00, $06, $06, $06 // 
       .byte  $00, $03, $06, $06, $06, $1E, $06, $06 // 
       .byte  $06, $06, $06, $06, $06, $06, $06, $06 // 
       .byte  $06, $06, $06, $1E, $36, $F3, $06, $06 // 
       .byte  $06, $06, $06, $06, $00, $00, $00, $1E // 
       .byte  $00, $00, $00, $00, $00, $04, $05, $05 // 
       .byte  $30, $05, $35, $95, $F0, $00, $05, $05 // 
       .byte  $05, $00, $1C, $05, $05, $03, $1E, $1E // 
       .byte  $00, $1E, $03, $03, $03, $06, $03, $03 // 
       .byte  $03, $F3, $03, $03, $03, $06, $06, $06 // 
       .byte  $06, $F6, $00, $06, $03, $1E, $1E, $03 // 
       .byte  $03, $00, $03, $00, $00, $D3, $A6, $56 // 
       .byte  $E6, $56, $B0, $90, $B6, $5E, $56, $1E // 
       .byte  $66, $F6, $A0, $AE, $00, $04, $04, $34 // 
       .byte  $04, $36, $F4, $F4, $00, $20, $55, $A0 // 
       .byte  $30, $F6, $F0, $F8, $B0, $1C, $05, $05 // 
       .byte  $00, $00, $00, $00, $30, $00, $00, $00 // 
       .byte  $00, $05, $30, $05, $05, $70, $67, $50 // 
       .byte  $60, $0C, $00, $00, $00, $05, $00, $00 // 
       .byte  $00, $07, $05, $05, $F0, $E0, $00, $00 // 
       .byte  $00, $00, $00, $F0, $80, $80, $02, $80 // 
       .byte  $00, $35, $85, $00, $80, $07, $1E, $00 // 
       .byte  $00, $02, $02, $05, $A5, $42, $A2, $05 // 
       .byte  $05, $05, $B5, $F5, $85, $00, $30, $00 // 
       .byte  $00, $03, $03, $03, $03, $F0, $03, $03 // 
       .byte  $03, $03, $03, $03, $03, $00, $1F, $00 // 
       .byte  $0F, $03, $03, $03, $03, $1F, $03, $03 // 
       .byte  $03, $00, $03, $03, $03, $2F, $2F, $8F // 
       .byte  $1F, $03, $03, $83, $03, $03, $83, $33 // 
       .byte  $C3, $5F, $03, $43, $33, $E3, $53, $0B // 
       .byte  $BB, $0C, $FB, $5C, $1E, $0C, $FC, $56 // 
       .byte  $5E, $AB, $FE, $FE, $5E, $1B, $F3, $1B // 
       .byte  $1B, $1C, $1C, $1C, $1B, $06, $06, $0C // 
       .byte  $0B, $06, $06, $1B, $1B, $E3, $FB, $53 // 
       .byte  $CC, $0C, $BC, $FC, $FC, $AC, $1C, $CC // 
       .byte  $1C, $1C, $CC, $CC, $1B, $00, $33, $18 // 
       .byte  $03, $F3, $A8, $F3, $03, $08, $08, $57 // 
       .byte  $F0, $D8, $F7, $00, $F0, $03, $00, $C0 // 
       .byte  $00, $A3, $03, $06, $83, $C1, $06, $86 // 
       .byte  $06, $03, $C0, $83, $A8, $0E, $5E, $1E // 
       .byte  $36, $FE, $A6, $F6, $06, $FE, $FE, $FE // 
       .byte  $0E, $F0, $FE, $1E, $FE, $06, $FE, $0E // 
       .byte  $00, $00, $1E, $FE, $06, $0E, $1E, $FE // 
       .byte  $F6, $0E, $0E, $0E, $F0, $A6, $F6, $5C // 
       .byte  $85, $06, $B0, $AC, $A0, $AE, $50, $AF // 
       .byte  $F0, $0E, $F6, $F6, $06, $3E, $FE, $F6 // 
       .byte  $06, $1B, $3E, $0E, $06, $FC, $FB, $F6 // 
       .byte  $56, $0E, $3E, $3E, $0E, $E5, $B0, $B3 // 
       .byte  $06, $A5, $F3, $B3, $B3, $B5, $05, $A5 // 
       .byte  $56, $E6, $A3, $F6, $F3, $B3, $F3, $45 // 
       .byte  $F0, $76, $F3, $F0, $F0, $23, $F5, $F5 // 
       .byte  $F5, $63, $63, $63, $66, $A1, $D1, $A1 // 
       .byte  $B0, $A0, $F0, $BE, $A0, $F0, $F0, $A0 // 
       .byte  $A0, $F0, $C0, $E0, $A0, $05, $05, $05 // 
       .byte  $00, $05, $09, $09, $00, $05, $00, $00 // 
       .byte  $00, $05, $3B, $05, $0B, $70, $07, $0E // 
       .byte  $70, $07, $0E, $E0, $03, $8E, $E0, $03 // 
       .byte  $8E, $C0, $01, $8E, $C0, $01, $8E, $C0 // 
       .byte  $01, $8E, $C0, $01, $84, $C1, $41, $84 // 
       .byte  $40, $81, $04, $41, $41, $04, $26, $32 // 
       .byte  $04, $28, $0A, $04, $10, $04, $04, $20 // 
       .byte  $02, $04, $60, $03, $04, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $38, $03, $80 // 
       .byte  $38, $03, $80, $38, $03, $80, $38, $03 // 
       .byte  $80, $38, $03, $80, $38, $03, $80, $38 // 
       .byte  $03, $80, $10, $01, $00, $10, $01, $00 // 
       .byte  $10, $01, $00, $10, $01, $00, $10, $01 // 
       .byte  $00, $10, $01, $00, $10, $31, $03, $10 // 
       .byte  $F1, $0F, $3F, $F3, $FF, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $07, $03, $80 // 
       .byte  $08, $83, $80, $08, $87, $00, $10, $47 // 
       .byte  $00, $10, $46, $00, $10, $46, $00, $30 // 
       .byte  $66, $00, $30, $66, $00, $30, $66, $0A // 
       .byte  $30, $62, $04, $10, $42, $0A, $10, $41 // 
       .byte  $31, $10, $41, $40, $08, $80, $80, $08 // 
       .byte  $81, $00, $07, $03, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $38, $01, $F0 // 
       .byte  $38, $01, $C8, $1C, $01, $C4, $1C, $01 // 
       .byte  $C4, $0C, $01, $C2, $0C, $01, $C2, $0C // 
       .byte  $01, $C1, $0C, $00, $81, $0C, $00, $81 // 
       .byte  $08, $00, $81, $08, $00, $81, $90, $00 // 
       .byte  $C1, $50, $00, $A1, $20, $00, $A2, $10 // 
       .byte  $00, $94, $18, $00, $88, $00, $00, $80 // 
       .byte  $00, $00, $80, $00, $00, $80, $00, $00 // 
       .byte  $80, $00, $00, $80, $FF, $02, $23, $FF // 
       .byte  $01, $43, $F8, $00, $83, $38, $01, $40 // 
       .byte  $38, $02, $20, $38, $04, $10, $38, $08 // 
       .byte  $08, $38, $08, $08, $10, $90, $04, $10 // 
       .byte  $90, $04, $10, $98, $FC, $10, $1F, $0C // 
       .byte  $10, $3C, $1C, $10, $5C, $1C, $10, $0E // 
       .byte  $38, $10, $0E, $38, $10, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $BF, $FF, $FF // 
       .byte  $3F, $82, $0F, $33, $82, $03, $03, $82 // 
       .byte  $00, $03, $82, $00, $03, $82, $00, $03 // 
       .byte  $82, $00, $01, $02, $18, $01, $03, $E0 // 
       .byte  $01, $1F, $80, $01, $1F, $00, $01, $17 // 
       .byte  $00, $01, $07, $00, $01, $07, $03, $01 // 
       .byte  $07, $0F, $01, $0F, $FF, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $3C, $0E, $01 // 
       .byte  $03, $0E, $01, $00, $8E, $01, $00, $4F // 
       .byte  $01, $E0, $4F, $01, $E0, $8F, $81, $E1 // 
       .byte  $0E, $C1, $46, $04, $61, $58, $04, $21 // 
       .byte  $48, $04, $11, $46, $04, $11, $43, $04 // 
       .byte  $09, $43, $04, $09, $41, $84, $07, $41 // 
       .byte  $C4, $03, $41, $C4, $03, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $AD, $87, $40 // 
       .byte  $85, $57, $A9, $99, $85, $58, $CE, $7E // 
       .byte  $40, $10, $15, $A9, $15, $8D, $7E, $40 // 
       .byte  $AC, $7F, $40, $88, $10, $02, $A0, $03 // 
       .byte  $8C, $7F, $40, $B1, $57, $8D, $FA, $47 // 
       .byte  $60, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $75, $76, $02 // 
       .byte  $07, $60, $79, $9C, $79, $60, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $2A, $00 // 
       .byte  $00, $15, $00, $00, $3A, $00, $00, $35 // 
       .byte  $00, $00, $5A, $80, $00, $3C, $00, $00 // 
       .byte  $1C, $00, $00, $6A, $00, $00, $F4, $00 // 
       .byte  $01, $AB, $00, $00, $57, $00, $00, $EB // 
       .byte  $00, $01, $D7, $00, $01, $A8, $00, $00 // 
       .byte  $D4, $00, $00, $78, $00, $00, $3C, $00 // 
       .byte  $00, $38, $00, $00, $28, $00, $00, $1C // 
       .byte  $00, $00, $7C, $00, $00, $00, $2A, $00 // 
       .byte  $00, $15, $00, $00, $2A, $00, $00, $7D // 
       .byte  $00, $00, $3A, $80, $00, $3D, $00, $00 // 
       .byte  $1E, $00, $00, $2B, $00, $00, $57, $80 // 
       .byte  $00, $6B, $80, $00, $D7, $00, $01, $AE // 
       .byte  $00, $00, $5C, $00, $00, $AC, $00, $01 // 
       .byte  $F8, $00, $01, $DF, $00, $00, $FE, $00 // 
       .byte  $00, $1E, $00, $00, $1E, $00, $00, $3C // 
       .byte  $00, $00, $7C, $00, $00, $00, $15, $00 // 
       .byte  $00, $1A, $80, $00, $1D, $00, $00, $36 // 
       .byte  $80, $00, $1D, $00, $00, $0E, $80, $00 // 
       .byte  $2F, $00, $03, $55, $C0, $03, $EA, $F0 // 
       .byte  $01, $D4, $70, $00, $0A, $30, $00, $14 // 
       .byte  $60, $00, $2A, $20, $00, $74, $00, $00 // 
       .byte  $7C, $80, $00, $FF, $C0, $01, $EF, $E0 // 
       .byte  $01, $CE, $20, $01, $C0, $00, $03, $80 // 
       .byte  $00, $07, $00, $00, $00, $00, $2A, $00 // 
       .byte  $00, $15, $00, $00, $1A, $80, $00, $37 // 
       .byte  $00, $00, $1E, $C0, $00, $0D, $E0, $00 // 
       .byte  $3A, $E0, $03, $35, $60, $03, $FA, $C0 // 
       .byte  $01, $E5, $C0, $00, $0A, $80, $00, $15 // 
       .byte  $00, $00, $2A, $80, $00, $7F, $80, $00 // 
       .byte  $F7, $80, $00, $E7, $00, $00, $77, $00 // 
       .byte  $00, $37, $80, $00, $7B, $C0, $00, $E1 // 
       .byte  $C0, $00, $03, $80, $00, $00, $2A, $00 // 
       .byte  $00, $54, $00, $00, $AC, $00, $00, $76 // 
       .byte  $00, $01, $BC, $00, $03, $D8, $00, $03 // 
       .byte  $AE, $00, $03, $56, $60, $01, $AF, $E0 // 
       .byte  $03, $D3, $C0, $00, $A8, $00, $00, $54 // 
       .byte  $00, $00, $AA, $00, $00, $D7, $00, $00 // 
       .byte  $F7, $80, $00, $73, $80, $00, $77, $00 // 
       .byte  $00, $F6, $00, $01, $EF, $00, $01, $C3 // 
       .byte  $80, $00, $E0, $00, $00, $00, $54, $00 // 
       .byte  $00, $AC, $00, $00, $5C, $00, $00, $B6 // 
       .byte  $00, $00, $5C, $00, $00, $38, $00, $00 // 
       .byte  $EC, $00, $01, $D7, $60, $07, $AB, $E0 // 
       .byte  $07, $15, $C0, $06, $28, $00, $03, $14 // 
       .byte  $00, $02, $2A, $00, $00, $17, $00, $00 // 
       .byte  $9F, $00, $01, $FF, $80, $03, $FB, $C0 // 
       .byte  $02, $39, $C0, $00, $01, $C0, $00, $00 // 
       .byte  $E0, $00, $00, $70, $00, $00, $54, $00 // 
       .byte  $00, $A8, $00, $00, $5C, $00, $00, $B4 // 
       .byte  $00, $01, $5E, $00, $00, $BC, $00, $00 // 
       .byte  $38, $00, $00, $D4, $00, $01, $EA, $00 // 
       .byte  $01, $D6, $00, $00, $FB, $00, $00, $7D // 
       .byte  $80, $00, $3A, $00, $00, $35, $00, $00 // 
       .byte  $1F, $80, $00, $1F, $80, $00, $FF, $00 // 
       .byte  $00, $7C, $00, $00, $78, $00, $00, $3C // 
       .byte  $00, $00, $3E, $00, $00, $00, $54, $00 // 
       .byte  $00, $A8, $00, $00, $5C, $00, $00, $AC // 
       .byte  $00, $01, $5E, $00, $00, $7C, $00, $00 // 
       .byte  $38, $00, $00, $56, $00, $00, $AF, $00 // 
       .byte  $00, $D7, $40, $01, $AF, $C0, $00, $D5 // 
       .byte  $00, $00, $CA, $80, $00, $15, $80, $00 // 
       .byte  $2B, $00, $00, $1E, $00, $00, $3C, $00 // 
       .byte  $00, $1C, $00, $00, $14, $00, $00, $38 // 
       .byte  $00, $00, $3E, $00, $00, $00, $28, $00 // 
       .byte  $00, $54, $00, $00, $7A, $00, $00, $DD // 
       .byte  $00, $00, $7A, $00, $00, $BC, $00, $01 // 
       .byte  $56, $00, $01, $AE, $00, $03, $56, $00 // 
       .byte  $02, $AE, $00, $03, $5C, $00, $07, $AC // 
       .byte  $00, $02, $54, $00, $00, $A8, $00, $00 // 
       .byte  $54, $00, $00, $EE, $00, $00, $CE, $00 // 
       .byte  $01, $9E, $00, $01, $CC, $00, $00, $C6 // 
       .byte  $00, $01, $C0, $00, $00, $00, $28, $00 // 
       .byte  $00, $54, $00, $00, $7A, $00, $00, $2C // 
       .byte  $00, $00, $78, $00, $00, $3C, $00, $00 // 
       .byte  $D7, $00, $00, $AB, $80, $01, $D7, $80 // 
       .byte  $01, $AF, $00, $00, $D4, $00, $00, $AA // 
       .byte  $00, $00, $54, $00, $00, $AA, $00, $00 // 
       .byte  $57, $00, $00, $E7, $00, $00, $C7, $00 // 
       .byte  $00, $EE, $00, $00, $63, $00, $00, $70 // 
       .byte  $00, $00, $E0, $00, $00, $00, $28, $00 // 
       .byte  $00, $54, $00, $00, $BA, $00, $00, $56 // 
       .byte  $00, $00, $7C, $00, $00, $2C, $00, $00 // 
       .byte  $D7, $00, $00, $AB, $80, $01, $D5, $80 // 
       .byte  $01, $AB, $80, $01, $D7, $00, $01, $AB // 
       .byte  $00, $00, $54, $00, $00, $AA, $00, $00 // 
       .byte  $57, $00, $00, $E7, $00, $01, $C7, $00 // 
       .byte  $00, $EE, $00, $00, $67, $00, $00, $73 // 
       .byte  $00, $00, $E0, $00, $00, $00, $2A, $00 // 
       .byte  $00, $54, $00, $00, $2E, $00, $00, $5B // 
       .byte  $00, $00, $3E, $00, $00, $5C, $00, $00 // 
       .byte  $2A, $00, $00, $75, $80, $00, $6A, $C0 // 
       .byte  $00, $D5, $C0, $00, $EA, $E0, $00, $D5 // 
       .byte  $60, $00, $2A, $00, $00, $15, $00, $00 // 
       .byte  $7A, $80, $00, $E3, $80, $00, $71, $80 // 
       .byte  $00, $3B, $80, $00, $33, $00, $00, $63 // 
       .byte  $80, $00, $07, $80, $00, $00, $54, $00 // 
       .byte  $00, $28, $00, $00, $54, $00, $00, $E8 // 
       .byte  $00, $00, $70, $00, $00, $28, $00, $00 // 
       .byte  $D6, $00, $01, $AE, $00, $03, $57, $00 // 
       .byte  $03, $AA, $00, $03, $54, $00, $01, $A8 // 
       .byte  $00, $00, $54, $00, $00, $A8, $00, $00 // 
       .byte  $54, $00, $00, $EC, $00, $01, $CE, $00 // 
       .byte  $01, $C6, $00, $00, $CF, $00, $00, $4C // 
       .byte  $00, $01, $C0, $00, $00, $00, $54, $00 // 
       .byte  $00, $A8, $00, $00, $D4, $00, $00, $E8 // 
       .byte  $00, $00, $50, $00, $00, $A8, $00, $01 // 
       .byte  $56, $00, $01, $AB, $00, $03, $56, $00 // 
       .byte  $03, $AC, $00, $03, $54, $00, $01, $A8 // 
       .byte  $00, $01, $5C, $00, $00, $BC, $00, $01 // 
       .byte  $CE, $00, $01, $CF, $00, $01, $87, $00 // 
       .byte  $00, $C6, $00, $00, $E7, $00, $00, $E0 // 
       .byte  $00, $01, $80, $00, $00, $00, $54, $00 // 
       .byte  $00, $2A, $00, $00, $56, $00, $00, $2F // 
       .byte  $00, $00, $1E, $00, $00, $2A, $00, $00 // 
       .byte  $D5, $00, $01, $AA, $00, $00, $D5, $80 // 
       .byte  $00, $AA, $C0, $00, $55, $C0, $00, $2A // 
       .byte  $00, $00, $55, $00, $00, $2A, $00, $00 // 
       .byte  $57, $00, $00, $7F, $00, $00, $F7, $00 // 
       .byte  $01, $C7, $00, $00, $E6, $00, $00, $0E // 
       .byte  $00, $00, $07, $00, $00, $00, $54, $00 // 
       .byte  $00, $2A, $00, $00, $56, $00, $00, $2B // 
       .byte  $00, $00, $5E, $00, $00, $2E, $00, $00 // 
       .byte  $15, $00, $00, $2A, $20, $00, $55, $60 // 
       .byte  $00, $AA, $E0, $00, $D5, $C0, $01, $AA // 
       .byte  $C0, $01, $D4, $00, $01, $2A, $00, $00 // 
       .byte  $54, $00, $00, $6A, $00, $00, $EF, $00 // 
       .byte  $01, $E7, $00, $00, $C7, $00, $00, $6E // 
       .byte  $00, $00, $07, $00, $00, $00, $FC, $00 // 
       .byte  $01, $EE, $00, $23, $7F, $00, $66, $DB // 
       .byte  $00, $EC, $1F, $80, $E6, $9D, $C0, $E3 // 
       .byte  $9F, $70, $F1, $CC, $E0, $F5, $03, $50 // 
       .byte  $7A, $39, $00, $3D, $C4, $80, $1F, $18 // 
       .byte  $80, $0F, $E2, $80, $17, $FD, $00, $24 // 
       .byte  $A5, $00, $19, $8D, $00, $02, $52, $80 // 
       .byte  $04, $B9, $80, $09, $55, $40, $0F, $8F // 
       .byte  $C0, $1F, $87, $E0, $00, $00, $FC, $00 // 
       .byte  $01, $0E, $00, $03, $F6, $00, $03, $5F // 
       .byte  $00, $03, $5B, $80, $06, $1E, $E0, $83 // 
       .byte  $1F, $C0, $C1, $CC, $E0, $E3, $19, $40 // 
       .byte  $7E, $E5, $00, $7F, $08, $80, $3F, $70 // 
       .byte  $80, $0F, $83, $80, $02, $7D, $00, $00 // 
       .byte  $B5, $00, $01, $3B, $60, $02, $4A, $E0 // 
       .byte  $04, $9A, $60, $05, $29, $E0, $07, $C4 // 
       .byte  $60, $0F, $C3, $C0, $00, $00, $3F, $00 // 
       .byte  $00, $7A, $80, $00, $6F, $C0, $00, $FA // 
       .byte  $C0, $01, $DA, $C0, $07, $78, $60, $03 // 
       .byte  $F8, $C1, $07, $33, $83, $02, $98, $C7 // 
       .byte  $06, $A7, $7E, $01, $10, $FE, $01, $0E // 
       .byte  $FC, $01, $C1, $F0, $00, $BE, $40, $00 // 
       .byte  $AD, $00, $06, $DC, $80, $07, $52, $40 // 
       .byte  $06, $59, $20, $07, $94, $A0, $06, $23 // 
       .byte  $E0, $03, $C3, $F0, $00, $00, $3F, $00 // 
       .byte  $00, $77, $80, $00, $FE, $C4, $00, $DB // 
       .byte  $66, $01, $F8, $37, $03, $B9, $67, $0F // 
       .byte  $F9, $C7, $07, $33, $8F, $0A, $C0, $AF // 
       .byte  $00, $9C, $5E, $01, $23, $BC, $01, $18 // 
       .byte  $F8, $01, $47, $F0, $00, $BF, $E8, $00 // 
       .byte  $A5, $24, $00, $B1, $98, $01, $4A, $40 // 
       .byte  $01, $9D, $20, $02, $AA, $90, $03, $F1 // 
       .byte  $F0, $07, $E1, $F8, $00, $00, $FC, $00 // 
       .byte  $01, $C6, $00, $22, $F2, $00, $65, $BD // 
       .byte  $00, $EC, $0F, $00, $E6, $61, $00, $A3 // 
       .byte  $86, $00, $60, $FE, $00, $D1, $57, $00 // 
       .byte  $7C, $BB, $00, $36, $C5, $80, $1F, $1A // 
       .byte  $80, $0F, $F5, $80, $17, $FF, $18, $24 // 
       .byte  $FF, $18, $19, $FB, $30, $03, $FD, $80 // 
       .byte  $06, $DE, $00, $05, $8E, $00, $07, $07 // 
       .byte  $80, $0F, $8F, $C0, $00, $00, $FC, $00 // 
       .byte  $01, $86, $00, $03, $F2, $00, $03, $7F // 
       .byte  $00, $02, $4D, $00, $07, $81, $00, $82 // 
       .byte  $22, $00, $C1, $C4, $00, $E3, $5B, $00 // 
       .byte  $7E, $E5, $00, $5F, $0A, $80, $33, $75 // 
       .byte  $80, $0F, $AB, $80, $02, $7F, $00, $00 // 
       .byte  $FD, $60, $01, $FD, $60, $03, $7E, $E0 // 
       .byte  $06, $FF, $E0, $05, $EF, $E0, $07, $C3 // 
       .byte  $20, $0F, $C0, $00, $00, $00, $3F, $00 // 
       .byte  $00, $61, $80, $00, $4F, $C0, $00, $FE // 
       .byte  $C0, $00, $B2, $40, $00, $81, $E0, $00 // 
       .byte  $44, $41, $00, $23, $83, $00, $DA, $C7 // 
       .byte  $00, $A7, $7E, $01, $50, $FA, $01, $AE // 
       .byte  $CC, $01, $D5, $F0, $00, $FE, $40, $06 // 
       .byte  $BF, $00, $06, $BF, $80, $07, $7E, $C0 // 
       .byte  $07, $FF, $60, $07, $F7, $A0, $04, $C3 // 
       .byte  $E0, $00, $03, $F0, $00, $00, $3F, $00 // 
       .byte  $00, $63, $80, $00, $4F, $44, $00, $BD // 
       .byte  $A6, $00, $F0, $37, $00, $86, $67, $00 // 
       .byte  $61, $C5, $00, $7F, $0D, $00, $EA, $8B // 
       .byte  $00, $DD, $3E, $01, $A3, $6C, $01, $58 // 
       .byte  $F8, $01, $AF, $F0, $18, $FF, $E8, $07 // 
       .byte  $FF, $24, $0C, $DF, $98, $01, $BF, $C0 // 
       .byte  $00, $7B, $60, $00, $71, $A0, $01, $E0 // 
       .byte  $E0, $03, $F1, $F0, $00, $00, $00, $00 // 
       .byte  $00, $3C, $00, $00, $76, $00, $00, $8A // 
       .byte  $00, $00, $DB, $00, $00, $45, $80, $00 // 
       .byte  $22, $80, $00, $19, $80, $00, $26, $00 // 
       .byte  $00, $41, $00, $00, $4B, $00, $00, $47 // 
       .byte  $00, $00, $4E, $80, $00, $84, $80, $00 // 
       .byte  $88, $80, $00, $80, $80, $00, $80, $80 // 
       .byte  $00, $63, $00, $00, $3E, $00, $00, $36 // 
       .byte  $00, $00, $7E, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $0A, $AA, $00, $08 // 
       .byte  $88, $00, $2A, $AA, $00, $AA, $AA, $02 // 
       .byte  $AA, $AA, $0A, $AA, $AA, $2A, $A0, $00 // 
       .byte  $AA, $8A, $AA, $AA, $28, $A8, $A8, $A0 // 
       .byte  $20, $A2, $A0, $20, $8A, $A0, $20, $8A // 
       .byte  $A0, $20, $8A, $A0, $20, $8A, $A0, $20 // 
       .byte  $55, $5F, $FF, $15, $7F, $FF, $05, $FF // 
       .byte  $FF, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $AA, $A0, $00, $22, $20 // 
       .byte  $00, $AA, $A8, $00, $AA, $AA, $00, $AA // 
       .byte  $AA, $80, $AA, $AA, $A0, $00, $0A, $A8 // 
       .byte  $AA, $A2, $AA, $2A, $28, $AA, $08, $0A // 
       .byte  $2A, $08, $0A, $8A, $08, $0A, $A2, $08 // 
       .byte  $0A, $A2, $08, $0A, $A2, $08, $0A, $A2 // 
       .byte  $FF, $D5, $40, $FF, $FD, $54, $FF, $FD // 
       .byte  $55, $00, $00, $00, $00, $00, $DC, $00 // 
       .byte  $00, $B2, $00, $00, $12, $00, $00, $3C // 
       .byte  $00, $00, $37, $00, $00, $70, $00, $00 // 
       .byte  $70, $00, $00, $60, $00, $00, $E0, $00 // 
       .byte  $00, $E0, $00, $00, $E0, $00, $00, $F0 // 
       .byte  $00, $00, $F0, $00, $00, $F0, $00, $00 // 
       .byte  $F8, $00, $00, $FC, $00, $00, $7F, $00 // 
       .byte  $00, $7E, $00, $00, $3C, $00, $00, $38 // 
       .byte  $00, $00, $18, $00, $00, $00, $08, $00 // 
       .byte  $00, $18, $00, $00, $38, $00, $00, $3C // 
       .byte  $00, $00, $7E, $00, $00, $7F, $00, $00 // 
       .byte  $FC, $00, $00, $F8, $00, $00, $F0, $00 // 
       .byte  $00, $F0, $00, $00, $E0, $00, $00, $E0 // 
       .byte  $00, $00, $E0, $00, $00, $60, $00, $00 // 
       .byte  $70, $00, $00, $70, $00, $00, $67, $00 // 
       .byte  $00, $3C, $00, $00, $12, $00, $00, $B2 // 
       .byte  $00, $00, $DC, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $04, $00, $00, $02, $00, $20 // 
       .byte  $1A, $00, $30, $26, $00, $78, $24, $00 // 
       .byte  $FF, $3F, $87, $FE, $17, $FF, $FC, $21 // 
       .byte  $FF, $F0, $30, $3F, $C0, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $1C, $00, $00, $3E, $00, $00, $3F // 
       .byte  $00, $40, $7F, $FF, $FF, $F3, $7F, $FF // 
       .byte  $F3, $1C, $40, $7F, $3E, $00, $3F, $36 // 
       .byte  $00, $3E, $32, $00, $1C, $20, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $7C, $00 // 
       .byte  $01, $83, $00, $02, $00, $80, $07, $83 // 
       .byte  $C0, $07, $FF, $C0, $0F, $9E, $E0, $0E // 
       .byte  $2E, $60, $1C, $47, $70, $18, $EB, $30 // 
       .byte  $36, $77, $98, $3F, $3F, $98, $3F, $93 // 
       .byte  $98, $3F, $CD, $98, $27, $B7, $98, $1B // 
       .byte  $7B, $30, $1D, $FB, $70, $0E, $F6, $60 // 
       .byte  $0F, $0E, $E0, $07, $FF, $C0, $03, $FF // 
       .byte  $80, $00, $FE, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $03, $C7 // 
       .byte  $80, $0F, $EF, $E0, $1F, $C7, $70, $37 // 
       .byte  $EF, $B8, $7D, $FF, $DC, $6F, $FF, $DC // 
       .byte  $7A, $FF, $DC, $6F, $FF, $BC, $7D, $BF // 
       .byte  $FC, $37, $FF, $F8, $1E, $FF, $F0, $0F // 
       .byte  $FF, $E0, $03, $FF, $80, $00, $FE, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $7E, $00 // 
       .byte  $00, $91, $00, $01, $EE, $80, $02, $5F // 
       .byte  $40, $03, $DF, $40, $04, $BF, $A0, $07 // 
       .byte  $BF, $A0, $04, $BB, $A0, $0F, $79, $D0 // 
       .byte  $09, $7A, $D0, $0F, $5B, $50, $09, $6B // 
       .byte  $D0, $0F, $73, $D0, $04, $BB, $A0, $07 // 
       .byte  $BF, $A0, $04, $BF, $A0, $03, $DF, $40 // 
       .byte  $02, $5F, $40, $01, $EE, $80, $00, $91 // 
       .byte  $00, $00, $7E, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $7F, $00, $01, $FF // 
       .byte  $C0, $03, $FF, $E0, $0C, $FF, $98, $13 // 
       .byte  $3E, $64, $2F, $C1, $FA, $4F, $DD, $F9 // 
       .byte  $33, $BE, $E6, $1C, $7F, $1C, $0F, $00 // 
       .byte  $78, $07, $BE, $F0, $03, $DD, $E0, $01 // 
       .byte  $DD, $C0, $00, $EB, $80, $00, $6B, $00 // 
       .byte  $00, $36, $00, $00, $14, $00, $00, $08 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $04, $00, $40 // 
       .byte  $0C, $00, $58, $1E, $00, $64, $FF, $00 // 
       .byte  $24, $7F, $E1, $FC, $3F, $FF, $E8, $0F // 
       .byte  $FF, $84, $03, $FC, $0C, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00 
start_bitmap_mem:	   
.import binary "bitmap_area.bin"
	   
D7F40: .byte  $80, $80, $80, $80, $80, $80, $81, $81 // 
       .byte  $81, $81, $81, $82, $82, $82, $82, $82 // 
       .byte  $83, $83, $83, $83, $83, $83, $84, $84 // 
       .byte  $84, $84, $84, $85, $85, $85, $85, $85 // 
       .byte  $86, $86, $86, $86, $86, $86, $87, $87 // 
       .byte  $87, $87, $87, $88, $88, $88, $88, $88 // 
       .byte  $89, $89, $89, $89, $89, $89, $8A, $8A // 
       .byte  $8A, $8A, $8A, $8B, $8B, $8B, $8B, $8B // 
       .byte  $8C, $8C, $8C, $8C, $8C, $8C, $8D, $8D // 
       .byte  $8D, $8D, $8D, $8E, $8E, $8E, $8E, $8E // 
D7F90: .byte  $00, $88, $8B, $8B, $B8, $B8, $B8, $BB // 
       .byte  $BB, $BB, $B3, $B3, $BB, $B3, $B3, $B3 // 
       .byte  $33, $33, $37, $37, $37, $77, $37, $37 // 
       .byte  $77, $77, $77, $47, $47, $47, $74, $74 // 
       .byte  $44, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
//start of screen definitions 65 x 48 bytes (65th is the start screen)       
       .byte  $2A, $2B, $2C, $2D, $2B, $2C, $2D, $2B // 
       .byte  $31, $19, $1A, $1B, $00, $00, $00, $00 // 
       .byte  $32, $26, $21, $04, $00, $09, $05, $06 // 
       .byte  $33, $26, $20, $00, $00, $0A, $07, $08 // 
       .byte  $31, $26, $21, $00, $29, $00 // 
D8026: .byte  $4E, $00, $32, $26, $25, $16, $19, $16 // 
       .byte  $16, $19, $2C, $2D, $2B, $2C, $2D, $2B // 
       .byte  $2C, $2D, $00, $00, $3C, $0F, $10, $11 // 
       .byte  $04, $04, $4F, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $0B, $0C, $0B, $0C, $0D, $0E // 
       .byte  $0D, $0E, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $16, $19, $4C, $4D, $16, $19 // 
       .byte  $16, $19, $2B, $2C, $2D, $2B, $2C, $2D // 
       .byte  $2B, $2C, $0B, $0C, $0C, $0B, $0C, $0F // 
       .byte  $10, $11, $00, $00, $00, $00, $00, $00 // 
       .byte  $4F, $29, $0D, $0E, $0D, $0E, $3D, $00 // 
       .byte  $3C, $0B, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $16, $17, $18, $19, $19, $16 // 
       .byte  $19, $16, $2D, $2B, $2C, $2D, $2B, $2C // 
       .byte  $2D, $2B, $05, $06, $4F, $04, $04, $02 // 
       .byte  $03, $01, $07, $08, $00, $00, $00, $00 // 
       .byte  $29, $1B, $04, $00, $00, $01, $00, $00 // 
       .byte  $00, $04, $00, $00, $00, $04, $00, $00 // 
       .byte  $00, $00, $17, $18, $19, $1A, $1B, $00 // 
       .byte  $00, $29, $2C, $2D, $2B, $2C, $2D, $2B // 
       .byte  $2C, $2D, $09, $05, $06, $37, $0B, $0C // 
       .byte  $0C, $0B, $0A, $07, $08, $00, $00, $00 // 
       .byte  $00, $00, $11, $00, $00, $00, $1B, $37 // 
       .byte  $0F, $10, $00, $00, $00, $4F, $04, $00 // 
       .byte  $00, $00, $01, $01, $00, $02, $03, $00 // 
       .byte  $3C, $0D, $2B, $2C, $2D, $2B, $2C, $2D // 
       .byte  $2B, $2C, $0E, $0D, $0E, $0D, $0E, $0F // 
       .byte  $10, $11, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $0C, $0B, $0B, $0C, $0B, $0C // 
       .byte  $0B, $0C, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $02, $03, $04, $4F, $3B, $04 // 
       .byte  $01, $37, $2D, $2B, $2C, $2D, $2B, $2C // 
       .byte  $2D, $2B, $0F, $10, $11, $37, $0D, $0E // 
       .byte  $0D, $0E, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $01, $04, $29, $4F, $29, $04 // 
       .byte  $02, $03, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $0B, $0B, $0C, $0F, $10, $11 // 
       .byte  $00, $04, $2D, $2B, $2C, $2D, $2B, $2C // 
       .byte  $2D, $38, $0C, $0B, $0B, $0B, $0F, $10 // 
       .byte  $11, $34, $00, $00, $00, $00, $12, $13 // 
       .byte  $29, $35, $01, $04, $4F, $00, $14, $15 // 
       .byte  $29, $36, $00, $00, $04, $00, $09, $05 // 
       .byte  $06, $34, $29, $00, $04, $00, $0A, $07 // 
       .byte  $08, $35, $33, $26, $24, $1C, $1D, $1E // 
       .byte  $1D, $1E, $31, $26, $20, $00, $00, $29 // 
       .byte  $04, $00, $32, $26, $21, $01, $00, $00 // 
       .byte  $4F, $1B, $33, $1C, $1F, $29, $04, $00 // 
       .byte  $00, $01, $31, $09, $05, $06, $00, $29 // 
       .byte  $00, $00, $32, $0A, $07, $08, $00, $04 // 
       .byte  $00, $04, $1C, $1C, $1D, $1E, $1C, $1D // 
       .byte  $1E, $1D, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $29, $09, $05, $06, $12, $13 // 
       .byte  $12, $13, $04, $0A, $07, $08, $14, $15 // 
       .byte  $14, $15, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $37, $0C, $0D, $0E, $0F, $10 // 
       .byte  $3D, $04, $1C, $1D, $1E, $1C, $1D, $1E // 
       .byte  $1D, $1E, $00, $00, $00, $4F, $01, $04 // 
       .byte  $00, $00, $29, $1B, $00, $00, $00, $00 // 
       .byte  $00, $37, $29, $37, $0D, $0E, $0D, $0E // 
       .byte  $29, $04, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $04, $4F, $04, $04, $29, $01 // 
       .byte  $01, $01, $1C, $1D, $1E, $1D, $1F, $00 // 
       .byte  $00, $29, $00, $00, $02, $03, $00, $00 // 
       .byte  $00, $04, $11, $00, $00, $00, $00, $00 // 
       .byte  $1B, $28, $4F, $01, $04, $04, $01, $37 // 
       .byte  $0C, $0B, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $0E, $0F, $10, $3D, $04, $37 // 
       .byte  $0D, $0E, $02, $03, $00, $0F, $10, $00 // 
       .byte  $4F, $04, $04, $00, $00, $00, $00, $00 // 
       .byte  $00, $37, $04, $00, $00, $00, $00, $00 // 
       .byte  $00, $4F, $01, $1B, $1B, $04, $04, $01 // 
       .byte  $37, $0D, $00, $00, $12, $13, $29, $00 // 
       .byte  $00, $00, $29, $00, $14, $15, $4F, $00 // 
       .byte  $02, $03, $37, $0D, $0E, $0C, $0C, $0B // 
       .byte  $0B, $0C, $3D, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $04, $00, $00, $00, $04, $09 // 
       .byte  $05, $06, $0E, $3D, $00, $12, $13, $0A // 
       .byte  $07, $08, $00, $00, $00, $14, $15, $00 // 
       .byte  $00, $00, $0F, $10, $00, $37, $0D, $0E // 
       .byte  $11, $1B, $0C, $0B, $0D, $0E, $11, $04 // 
       .byte  $00, $29, $00, $00, $00, $00, $00, $04 // 
       .byte  $00, $0F, $4F, $00, $00, $09, $05, $06 // 
       .byte  $00, $01, $01, $3C, $3D, $0A, $07, $08 // 
       .byte  $00, $04, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $37, $37, $0D, $0E, $0D, $0E, $0D // 
       .byte  $0E, $3D, $01, $00, $29, $00, $12, $13 // 
       .byte  $00, $36, $10, $00, $01, $00, $14, $15 // 
       .byte  $00, $34, $04, $00, $1B, $00, $12, $13 // 
       .byte  $00, $35, $29, $00, $4F, $00, $14, $15 // 
       .byte  $29, $36, $3D, $00, $04, $00, $09, $05 // 
       .byte  $06, $34, $1B, $00, $4F, $00, $0A, $07 // 
       .byte  $08, $35, $33, $00, $12, $13, $00, $04 // 
       .byte  $00, $01, $31, $00, $14, $15, $00, $1B // 
       .byte  $00, $37, $32, $19, $1A, $1B, $00, $01 // 
       .byte  $00, $00, $33, $26, $21, $29, $00, $04 // 
       .byte  $02, $03, $31, $26, $25, $1A, $00, $00 // 
       .byte  $00, $29, $32, $26, $26, $21, $29, $3B // 
       .byte  $00, $37, $37, $0C, $0B, $0C, $0D, $0E // 
       .byte  $0C, $0B, $04, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $09, $05, $06 // 
       .byte  $00, $04, $29, $00, $4F, $0A, $07, $08 // 
       .byte  $00, $4F, $01, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $0E, $0F, $10, $00, $37, $0D // 
       .byte  $0E, $11, $0B, $0C, $0C, $0B, $0B, $0C // 
       .byte  $0B, $0C, $00, $00, $02, $03, $00, $37 // 
       .byte  $0F, $10, $12, $13, $05, $06, $00, $00 // 
       .byte  $00, $00, $14, $15, $07, $08, $00, $04 // 
       .byte  $00, $00, $00, $00, $12, $13, $00, $4F // 
       .byte  $00, $04, $29, $00, $14, $15, $00, $01 // 
       .byte  $00, $01, $0B, $0B, $0C, $0B, $0C, $0D // 
       .byte  $0E, $0D, $04, $29, $04, $02, $03, $00 // 
       .byte  $37, $0F, $00, $00, $12, $13, $00, $00 // 
       .byte  $04, $04, $00, $00, $14, $15, $4F, $00 // 
       .byte  $00, $00, $01, $00, $00, $00, $3B, $00 // 
       .byte  $00, $04, $01, $00, $29, $04, $1B, $37 // 
       .byte  $0D, $0E, $4F, $00, $37, $0C, $11, $00 // 
       .byte  $12, $13, $10, $00, $1B, $00, $00, $00 // 
       .byte  $14, $15, $04, $00, $01, $00, $04, $00 // 
       .byte  $12, $13, $00, $00, $00, $00, $00, $00 // 
       .byte  $14, $15, $17, $18, $19, $1A, $1B, $3B // 
       .byte  $00, $00, $1C, $1D, $1E, $1F, $02, $03 // 
       .byte  $00, $01, $12, $13, $00, $12, $13, $12 // 
       .byte  $13, $09, $14, $15, $00, $14, $15, $14 // 
       .byte  $15, $0A, $05, $06, $00, $00, $00, $00 // 
       .byte  $00, $00, $07, $08, $4F, $02, $03, $1B // 
       .byte  $04, $01, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $04, $0F, $10, $00, $3C, $0C, $3D // 
       .byte  $00, $04, $12, $13, $37, $0C, $0B, $0B // 
       .byte  $0C, $0B, $14, $15, $00, $00, $00, $1B // 
       .byte  $04, $01, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $04, $12, $13, $00, $09, $05, $06 // 
       .byte  $00, $29, $14, $15, $00, $0A, $07, $08 // 
       .byte  $00, $00, $37, $0B, $0C, $0D, $0E, $11 // 
       .byte  $00, $01, $11, $00, $01, $00, $12, $13 // 
       .byte  $00, $36, $04, $00, $04, $00, $14, $15 // 
       .byte  $00, $34, $29, $00, $37, $0C, $0C, $0B // 
       .byte  $0B, $35, $1B, $00, $4F, $02, $03, $04 // 
       .byte  $04, $36, $00, $00, $00, $00, $09, $05 // 
       .byte  $06, $34, $37, $0F, $10, $00, $0A, $07 // 
       .byte  $08, $35, $33, $22, $26, $21, $05, $06 // 
       .byte  $00, $01, $31, $23, $26, $20, $07, $08 // 
       .byte  $00, $04, $32, $23, $26, $25, $19, $1A // 
       .byte  $00, $4F, $33, $22, $26, $24, $1D, $1F // 
       .byte  $00, $1B, $31, $26, $26, $20, $00, $00 // 
       .byte  $00, $00, $32, $26, $26, $25, $19, $16 // 
       .byte  $19, $16, $09, $05, $06, $00, $12, $13 // 
       .byte  $12, $13, $0A, $07, $08, $00, $14, $15 // 
       .byte  $14, $15, $3C, $0C, $0B, $0C, $11, $00 // 
       .byte  $00, $00, $01, $02, $03, $04, $04, $00 // 
       .byte  $37, $0D, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $16, $17, $18, $19, $16, $19 // 
       .byte  $19, $16, $01, $00, $12, $13, $00, $29 // 
       .byte  $00, $4F, $04, $00, $14, $15, $00, $1B // 
       .byte  $00, $04, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $04, $0E, $0D, $0E, $0F, $10, $11 // 
       .byte  $29, $01, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $16, $19, $4C, $4D, $19, $16 // 
       .byte  $16, $19, $4F, $00, $37, $0C, $0B, $0B // 
       .byte  $0C, $0C, $04, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $12, $13, $00, $01, $09, $05 // 
       .byte  $06, $1B, $14, $15, $00, $04, $0A, $07 // 
       .byte  $08, $37, $00, $00, $00, $01, $04, $00 // 
       .byte  $00, $00, $16, $16, $17, $18, $19, $16 // 
       .byte  $19, $19, $0C, $0B, $0C, $0B, $0C, $11 // 
       .byte  $00, $37, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $4F, $04, $04, $12, $13, $1B // 
       .byte  $29, $01, $01, $00, $00, $14, $15, $00 // 
       .byte  $00, $04, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $19, $16, $16, $19, $17, $18 // 
       .byte  $19, $16, $0C, $11, $00, $0F, $10, $3D // 
       .byte  $00, $01, $00, $00, $00, $12, $13, $00 // 
       .byte  $00, $01, $12, $13, $00, $14, $15, $00 // 
       .byte  $12, $13, $14, $15, $00, $02, $03, $3B // 
       .byte  $14, $15, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $16, $19, $16, $4C, $4D, $16 // 
       .byte  $19, $16, $0E, $0C, $0B, $0C, $0C, $11 // 
       .byte  $00, $37, $12, $13, $00, $00, $00, $00 // 
       .byte  $00, $00, $14, $15, $00, $00, $01, $09 // 
       .byte  $05, $06, $37, $0F, $10, $11, $04, $0A // 
       .byte  $07, $08, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $16, $17, $18, $19, $1A, $1B // 
       .byte  $29, $04, $0F, $10, $3D, $00, $01, $12 // 
       .byte  $13, $36, $00, $00, $00, $00, $4F, $14 // 
       .byte  $15, $34, $02, $03, $00, $00, $04, $00 // 
       .byte  $00, $35, $04, $04, $29, $00, $09, $05 // 
       .byte  $06, $36, $00, $00, $04, $00, $0A, $07 // 
       .byte  $08, $34, $4F, $00, $01, $00, $1B, $00 // 
       .byte  $00, $35, $33, $26, $26, $24, $1C, $1D // 
       .byte  $1D, $1E, $31, $26, $26, $20, $29, $00 // 
       .byte  $00, $00, $32, $26, $26, $21, $29, $00 // 
       .byte  $00, $01, $33, $24, $1E, $1F // 
D861C: .byte  $29, $00, $02, $03, $31, $20, $00, $00 // 
       .byte  $01, $00, $04, $00, $32, $20, $4F, $04 // 
       .byte  $04, $00, $37, $0D, $1C, $1C, $1D, $1E // 
       .byte  $1D, $1E, $1C, $1D, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $3C, $0F, $10, $0F // 
       .byte  $10, $0D, $0E, $0D, $01, $00, $00, $00 // 
       .byte  $04, $00, $00, $04, $00, $00, $29, $00 // 
       .byte  $12, $13, $00, $00, $4F, $00, $04, $00 // 
       .byte  $14, $15, $00, $01, $1C, $1D, $1E, $1C // 
       .byte  $1D, $1E, $1D, $1E, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $01, $04, $29, $04 // 
       .byte  $02, $03, $04, $3B, $1B, $00, $00, $4F // 
       .byte  $00, $04, $00, $00, $00, $00, $00, $04 // 
       .byte  $00, $01, $00, $04, $01, $04, $00, $04 // 
       .byte  $00, $1B, $00, $37, $1C, $1D, $1E, $1E // 
       .byte  $1C, $1D, $1E, $1D, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $4F, $01, $37, $0D // 
       .byte  $0E, $0C, $0C, $11, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $29, $01, $00, $00, $00 // 
       .byte  $00, $00, $00, $09, $0B, $0C, $0D, $0E // 
       .byte  $11, $00, $04, $0A, $1D, $1E, $1C, $1C // 
       .byte  $1D, $1E, $1C, $1D, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $37, $0D, $0E, $0F // 
       .byte  $10, $0D, $0E, $11, $04, $00, $00, $00 // 
       .byte  $00, $02, $03, $04, $04, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $37, $0C, $0B, $0C // 
       .byte  $3D, $00, $00, $04, $1C, $1D, $1E, $1D // 
       .byte  $1E, $1C, $1D, $1E, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $01, $02, $03, $04 // 
       .byte  $04, $37, $0F, $10, $29, $00, $00, $00 // 
       .byte  $00, $00, $00, $04, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $37, $0C, $0B, $0B // 
       .byte  $0C, $0B, $0C, $11, $1C, $1D, $1E, $1D // 
       .byte  $1E, $1F, $12, $13, $00, $00, $00, $00 // 
       .byte  $02, $03, $14, $15, $4F, $00, $00, $00 // 
       .byte  $00, $00, $00, $01, $04, $04, $04, $29 // 
       .byte  $00, $00, $00, $04, $00, $00, $05, $06 // 
       .byte  $00, $04, $00, $12, $01, $00, $07, $08 // 
       .byte  $00, $04, $00, $14, $29, $00, $01, $00 // 
       .byte  $02, $03, $04, $36, $04, $00, $1B, $00 // 
       .byte  $12, $13, $00, $34, $04, $00, $4F, $00 // 
       .byte  $14, $15, $00, $35, $1B, $00, $29, $00 // 
       .byte  $12, $13, $01, $36, $13, $00, $04, $00 // 
       .byte  $14, $15, $00, $34, $15, $00, $4F, $00 // 
       .byte  $12, $13, $4F, $35, $33, $25, $19, $1A // 
       .byte  $1B, $00, $02, $03, $31, $26, $27 // 
D878B: .byte  $21 // 
D878C: .byte  $4E, $00, $00, $04, $32, $24, $1C, $1F // 
       .byte  $00, $00, $00, $04, $33, $20, $00, $00 // 
       .byte  $00, $00, $04, $04, $31, $21, $00, $01 // 
       .byte  $00, $00, $00, $00, $32, $20, $00, $3B // 
       .byte  $29, $3C, $0F, $10, $04, $00, $01, $00 // 
       .byte  $29, $04, $00, $01, $04, $00, $1B, $00 // 
       .byte  $00, $00, $00, $28, $04, $00, $29, $00 // 
       .byte  $00, $00, $12, $13, $4F, $37, $0C, $0B // 
       .byte  $0C, $11, $14, $15, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $0F, $10, $11, $02 // 
       .byte  $03, $04, $3C, $0D, $12, $13, $00, $29 // 
       .byte  $00, $29, $00, $01, $14, $15, $00, $00 // 
       .byte  $00, $00, $00, $00, $09, $05, $06, $00 // 
       .byte  $00, $00, $00, $01, $0A, $07, $08, $37 // 
       .byte  $0C, $0B, $0B, $0C, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $0E, $0D, $0E, $0D // 
       .byte  $0E, $0C, $0C, $0F, $0B, $0C, $0B, $0C // 
       .byte  $11, $00, $01, $04, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $29, $00, $00, $00 // 
       .byte  $00, $00, $04, $12, $0B, $0C, $0C, $11 // 
       .byte  $00, $00, $00, $14, $00, $00, $00, $04 // 
       .byte  $00, $00, $12, $13, $10, $3D, $00, $02 // 
       .byte  $03, $00, $14, $15, $4F, $04, $04, $02 // 
       .byte  $03, $00, $00, $37, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $13, $00, $00, $00 // 
       .byte  $00, $00, $00, $12, $15, $04, $29, $01 // 
       .byte  $3B, $04, $04, $14, $01, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $01, $00, $37, $0C // 
       .byte  $11, $00, $02, $03, $0C, $0C, $0B, $0C // 
       .byte  $0B, $0C, $0B, $0C, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $01, $37, $0D, $0E // 
       .byte  $0F, $10, $11, $12, $04, $00, $00, $00 // 
       .byte  $00, $00, $00, $14, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $1B, $04, $04, $29 // 
       .byte  $29, $00, $37, $0F, $11, $00, $02, $03 // 
       .byte  $00, $01, $00, $4F, $00, $00, $00, $04 // 
       .byte  $00, $29, $00, $12, $13, $00, $00, $4F // 
       .byte  $00, $00, $00, $14, $15, $00, $00, $04 // 
       .byte  $00, $00, $00, $04, $00, $00, $00, $12 // 
       .byte  $13, $00, $00, $04, $10, $11, $1B, $14 // 
       .byte  $15, $04, $00, $29, $3B, $00, $01, $00 // 
       .byte  $12, $13, $00, $35, $13, $00, $04, $00 // 
       .byte  $14, $15, $00, $36, $15, $00, $01, $12 // 
       .byte  $13, $12, $13, $34, $04, $00, $04, $14 // 
       .byte  $15, $14, $15, $35, $04, $00, $00, $00 // 
       .byte  $09, $05, $06, $36, $29, $00, $04, $00 // 
       .byte  $0A, $07, $08, $34, $32, $20, $00, $29 // 
       .byte  $04, $37, $0C, $0B, $33, $21, $00, $00 // 
       .byte  $00, $00, $00, $00, $31, $25, $16, $19 // 
       .byte  $4C, $4D, $16, $19, $32, $26, $26, $24 // 
       .byte  $1D, $1E, $1C, $1D, $33, $26, $26, $20 // 
       .byte  $00, $00, $00, $00, $31, $26, $24, $1F // 
       .byte  $00, $37, $0F, $10, $0B, $0C, $0B, $0C // 
       .byte  $0C, $0B, $0C, $0B, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $1A, $1B, $00, $12 // 
       .byte  $13, $00, $00, $02, $1F, $00, $00, $14 // 
       .byte  $15, $00, $37, $0D, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $0F, $10, $11, $04 // 
       .byte  $04, $29, $04, $4F, $0B, $0C, $0B, $0C // 
       .byte  $0D, $0E, $0D, $0E, $00, $00, $00, $04 // 
       .byte  $00, $00, $00, $04, $03, $00, $00, $00 // 
       .byte  $00, $09, $05, $06, $0E, $11, $00, $00 // 
       .byte  $00, $0A, $07, $08, $00, $00, $00, $29 // 
       .byte  $00, $00, $00, $01, $29, $37, $0D, $0E // 
       .byte  $0F, $10, $0C, $0B, $11, $1B, $00, $29 // 
       .byte  $01, $00, $4F, $04, $04, $00, $00, $04 // 
       .byte  $00, $00, $00, $04, $29, $00, $12, $13 // 
       .byte  $00, $00, $00, $02, $4F, $00, $14, $15 // 
       .byte  $00, $3B, $00, $12, $04, $00, $00, $1B // 
       .byte  $00, $00, $00, $14, $0C, $11, $00, $04 // 
       .byte  $01, $00, $37, $0B, $01, $00, $37, $11 // 
       .byte  $04, $00, $3C, $0C, $04, $00, $00, $1B // 
       .byte  $00, $00, $00, $00, $03, $00, $4F, $04 // 
       .byte  $29, $00, $00, $04, $13, $00, $00, $00 // 
       .byte  $00, $00, $02, $03, $15, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $0C, $0B, $0C, $0D // 
       .byte  $0E, $0F, $10, $0F, $0B, $0C, $0B, $0B // 
       .byte  $11, $00, $01, $04, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $12, $13, $37, $0D // 
       .byte  $0E, $0D, $0E, $0F, $14, $15, $00, $00 // 
       .byte  $00, $00, $04, $29, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $10, $11, $3B, $01 // 
       .byte  $01, $29, $04, $04, $0C, $0B, $0B, $0C // 
       .byte  $3D, $04, $00, $01, $00, $00, $02, $03 // 
       .byte  $00, $00, $00, $02, $10, $11, $04, $00 // 
       .byte  $00, $00, $00, $04, $04, $00, $00, $00 // 
       .byte  $04, $01, $3B, $04, $00, $00, $00, $29 // 
       .byte  $00, $00, $00, $00, $4F, $04, $37, $0D // 
       .byte  $0E, $0D, $0E, $0F, $04, $00, $29, $00 // 
       .byte  $01, $00, $00, $35, $03, $00, $01, $00 // 
       .byte  $12, $13, $00, $36, $04, $00, $3B, $00 // 
       .byte  $14, $15, $00, $34, $01, $00, $04, $00 // 
       .byte  $09, $05, $06, $35, $00, $00, $1B, $00 // 
       .byte  $0A, $07, $08, $36, $10, $00, $04, $00 // 
       .byte  $4F, $29, $00, $34, $32, $24, $1F, $4F // 
       .byte  $00, $3C, $0D, $0E, $33, $20, $00, $00 // 
       .byte  $00, $3E, $3F, $00, $31, $20, $00, $04 // 
       .byte  $29, $02, $03, $04, $32, $21, $00, $00 // 
       .byte  $00, $00, $00, $00, $33, $25, $19, $4C // 
       .byte  $4D, $16, $19, $16, $39, $2E, $2F, $30 // 
       .byte  $2E, $2F, $30, $2E, $0B, $0C, $0B, $0C // 
       .byte  $0B, $0C, $0C, $0B, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $01, $00, $00, $00 // 
       .byte  $00, $00, $00, $04, $00, $00, $00, $02 // 
       .byte  $03, $00, $00, $00, $19, $16, $17, $18 // 
       .byte  $19, $1A, $1B, $37, $2F, $30, $2E, $2F // 
       .byte  $30, $2E, $2F, $30, $0B, $0C, $0B, $0C // 
       .byte  $0B, $0C, $0C, $0D, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $01, $00, $00, $29 // 
       .byte  $00, $00, $00, $37, $00, $00, $00, $04 // 
       .byte  $04, $00, $00, $00, $0D, $0E, $0D, $0E // 
       .byte  $0F, $10, $11, $1B, $2E, $2F, $30, $2E // 
       .byte  $2F, $30, $2E, $2F, $11, $3B, $00, $29 // 
       .byte  $01, $00, $3C, $0D, $00, $00, $00, $02 // 
       .byte  $03, $00, $00, $00, $0C, $0B, $11, $00 // 
       .byte  $00, $29, $04, $04, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $0D, $0E, $0D, $0E // 
       .byte  $0F, $10, $0B, $0C, $30, $2E, $2F, $30 // 
       .byte  $2E, $2F, $30, $2E, $0E, $0D, $0E, $0C // 
       .byte  $0B, $0C, $0B, $0C, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $01, $1B, $4F, $04 // 
       .byte  $02, $03, $29, $3B, $00, $00, $00, $00 // 
       .byte  $00, $09, $05, $06, $0C, $0B, $0C, $0C // 
       .byte  $11, $0A, $07, $08, $2F, $30, $2E, $2F // 
       .byte  $30, $2E, $2F, $30, $0C, $0B, $0C, $0D // 
       .byte  $0E, $0F, $10, $11, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $01, $01, $02, $03 // 
       .byte  $04, $04, $01, $37, $04, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $04, $29, $4F, $3C // 
       .byte  $0D, $0E, $0F, $10, $2E, $2F, $30, $2E // 
       .byte  $2F, $30, $2E, $2F, $04, $3B, $01, $02 // 
       .byte  $03, $1B, $29, $04, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $0C, $0B, $0C, $11 // 
       .byte  $04, $04, $4F, $29, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $0F, $10, $0D, $0E // 
       .byte  $0C, $0B, $0B, $0C, $30, $2E, $2F, $30 // 
       .byte  $2E, $2F, $30, $2E, $04, $00, $4F, $00 // 
       .byte  $01, $29, $00, $35, $00, $00, $04, $00 // 
       .byte  $12, $13, $29, $36, $0F, $10, $11, $00 // 
       .byte  $14, $15, $00, $34, $00, $00, $00, $00 // 
       .byte  $09, $05, $06, $35, $37, $0D, $0E, $11 // 
       .byte  $0A, $07, $08, $36, $30, $2E, $2F, $30 // 
       .byte  $2E, $2F, $30, $3A, $42, $40, $41, $41 // 
       .byte  $42, $42, $40, $41, $41, $42, $42, $42 // 
       .byte  $40, $41, $42, $42, $42, $46, $47, $46 // 
       .byte  $47, $46, $47, $42, $45, $43, $44, $43 // 
       .byte  $44, $43, $44, $45, $48, $48, $49, $48 // 
       .byte  $49, $48, $49, $49, $4A, $4B, $4A, $4B // 
       .byte  $4A, $4B, $4A, $4B 
//opening screen definition       
       .byte  $2A, $2B, $2C, $2D, $2B, $2C, $2D, $38
       .byte  $31, $00, $00, $00, $00, $00, $00, $34
       .byte  $32, $00, $09, $05, $06, $00, $00, $35
       .byte  $32, $00, $0a, $07, $08, $4f, $00, $35
       .byte  $33, $00, $00, $00, $00, $00, $00, $36
       .byte  $39, $2E, $2F, $30, $2E, $2F, $30, $3A


/*
       .byte $2A, $2B, $2C, $2D // 
       .byte  $2B, $2C, $2D, $38, $31, $00, $00, $00 // 
       .byte  $00, $00, $00, $34, $32, $00, $00, $00 // 
       .byte  $00, $00, $00, $35, $32, $3C, $0C, $0B // 
       .byte  $0C, $0B, $3D, $35, $33, $00, $00, $00 // 
       .byte  $00, $00, $00, $36, $39, $2E, $2F, $30 // 
       .byte  $2E, $2F, $30, $3A
*/
//==========================       

D8C60: .byte  $00, $59, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $59, $00, $00, $79, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $93, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $77, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $78, $78, $78, $78, $79, $79, $79, $79 // 
       .byte  $7A, $7A, $7A, $7A, $20, $20, $20, $20 // 
       .byte  $60, $61, $62, $63, $5C, $5D, $5E, $5F // 
       .byte  $64, $65, $66, $67, $68, $69, $6A, $6B // 
       .byte  $3C, $3D, $3E, $3F, $38, $39, $3A, $3B // 
       .byte  $54, $55, $56, $57, $58, $59, $5A, $5B // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $0E, $0C, $0C, $E6, $50, $E0, $E0, $EC // 
       .byte  $E0, $0E, $E0, $E8, $5C, $E0, $EC, $EC // 
       .byte  $05, $05, $80, $58, $E0, $50, $F0, $89 // 
       .byte  $01, $05, $0F, $98, $50, $80, $50, $58 // 
       .byte  $0C, $70, $97, $EC, $C9, $7C, $7C, $7C // 
       .byte  $89, $0C, $9C, $E8, $80, $80, $08, $85 // 
       .byte  $01, $00, $0C, $37, $D0, $C0, $70, $70 // 
       .byte  $70, $07, $0C, $70, $E0, $C0, $05, $C0 // 
       .byte  $01, $70, $75, $75, $02, $02, $20, $20 // 
       .byte  $00, $05, $02, $50, $80, $50, $20, $20 // 
       .byte  $02, $20, $E5, $E8, $20, $D0, $D5, $D0 // 
       .byte  $05, $02, $0D, $D0, $20, $70, $C0, $D5 // 
       .byte  $05, $02, $02, $20, $52, $03, $07, $30 // 
       .byte  $50, $50, $E0, $E3, $B5, $0B, $0B, $B0 // 
       .byte  $02, $02, $02, $25, $03, $80, $30, $70 // 
       .byte  $E0, $E0, $50, $E0, $0B, $05, $B0, $B5 // 
       .byte  $61, $64, $E8, $E5, $01, $01, $01, $E5 // 
       .byte  $90, $40, $50, $75, $01, $70, $67, $65 // 
       .byte  $70, $70, $70, $67, $60, $E0, $E0, $60 // 
       .byte  $10, $16, $01, $E0, $00, $00, $85, $85 // 
       .byte  $E5, $01, $01, $E7, $E5, $E0, $E0, $E0 // 
       .byte  $0E, $E0, $E0, $E0, $05, $0E, $03, $E9 // 
       .byte  $01, $01, $7E, $7D, $E0, $E0, $E0, $E7 // 
       .byte  $E0, $E0, $E0, $5E, $E5, $75, $95, $E0 // 
       .byte  $C4, $C4, $54, $E5, $C0, $40, $C0, $E0 // 
       .byte  $70, $C0, $C0, $E9, $95, $09, $09, $95 // 
       .byte  $01, $01, $E5, $E9, $C0, $C0, $E0, $E5 // 
       .byte  $98, $C0, $C0, $E5, $B9, $09, $0C, $C5 // 
       .byte  $02, $02, $27, $27, $E0, $E0, $E0, $70 // 
       .byte  $E0, $E0, $60, $F6, $01, $01, $01, $F5 // 
       .byte  $02, $02, $02, $27, $E0, $E6, $E6, $E6 // 
       .byte  $E0, $E6, $E6, $E6, $01, $01, $01, $65 // 
       .byte  $E7, $01, $97, $E7, $90, $47, $47, $E7 // 
       .byte  $90, $E4, $E4, $E7, $05, $04, $04, $75 // 
       .byte  $01, $01, $F0, $15, $F0, $E5, $5C, $5C // 
       .byte  $E0, $E5, $E5, $5C, $05, $01, $05, $5C // 
       .byte  $05, $00, $90, $64, $C1, $50, $D5, $D0 // 
       .byte  $05, $C0, $D0, $F0, $05, $05, $0C, $01 // 
       .byte  $E0, $50, $50, $5C, $E5, $E0, $E8, $EC // 
       .byte  $EC, $EC, $C5, $5C, $3C, $5C, $C5, $5C // 
       .byte  $50, $E0, $50, $E0, $EC, $E0, $D0, $E5 // 
       .byte  $DC, $D5, $5C, $BC, $5C, $B5, $5C, $5C // 
       .byte  $E0, $80, $E0, $E0, $E4, $50, $89, $E8 // 
       .byte  $53, $E5, $53, $56, $E6, $56, $E6, $13 // 
       .byte  $60, $85, $09, $98, $90, $80, $93, $08 // 
       .byte  $63, $63, $63, $03, $35, $56, $30, $F0 // 
       .byte  $90, $90, $80, $E5, $38, $38, $89, $98 // 
       .byte  $96, $36, $63, $63, $F6, $60, $06, $35 // 
       .byte  $A0, $50, $A5, $C0, $90, $50, $50, $C0 // 
       .byte  $53, $53, $C5, $5C, $60, $36, $56, $53 // 
       .byte  $C0, $40, $E0, $10, $04, $E0, $57, $15 // 
       .byte  $07, $07, $75, $95, $06, $60, $36, $35 // 
       .byte  $E0, $60, $90, $E0, $5C, $E0, $5C, $5C // 
       .byte  $15, $05, $10, $10, $10, $15, $C0, $06 // 
       .byte  $E0, $06, $E3, $13, $E6, $56, $56, $13 // 
       .byte  $56, $E0, $50, $53, $03, $00, $00, $E5 // 
       .byte  $03, $06, $06, $F3, $86, $35, $63, $E3 // 
       .byte  $65, $53, $53, $56, $05, $35, $85, $01 // 
       .byte  $83, $60, $03, $63, $E3, $30, $83, $83 // 
       .byte  $56, $53, $53, $53, $00, $85, $05, $58 // 
       .byte  $63, $5E, $35, $53, $36, $3E, $30, $05 // 
       .byte  $30, $3E, $E0, $E0, $0E, $0E, $01, $E8 // 
       .byte  $63, $35, $50, $50, $53, $35, $05, $E0 // 
       .byte  $35, $57, $50, $E0, $38, $36, $57, $53 // 
       .byte  $E3, $E5, $95, $50, $59, $59, $95, $35 // 
       .byte  $03, $39, $53, $65, $05, $93, $35, $E5 // 
       .byte  $35, $35, $35, $35, $89, $93, $59, $93 // 
       .byte  $89, $39, $39, $23, $85, $93, $63, $53 // 
       .byte  $50, $25, $25, $23, $83, $59, $59, $53 // 
       .byte  $38, $59, $95, $23, $93, $93, $95, $53 // 
       .byte  $03, $06, $E0, $E0, $03, $E0, $E6, $E0 // 
       .byte  $03, $E6, $56, $56, $E3, $56, $01, $E0 // 
       .byte  $63, $50, $50, $E0, $63, $56, $E0, $E0 // 
       .byte  $03, $65, $56, $53, $E3, $E0, $E3, $E0 // 
       .byte  $30, $36, $36, $36, $03, $E3, $E3, $E3 // 
       .byte  $E3, $D3, $D3, $E3, $03, $D3, $D3, $D3 // 
       .byte  $E3, $3C, $09, $E3, $E9, $E9, $E9, $E9 // 
       .byte  $69, $E9, $69, $69, $06, $D6, $06, $E8 // 
       .byte  $E0, $70, $50, $05, $01, $08, $62, $05 // 
       .byte  $D0, $08, $06, $05, $08, $09, $09, $05 // 
       .byte  $E5, $30, $50, $E5, $E0, $05, $05, $90 // 
       .byte  $50, $05, $05, $E0, $01, $06, $09, $D0 // 
       .byte  $60, $6E, $6E, $63, $63, $E6, $6E, $E6 // 
       .byte  $60, $E6, $E6, $E6, $E6, $E6, $E6, $E6 // 
       .byte  $E0, $E0, $E0, $E6, $E6, $0E, $0E, $E6 // 
       .byte  $36, $E6, $E3, $36, $63, $63, $63, $E6 // 
       .byte  $E0, $E0, $E0, $E6, $3E, $36, $0E, $A6 // 
       .byte  $30, $E3, $E6, $36, $6E, $6E, $63, $E3 // 
       .byte  $E0, $E0, $E0, $E6, $0E, $E0, $E0, $E6 // 
       .byte  $6E, $E6, $E6, $36, $63, $63, $63, $6E // 
       .byte  $6E, $6E, $3E, $6E, $E6, $3E, $36, $3E // 
       .byte  $06, $0E, $E0, $0E, $3E, $0E, $0E, $E0 // 
       .byte  $E6, $E6, $E6, $E6, $03, $E6, $6E, $30 // 
       .byte  $E0, $06, $E0, $E0, $0E, $E0, $E0, $E0 // 
       .byte  $6E, $E6, $E6, $63, $E6, $3E, $36, $36 // 
       .byte  $06, $06, $60, $E6, $0E, $0E, $0E, $E6 // 
       .byte  $60, $30, $E3, $36, $60, $3E, $E3, $36 // 
       .byte  $6E, $E6, $36, $36, $DE, $0E, $23, $E6 // 
       .byte  $1E, $0E, $0E, $E3, $E0, $E0, $E6, $E3 // 
       .byte  $E0, $E0, $E6, $3E, $0E, $0E, $60, $E3 // 
       .byte  $E0, $E0, $30, $6E, $06, $E0, $36, $36 // 
       .byte  $E6, $36, $E3, $36, $36, $36, $E3, $36 // 
       .byte  $30, $20, $36, $36, $36, $60, $36, $E6 // 
       .byte  $30, $03, $0E, $E6, $6E, $30, $E0, $E0 // 
       .byte  $63, $0E, $E0, $E0, $3E, $E0, $E0, $E0 // 
       .byte  $3E, $E0, $E0, $E0, $E3, $0E, $06, $E0 // 
       .byte  $6E, $30, $E0, $E0, $30, $30, $E0, $E6 // 
       .byte  $36, $E3, $36, $E6, $36, $E3, $36, $36 // 
       .byte  $06, $07, $07, $95, $40, $70, $70, $75 // 
       .byte  $E5, $70, $70, $E0, $C5, $50, $90, $90 // 
       .byte  $E6, $36, $36, $36, $63, $E6, $E6, $E6 // 
       .byte  $E3, $E6, $E6, $76, $06, $E6, $E6, $E6 // 
       .byte  $3E, $3E, $3E, $E3, $E0, $E6, $E0, $E6 // 
       .byte  $36, $36, $E6, $E6, $36, $E6, $E6, $E6 // 
       .byte  $06, $E3, $3E, $3E, $E3, $E6, $E6, $E0 // 
       .byte  $63, $E3, $36, $3E, $0E, $E6, $36, $36 // 
       .byte  $80, $80, $80, $08, $04, $06, $06, $64 // 
       .byte  $65, $60, $56, $65, $05, $58, $50, $56 // 
       .byte  $05, $07, $70, $57, $5C, $5C, $5C, $5C // 
       .byte  $C5, $C5, $EC, $E5, $70, $75, $C0, $E0 // 
       .byte  $57, $05, $57, $25, $50, $5C, $5C, $57 // 
       .byte  $90, $EC, $E5, $E5, $05, $0C, $04, $75 // 
       .byte  $01, $70, $7E, $E7, $E9, $E2, $E2, $E2 // 
       .byte  $E1, $01, $92, $2C, $E0, $20, $25, $25 // 
       .byte  $0E, $70, $E8, $E8, $0E, $0E, $E0, $E0 // 
       .byte  $00, $05, $00, $E0, $20, $20, $E0, $E0 // 
       .byte  $03, $03, $03, $93, $D0, $DF, $EF, $EF // 
       .byte  $73, $70, $DF, $EF, $70, $DF, $DF, $ED // 
       .byte  $03, $03, $03, $93, $EF, $FC, $FC, $FC // 
       .byte  $E3, $EF, $EF, $EF, $E3, $E8, $EC, $CF // 
       .byte  $03, $B3, $03, $E3, $BF, $BF, $BF, $BF // 
       .byte  $EF, $01, $E0, $E8, $E3, $EF, $EF, $E8 // 
       .byte  $CB, $CB, $3C, $C3, $EB, $C0, $BE, $BC // 
       .byte  $7B, $EB, $CE, $06, $C8, $BC, $06, $C6 // 
       .byte  $0C, $BC, $C8, $C3, $EB, $EB, $EB, $EC // 
       .byte  $EB, $EB, $EB, $EC, $EC, $E3, $EC, $C8 // 
       .byte  $BC, $3C, $CB, $3B, $B0, $EB, $EB, $EB // 
       .byte  $EB, $EB, $6B, $FB, $0B, $01, $0B, $FC // 
       .byte  $03, $80, $30, $E0, $E8, $73, $01, $E0 // 
       .byte  $01, $07, $03, $E7, $37, $03, $D7, $E7 // 
       .byte  $E0, $E3, $3F, $E3, $01, $E6, $E8, $60 // 
       .byte  $D0, $D0, $D3, $38, $01, $36, $D6, $36 // 
       .byte  $06, $03, $06, $FE, $C0, $E0, $E1, $E1 // 
       .byte  $06, $C1, $C6, $C0, $0E, $06, $06, $01 // 
       .byte  $FE, $F6, $06, $FE, $E6, $C6, $06, $E1 // 
       .byte  $C6, $00, $F6, $E8, $00, $06, $01, $E8 // 
       .byte  $E0, $E0, $EF, $BC, $0E, $E0, $EF, $5C // 
       .byte  $D0, $6E, $EC, $EC, $06, $E0, $DE, $DE // 
       .byte  $BC, $BC, $E0, $EF, $5C, $CB, $01, $E8 // 
       .byte  $E0, $E6, $E0, $E0, $06, $06, $06, $D6 // 
       .byte  $60, $53, $65, $93, $90, $56, $65, $08 // 
       .byte  $63, $63, $63, $03, $35, $56, $30, $F0 // 
       .byte  $65, $50, $80, $E5, $38, $65, $53, $58 // 
       .byte  $96, $36, $63, $63, $F6, $60, $06, $35 // 
       .byte  $F0, $E0, $E0, $ED, $E0, $E8, $F0, $E0 // 
       .byte  $EC, $DE, $5E, $E5, $E5, $5E, $D5, $E5 // 
       .byte  $D0, $D0, $D0, $D5, $0D, $50, $50, $95 // 
       .byte  $D0, $59, $59, $A5, $0D, $05, $0B, $05 // 
       .byte  $67, $67, $67, $67, $67, $67, $67, $67 // 
       .byte  $27, $27, $27, $27, $77, $77, $77, $77 // 
       .byte  $67, $67, $67, $67, $67, $67, $67, $67 // 
       .byte  $67, $67, $67, $67, $67, $67, $67, $67 // 
       .byte  $67, $67, $67, $67, $72, $62, $62, $67 // 
       .byte  $72, $72, $72, $27, $72, $72, $72, $72 // 
       .byte  $88, $88, $88, $88, $88, $88, $88, $88 // 
       .byte  $88, $88, $88, $88, $88, $88, $88, $88 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC // 
       .byte  $31, $31, $31, $31, $31, $31, $31, $31 // 
       .byte  $31, $31, $31, $31, $31, $31, $31, $31 // 
       .byte  $BB, $00, $00, $00, $BB, $00, $00, $00 // 
       .byte  $BB, $00, $00, $00, $02, $00, $00, $00 // 
       .byte  $00, $00, $00, $BB, $00, $00, $00, $BB // 
       .byte  $00, $00, $00, $BB, $00, $00, $00, $BB // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $BB, $BB, $BB, $BB // 
       .byte  $BB, $00, $00, $00, $BB, $00, $00, $00 // 
       .byte  $BB, $00, $00, $00, $BB, $BB, $BB, $BB // 
       .byte  $20, $21, $22, $23, $24, $25, $26, $27 // 
       .byte  $28, $29, $2A, $2B, $2C, $2D, $2E, $2F // 
       .byte  $A0, $A4, $00, $00, $A8, $AC, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
D92C0: .byte  $00, $30, $60, $90, $C0, $F0, $20, $50 // 
       .byte  $80, $B0, $E0, $10, $40, $70, $A0, $D0 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
L9300: ldx current_screen      // 
      lda  D42C0,X  // 
      bpl  L9330    // 
      pha           // 
      and #$30      // 
      lsr           // 
      lsr           // 
      lsr           // 
      lsr           // 
      tay           // 
      lda  D4088,Y  // 
      sta  D4087    // 
      lda  D408C,Y  // 
      sta $D029     // Sprite 2 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      pla           // 
      tay           // 
      and #$40      // 
      beq  L9323    // 
      lda #$80      // 
L9323: sta $24       // 
      tya           // 
      pha           // 
      lda  D41C0,X  // 
      ldx #$02      // 
      jsr  L93A0    // 
      pla           // 
L9330: tay           // 
      and #$08      // 
      beq  L9354    // 
      tya           // 
      and #$07      // 
      tay           // 
      lda  D4078,Y  // 
      sta  D47FF    // 
      lda  D4080,Y  // 
      sta $D02E     // Sprite 7 Color    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda  D4090,Y  // 
      sta $25       // 
      ldx current_screen      // 
      lda  D3FC0,X  // 
      ldx #$07      // 
      jsr  L93A0    // 
L9354: jsr  L9700    // 
L9357: rts           // 
D9358: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
L9360: lda current_screen      // 
      cmp #$28      // 
      bne  L9357    // 
      lda $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp #$78      // 
      bcs  L9357    // 
      lda $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp #$60      // 
      bcs  L9357    // 
      ldx #$01      // 
      stx  DF4E     // 
      lda #$05      // 
      clc           // 
      sed           // 
      adc $40       // 
      cld           // 
      jsr  L438F    // 
      jsr  L0EDA    // 
      jsr  L9898    // 
      lda #$5F      // 
      sta $FF       // 
L938D: ldx #$50      // 
      jsr  L0C27    // 
      dec $FF       // 
      bpl  L938D    // 
      ldx #$F6      // 
      txs           // 
      jmp  L3EA0    // 
D939C: .byte  $00, $00, $00, $00 // 
/**
	POSITION SPRITE, A = x, and y position, X = sprite nr
**/
L93A0: pha           // 
      txa           // 
      asl           // x times 2 -> y register, so you can use y as index
      tay           // 
      pla           // 
      pha           // 
      and #$F0      // high nybble is x position
      clc           // 
      adc #$10      // add 16
      sta $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      iny           //  y + 1 for sprite y position register
      pla           // 
      asl           // A<<4 high nybble is 'moved out' of byte
      asl           // so low nybble time 16	
      asl           // 
      asl           // 
      clc           // 
      adc #$28      // + 40
      sta $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jsr  L0DA0    // X still contains sprite number, is now used for subroutine to turn on sprite
      rts           // 
D93BE: .byte  $00, $00 // 
D93C0: .byte  $CB, $90, $C0, $C0, $C0, $9A, $E0, $E0 // 
       .byte  $C0, $CC, $E0, $E0, $ED, $9D, $CC, $90 // 
       .byte  $99, $ED, $EB, $C8, $9D, $90, $E0, $90 // 
       .byte  $E0, $EC, $C0, $EA, $90, $9D, $EB, $C0 // 
       .byte  $0D, $E0, $C9, $E0, $CD, $9C, $ED, $90 // 
       .byte  $90, $EC, $CD, $90, $9D, $C0, $90, $90 // 
       .byte  $C0, $90, $ED, $90, $90, $ED, $CB, $00 // 
       .byte  $90, $E0, $0D, $CA, $0D, $C0, $E0, $C0 // 
L9400: dec  D9421    // 
      bne  L9420    // 
      lda #$FF      // 
      sta  D9421    // 
      ldy  D9422    // 
      dey           // 
      bpl  L9412    // 
      ldy #$0A      // 
L9412: sty  D9422    // 
      ldx #$07      // 
      lda  D9423,Y  // 
L941A: sta  D47E0,X  // 
      dex           // 
      bpl  L941A    // 
L9420: rts           // 
D9421: .byte  $5D // 
D9422: .byte  $08 // 
D9423: .byte  $0E, $06, $0C, $07, $08, $02, $04, $0A // 
       .byte  $0D, $05, $03, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
L9443: lda #$12      // 
      sta $D01C     // Sprites 0-7 Multi-Color Mode Select:  1 = M.C.M.  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$00      // 
      sta $D017     // Sprites 0-7 Expand 2x Vertical (Y)  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta  D4371    // 
      lda #$38      // 
      sta current_screen      // 
      ldx #$4E      // 
      stx  D8026    // 
      ldx #$01      // 
      stx $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      nop           // 
      lda #$29      // 
      sta  D861C    // 
      lda #$05      // 
      sta $50       // 
      jsr  L4372    // 
      lda #$00      // 
      sta  D9998    // 
      jsr  L0A00    // 
      jsr  L9300    // 
L9476: lda #$00      // 
      sta $60       // 
      sta $61       // 
      sta $49       // 
      sta $8E       // 
      ldy #$63      // 
      sty  D47F8    // 
      lda #$02      // 
      sta $3F       // 
      jmp  L9936    // 
D948C: .byte  $00, $00, $00, $00 // 
L9490: lda $DC00     // Data Port A (Keyboard, Joystick, Paddles,  Light-Pen)   7-0 Write Keyboard Column Values for Keyboard  Scan 7-6-2006 Read Paddles on Port A / B (01 = Port A,  10 = Port B) 4 Joystick A Fire Button: 1 = Fire 3-2-2006 Paddle Fire Buttons 3-0 Joystick A Direction (0-15)    MOS 6526 Complex Interface Adapter (CIA) #2
      and #$0F      // 
      cmp #$0F      // 
      bne  L9490    // 
L9499: lda $C5       // 
      cmp #$40      // 
      bne  L9499    // 
      ldx #$02      // 
L94A1: lda  D4098,X  // 
      sta  D409C,X  // 
      dex           // 
      bpl  L94A1    // 
      ldx #$01      // 
      lda #$05      // 
      jsr  L438F    // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      lda #$FF      // 
      sta $48       // 
      nop           // 
      nop           // 
      nop           // 
      nop           // 
      lda #$01      // 
      sta  D409E    // 
      lda #$01      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$01      // 
      sta current_screen      // 
      jsr  L0A00    // 
      lda #$7F      // 
      sta $48       // 
      jmp  L9936    // 
D94E3: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $A9, $40, $85 // 
       .byte  $58, $A2, $04, $BD, $F0, $95, $30, $06 // 
       .byte  $A9, $F5, $A0, $10, $D0, $04, $A9, $1B // 
       .byte  $A0, $20, $85, $FC, $84, $57, $DE, $18 // 
       .byte  $40, $10, $16, $A9, $02, $9D, $18, $40 // 
       .byte  $BC, $F8, $95, $88, $10, $02, $A0, $03 // 
       .byte  $98, $9D, $F8, $95, $B1, $57, $9D, $F8 // 
       .byte  $47, $8A, $0A, $A8, $18, $B9, $00, $D0 // 
       .byte  $7D, $F0, $95, $99, $00, $D0, $C5, $FC // 
       .byte  $D0, $12, $BD, $F0, $95, $49, $FF, $18 // 
       .byte  $69, $01, $9D, $F0, $95, $A9, $00, $9D // 
       .byte  $18, $40, $F0, $15, $C8, $DE, $E8, $95 // 
       .byte  $10, $0F, $A9, $04, $9D, $E8, $95, $38 // 
       .byte  $B9, $00, $D0, $FD, $F0, $95, $99, $00 // 
       .byte  $D0, $CA, $E0, $01, $D0, $95, $60, $00 // 
       .byte  $00 // 
D9574: .byte  $00 // 
L9575: dec  D9574    // 
      bpl  L9598    // 
      lda #$0A      // 
      sta  D9574    // 
      ldx #$04      // 
L9581: ldy  L9598,X  // 
      dey           // 
      bpl  L9589    // 
      ldy #$03      // 
L9589: tya           // 
      sta  L9598,X  // 
      lda  D4040,Y  // 
      sta  D47F8,X  // 
      dex           // 
      cpx #$01      // 
      bne  L9581    // 
L9598: rts           // 
D9599: .byte  $00, $01, $02, $02, $00, $00, $00, $03 // 
       .byte  $43, $02, $02, $04, $02, $00, $00, $34 // 
       .byte  $35, $FF, $01, $FF, $00, $00, $00, $00 // 
       .byte  $00, $03, $02, $01, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $02 // 
       .byte  $43, $02, $02, $02, $02, $00, $00, $34 // 
       .byte  $35, $FF, $01, $FF, $00, $00, $00, $00 // 
       .byte  $00, $00, $03, $02, $00, $00, $00 // 
L9600: ldx #$00      // 
L9602: lda #$40      // 
      sta $58       // 
      lda #$00      // 
      sta $57       // 
      ldy  D47F8,X  // 
L960D: lda #$40      // 
      jsr  L0C02    // 
      dey           // 
      bne  L960D    // 
      ldy #$3C      // 
L9617: lda ($57),Y   // 
      sta  D4D03,Y  // 
      dey           // 
      bpl  L9617    // 
      lda #$34      // 
      sta  D47F8,X  // 
      lda #$13      // 
      sta $FB       // 
      lda #$4D      // 
      sta $5A       // 
      sta $58       // 
L962E: lda $FB       // 
      sta $FC       // 
      lda #$39      // 
      sta $57       // 
      clc           // 
      adc #$03      // 
      sta $59       // 
L963B: ldy #$02      // 
L963D: lda ($57),Y   // 
      sta ($59),Y   // 
      dey           // 
      bpl  L963D    // 
      dec $FC       // 
      bmi  L965C    // 
      sec           // 
      lda $57       // 
      sta $59       // 
      sbc #$03      // 
      sta $57       // 
      lda $58       // 
      sta $5A       // 
      sbc #$00      // 
      sta $5A       // 
      jmp  L963B    // 
L965C: ldx #$15      // 
      jsr  L0C27    // 
      dec $FB       // 
      bpl  L962E    // 
      rts           // 
D9666: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $39, $79, $3C, $7C, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
L968A: lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      and #$04      // 
      beq  L96BE    // 
      lda $D00A     // Sprite 5 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sbc #$0C      // 
      cmp $D004     // Sprite 2 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcs  L96BE    // 
      clc           // 
      lda $D00A     // Sprite 5 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      adc #$0C      // 
      cmp $D004     // Sprite 2 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcc  L96BE    // 
      nop           // 
      sec           // 
      lda $D00B     // Sprite 5 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sbc #$14      // 
      cmp $D005     // Sprite 2 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcs  L96BE    // 
      clc           // 
      lda $D00B     // Sprite 5 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      adc #$14      // 
      cmp $D005     // Sprite 2 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcc  L96BE    // 
      rts           // 
L96BE: clc           // 
      rts           // 
L96C0: lda #$38      // 
      bcc  L96C6    // 
      lda #$3A      // 
L96C6: sta $58       // 
      lda #$55      // 
      sta $5A       // 
      ldy #$00      // 
      sty $57       // 
      sty $59       // 
L96D2: lda ($57),Y   // 
      sta ($59),Y   // 
      iny           // 
      bne  L96D2    // 
      inc $5A       // 
      inc $58       // 
      lda $5A       // 
      cmp #$57      // 
      bne  L96D2    // 
      rts           // 
D96E4: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
L9700: ldx current_screen      // 
      beq  L973D    // 
      lda  D8C60,X  // 
      beq  L973C    // 
      ldx #$01      // 
      jsr  L93A0    // 
      lda $D002     // Sprite 1 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      clc           // 
      adc #$18      // 
      sta $D008     // Sprite 4 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda $D003     // Sprite 1 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      clc           // 
      adc #$06      // 
      sta $D003     // Sprite 1 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $D009     // Sprite 4 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ldx #$75      // 
      stx  D47F9    // 
      inx           // 
      stx  D47FC    // 
      lda #$08      // 
      sta $D028     // Sprite 1 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $D02B     // Sprite 4 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ora #$12      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
L973C: rts           // 
L973D: lda  D975C    // 
      bmi  L973C    // 
      lda #$74      // 
      sta  D47FB    // 
      lda #$07      // 
      sta $D02A     // Sprite 3 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$BC      // 
      sta $D006     // Sprite 3 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$B8      // 
      sta $D007     // Sprite 3 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ldx #$03      // 
      jsr  L0DA0    // 
      rts           // 
D975C: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
L9770: lda #$00      // 
      sta $D020     // Border Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $40       // 
      ldx #$01      // 
      stx $D021     // Background Color 0  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      nop           // 
      nop           // 
      nop           // 
      jsr  L438F    // 
      jsr  L43CD    // 
      lda #$03      // 
      sta $D026     // Sprite Multi-Color Register 1    MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jsr  L3E20    // 
      lda #$12      // 
      sta $D01C     // Sprites 0-7 Multi-Color Mode Select:  1 = M.C.M.  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$05      // 
      sta $D025     // Sprite Multi-Color Register 0  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda $01       // 
      and #$FE      // 
      sta $01       // 
      rts           // 
D979E: .byte  $C0, $9D, $00, $01, $02, $03, $04, $05 // 
       .byte  $06, $07, $40, $41, $42, $02, $44, $45 // 
       .byte  $46, $47, $80, $81, $82, $83, $84, $85 // 
       .byte  $86, $87, $C0, $C1, $C2, $C3, $C4, $C5 // 
       .byte  $C6, $C7, $60, $60, $60, $60, $60, $60 // 
       .byte  $60, $60, $61, $61, $61, $61, $61, $61 // 
       .byte  $61, $61, $62, $62, $62, $62, $62, $62 // 
       .byte  $62, $62, $63, $63, $63, $63, $63, $63 // 
       .byte  $63, $63 // 
L97E0: lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      and #$04      // 
      beq  L97F3    // 
      ldx #$02      // 
      jsr  L9900    // 
      bcc  L97F3    // 
      nop           // 
      nop           // 
      jmp  L9BA2    // 
L97F3: rts           // 
D97F4: .byte  $00, $00, $FF, $FF, $00, $00, $02, $FF // 
       .byte  $00, $00, $FF, $FF // 
L9800: lda #$00      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$40      // 
      sta current_screen      // 
      jsr  L0A00    // 
L980C: ldx #$02      // 
L980E: lda  D4060,X  // 
      sta  D47FA,X  // 
      lda #$07      // 
      sta $D029,X   // Sprite 2 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      dex           // 
      bpl  L980E    // 
      ldx #$05      // 
L981E: lda  D4063,X  // 
      sta $D004,X   // Sprite 2 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      dex           // 
      bpl  L981E    // 
      inx           // 
      stx $2F       // 
      stx $D01C     // Sprites 0-7 Multi-Color Mode Select:  1 = M.C.M.  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda $D01E     // Sprite to Sprite Collision Detect  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$1D      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$BE      // 
      sta $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      inx           // 
      stx $D027     // Sprite 0 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      nop           // 
      nop           // 
      nop           // 
L9841: ldx #$10      // 
      jsr  L0C27    // 
      jsr  L0C30    // 
      jsr  L9A00    // 
      jsr  L0C25    // 
      nop           // 
      nop           // 
      nop           // 
      jsr  L9575    // 
      jsr  L0C30    // 
      jsr  L9400    // 
      nop           // 
      nop           // 
      nop           // 
      jsr  L3E95    // 
      jmp  L9841    // 
D9864: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00 // 
//L9870:change hero sprite graphics to hero and princess 
//L9874:reset hero and princess  sprite graphics to hero 	   
L9870: lda #>hero_princess_sprites_backup  // 
      bne  L9876    // 
L9874: lda #>hero_sprites_backup      // 
L9876: sta $58       // 
      lda #$57      // 
      sta $5A       // 
      ldy #$00      // 
      sty $57       // 
      sty $59       // 
      ldx #$03      // 
L9884: lda ($57),Y   // 
      sta ($59),Y   // 
      iny           // 
      bne  L9884    // 
      inc $5A       // 
      inc $58       // 
      dex           // 
      bpl  L9884    // 
      rts           // 
D9893: .byte  $00, $00, $00, $00, $00 // 
L9898: ldx #$00      // 
      lda #$03      // 
      sta $FF       // 
      lda #$BC      // 
      sta $58       // 
      lda #$65      // 
      sta $5A       // 
      lda #$00      // 
      sta $57       // 
      lda #$A0      // 
      sta $59       // 
L98AE: ldy #$00      // 
L98B0: lda ($57,X)   // 
      sta ($59),Y   // 
      inc $57       // 
      iny           // 
      cpy #$40      // 
      bne  L98B0    // 
      dec $FF       // 
      bmi  L98CF    // 
      clc           // 
      lda $59       // 
      adc #$40      // 
      sta $59       // 
      lda $5A       // 
      adc #$01      // 
      sta $5A       // 
      jmp  L98AE    // 
L98CF: ldx #$03      // 
      lda #$44      // 
      sta $58       // 
      lda #$B4      // 
      sta $57       // 
L98D9: ldy #$07      // 
      lda #$70      // 
L98DD: sta ($57),Y   // 
      dey           // 
      bpl  L98DD    // 
      dex           // 
      bmi  L98ED    // 
      lda #$28      // 
      jsr  L0C02    // 
      jmp  L98D9    // 
L98ED: rts           // 
D98EE: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00 // 
L9900: txa           // 
      asl           // 
      tay           // 
      sec           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sbc #$11      // 
      cmp $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcs  L9934    // 
      clc           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      adc #$0C      // 
      bcc  L9918    // 
      lda #$FF      // 
L9918: cmp $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcc  L9934    // 
      iny           // 
      sec           // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sbc #$10      // 
      cmp $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcs  L9934    // 
      lda $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      adc #$14      // 
      cmp $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcc  L9934    // 
      rts           // 
L9934: clc           // 
      rts           // 
L9936: jsr  L0C25    // 
      lda $033C     // 
      jsr  L4300    // 
      jsr  L0D00    // 
      jsr  L9999    // 
      jsr  L9AF9    // 
      jsr  L9C00    // 
      jsr  L9E70    // 
      jsr  L97E0    // 
      jsr  L09C0    // 
      jsr  L9400    // 
      jsr  L0D69    // 
      jsr  L9F00    // 
      jsr  L9360    // 
      jsr  L3E00    // 
      jsr  L3E95    // 
      jsr  L9E00    // 
      ldx #$F6      // 
      txs           // 
      jmp  L9936    // 
D996F: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $10 // 
D9997: .byte  $00 // 
D9998: .byte  $00 // 
L9999: lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      and #$80      // 
      beq  L99CD    // 
      ldx #$07      // 
      jsr  L9900    // 
      bcc  L99CD    // 
      lda  D47FF    // 
      cmp #$7A      // 
      bne  L99B3    // 
      lda #$00      // 
      sta  D8026    // 
L99B3: jsr  L0DAA    // 
      lda $25       // 
      beq  L99CE    // 
      ldx #$01      // 
      jsr  L438F    // 
L99BF: ldx current_screen      // 
      lda  D42C0,X  // 
      and #$F7      // 
      sta  D42C0,X  // 
      lda #$00      // 
      sta $25       // 
L99CD: rts           // 
L99CE: lda #$80      // 
      sta $8E       // 
      lda #$10      // 
      sta  D4371    // 
      jsr  L9D21    // 
      jmp  L99BF    // 
D99DD: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $70 // 
       .byte  $71, $72, $73, $70, $71, $72, $73, $6C // 
       .byte  $6D, $6E, $6F // 
L9A00: ldy #$02      // 
      lda $2F       // 
      bne  L9A52    // 
      jsr  L9D60    // 
      lsr           // 
      lsr           // 
      tax           // 
      and #$03      // 
      cmp #$03      // 
      beq  L9A3E    // 
      txa           // 
      lsr           // 
      bcs  L9A1D    // 
      dec $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ldy #$01      // 
      bne  L9A22    // 
L9A1D: inc $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ldy #$00      // 
L9A22: cpy  D43F3    // 
      beq  L9A2F    // 
      sty  D43F3    // 
      lda #$00      // 
      sta  D436F    // 
L9A2F: lda  DFF0,Y   // 
      sta $0340     // 
      jsr  L4344    // 
      jsr  L9AC0    // 
      ldy  D43F3    // 
L9A3E: lda $DC00     // Data Port A (Keyboard, Joystick, Paddles,  Light-Pen)   7-0 Write Keyboard Column Values for Keyboard  Scan 7-6-2006 Read Paddles on Port A / B (01 = Port A,  10 = Port B) 4 Joystick A Fire Button: 1 = Fire 3-2-2006 Paddle Fire Buttons 3-0 Joystick A Direction (0-15)    MOS 6526 Complex Interface Adapter (CIA) #2
      and #$10      // 
      beq  L9A4B    // 
      lda $028D     // SHFLAG Flag: Keyb'rd SHIFT Key/CTRL Key/C= Key 
      lsr           // 
      bcc  L9A6C    // 
L9A4B: lda  D4048,Y  // 
      ora #$C0      // 
      sta $31       // 
L9A52: nop           // 
      nop           // 
      nop           // 
      inc $2F       // 
      ldx $2F       // 
      cpx #$21      // 
      beq  L9A97    // 
      lda  D7F90,X  // 
      pha           // 
      jsr  L9A6D    // 
      pla           // 
      asl           // 
      asl           // 
      asl           // 
      asl           // 
      jsr  L9A6D    // 
L9A6C: rts           // 
L9A6D: and $31       // 
      sta $02       // 
      ldx #$01      // 
L9A73: ldy #$01      // 
L9A75: asl $02       // 
      bcc  L9A90    // 
      clc           // 
      lda $D000,X   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      adc  DBFE,Y   // 
      cmp  D43F5    // 
      beq  L9A9F    // 
      cmp #$FF      // 
      beq  L9A90    // 
      cmp #$08      // 
      bcc  L9A90    // 
      sta $D000,X   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
L9A90: dey           // 
      bpl  L9A75    // 
      dex           // 
      bpl  L9A73    // 
      rts           // 
L9A97: lda #$00      // 
      sta $2F       // 
      jsr  L9AC0    // 
      rts           // 
L9A9F: jmp  L3D00    // 
D9AA2: .byte  $8B, $B8, $B8, $B8, $BB, $BB, $BB, $B3 // 
       .byte  $B3, $02, $B3, $B3, $B3, $33, $02, $37 // 
       .byte  $37, $02, $77, $37, $37, $77, $77, $77 // 
       .byte  $47, $47, $47, $74, $74, $44 // 
L9AC0: lda $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp #$20      // 
      bcc  L9AE3    // 
      cmp #$36      // 
      bcc  L9AE9    // 
      cmp #$60      // 
      bcc  L9AE3    // 
      cmp #$76      // 
      bcc  L9AE9    // 
      cmp #$A0      // 
      bcc  L9AE3    // 
      cmp #$B6      // 
      bcc  L9AE9    // 
      cmp #$E0      // 
      bcc  L9AE3    // 
      cmp #$F6      // 
      bcc  L9AE9    // 
L9AE3: ldx #$F6      // 
      txs           // 
      jmp  L0C98    // 
L9AE9: rts           // 
D9AEA: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $EA, $EA, $EA, $EA, $EA, $EA, $EA // 
L9AF9: lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      and #$04      // 
      beq  L9B5E    // 
      ldy $61       // 
      bne  L9B5E    // 
      lda $24       // 
      bpl  L9B5D    // 
      sec           // 
      lda $D005     // Sprite 2 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      tay           // 
      sbc #$08      // 
      cmp $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcs  L9B5D    // 
      tya           // 
      adc #$0A      // 
      cmp $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcc  L9B5D    // 
      lda $D004     // Sprite 2 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      tay           // 
      cmp $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcs  L9B35    // 
      tya           // 
      adc #$40      // 
      bcs  L9B2F    // 
      cmp $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcc  L9B5D    // 
L9B2F: ldy #$79      // 
      lda #$00      // 
      beq  L9B43    // 
L9B35: tya           // 
      sbc #$40      // 
      bcc  L9B3F    // 
      cmp $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      bcs  L9B5D    // 
L9B3F: ldy #$7F      // 
      lda #$40      // 
L9B43: sty  D47FE    // 
      sta $60       // 
      lda #$0F      // 
      sta $D02D     // Sprite 6 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jsr  L0DC2    // 
      lda #$60      // 
      sta $61       // 
      ldx #$06      // 
      jsr  L0DA0    // 
      lda #$00      // 
      sta $24       // 
L9B5D: rts           // 
L9B5E: ldy $61       // 
      beq  L9B5D    // 
      dey           // 
      beq  L9B92    // 
      sty $61       // 
      ldx #$06      // 
      lda $60       // 
      sta  L9B6E+1  // 
L9B6E: jsr  L0B40    // 
      bcs  L9B92    // 
      lda $D00C     // Sprite 6 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp #$18      // 
      bcc  L9B92    // 
      cmp #$FE      // 
      bcs  L9B92    // 
      ldx #$06      // 
      jsr  L9900    // 
      bcs  L9BA2    // 
      rts           // 
D9B86: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $18 // 
L9B92: lda $D00D     // Sprite 6 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      adc #$05      // 
      sta $D00D     // Sprite 6 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$00      // 
      sta $61       // 
      jsr  L9BF0    // 
      rts           // 
L9BA2: pla           // 
      pla           // 
      lda #$15      // 
      sta $50       // 
      jsr  L9BE0    // 
      lda #$05      // 
      sta $50       // 
      dec $40       // 
      jsr  L43CD    // 
      bne  L9BB9    // 
      jmp  L092D    // 
L9BB9: ldy #$01      // 
L9BBB: lda  D409C,Y  // 
      sta $D000,Y   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      dey           // 
      bpl  L9BBB    // 
      lda  D409E    // 
      sta current_screen      // 
      jsr  L0D53    // 
      lda #$20      // 
      sta  D47F8    // 
      jsr  L0DE6    // 
      lda #$00      // 
      sta  D4371    // 
      jmp  L9476    // 
D9BDC: .byte  $00, $00, $00, $00 // 
L9BE0: ldx #$01      // 
      nop           // 
      nop           // 
      nop           // 
      jsr  L9600    // 
      rts           // 
D9BE9: .byte  $00, $00, $00, $00, $00, $00, $00 // 
L9BF0: ldx #$00      // 
      jsr  L9D40    // 
      rts           // 
D9BF6: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00 // 
L9C00: ldy $49       // 
      bne  L9C57    // 
      lda $8E       // 
      bpl  L9C56    // 
      lda $DC00     // Data Port A (Keyboard, Joystick, Paddles,  Light-Pen)   7-0 Write Keyboard Column Values for Keyboard  Scan 7-6-2006 Read Paddles on Port A / B (01 = Port A,  10 = Port B) 4 Joystick A Fire Button: 1 = Fire 3-2-2006 Paddle Fire Buttons 3-0 Joystick A Direction (0-15)    MOS 6526 Complex Interface Adapter (CIA) #2
      and #$10      // 
      beq  L9C15    // 
      lda $028D     // SHFLAG Flag: Keyb'rd SHIFT Key/CTRL Key/C= Key 
      lsr           // 
      bcc  L9C56    // 
L9C15: ldx #$01      // 
L9C17: lda $D000,X   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $D00A,X   // Sprite 5 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      dex           // 
      bpl  L9C17    // 
      ldy  D4343    // 
      sty  D9C8C    // 
      lda  D40A8,Y  // 
      sta  D47FD    // 
      ldx  D40A4,Y  // 
      stx $4B       // 
      lda  D40A0,Y  // 
      clc           // 
      adc $D00A,X   // Sprite 5 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $D00A,X   // Sprite 5 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$0F      // 
      sta $D02C     // Sprite 5 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      lda #$60      // 
      sta $49       // 
      ldx #$05      // 
      jsr  L0DA0    // 
      lda #$00      // 
      sta $8E       // 
      sta  D4371    // 
      sta  D436F    // 
      jsr  L4344    // 
L9C56: rts           // 
L9C57: dey           // 
      beq  L9C7F    // 
      sty $49       // 
      jsr  L968A    // 
      bcs  L9C8D    // 
      ldx #$05      // 
      ldy  D9C8C    // 
      lda  DBF8,Y   // 
      sta  L9C6C+1  // 
L9C6C: jsr  L0B40    // 
      bcs  L9C7F    // 
      ldy $4B       // 
      lda $D00A,Y   // Sprite 5 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp #$14      // 
      bcc  L9C7F    // 
      cmp #$FE      // 
      beq  L9C7F    // 
      rts           // 
L9C7F: jsr  L9D1B    // 
      ldx #$05      // 
      jsr  L0DAA    // 
      lda #$00      // 
      sta $49       // 
      rts           // 
D9C8C: .byte  $01 // 
L9C8D: ldx current_screen      // 
      lda  D42C0,X  // 
      and #$7F      // 
      sta  D42C0,X  // 
      jsr  L9C7F    // 
      ldx #$02      // 
      jsr  L9602    // 
      ldx #$02      // 
      jsr  L0DAA    // 
      ldx #$01      // 
      txa           // 
      jsr  L438F    // 
      rts           // 
D9CAB: .byte  $00, $00, $00, $00, $00 // 
//write 'THE'  on bitmap 
L9CB0: ldx #$17      // 
L9CB2: lda  D3F50,X  // 
      sta  start_bitmap_mem+$668,X  // 
      dex           // 
      bpl  L9CB2    // 
      ldx #$02      // 
      lda #$04      // 
L9CBF: sta  D44CD,X  // 
      dex           // 
      bpl  L9CBF    // 
//write 'ADVENTURE' on bitmap      
      ldx #$47      // 
L9CC7: lda  D3F68,X  // 
      sta  start_bitmap_mem+$D10,X  // 
      dex           // 
      bpl  L9CC7    // 
      ldx #$08      // 
      lda #$0E      // 
L9CD4: sta  D45A2,X  // 
      dex           // 
      bpl  L9CD4    // 
//write  'BY G DUDDLE' on bitmap      
      ldx #$57      // 
L9CDC: lda  D9EA0,X  // 
      sta  start_bitmap_mem+$1568+960,X  // 
      dex           // 
      bpl  L9CDC    // 
      ldx #$0B      // 
      lda #$06      // 
L9CE9: sta  D46AC+120,X  // 
      dex           // 
      bpl  L9CE9    // 
      jmp  L4160    // 
D9CF2: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00 // 
L9D00: ldx #$0F      // 
L9D02: stx $D418     // Select Filter Mode and Volume 7 Cut-Off Voice 3 Output: 1 = Off, 0 = On   6 Select Filter High-Pass Mode: 1 = On 5 Select Filter Band-Pass Mode: 1 = On 4 Select Filter Low-Pass Mode: 1 = On 3-0 Select Output Volume: 0-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      txa           // 
      ldx #$40      // 
      jsr  L0C27    // 
      tax           // 
      dex           // 
      bpl  L9D02    // 
      lda #$02      // 
      sta  DF4E     // 
      jsr  L0EDA    // 
      jsr  L3DA0    // 
      rts           // 
L9D1B: lda #$0C      // 
L9D1D: sta  L0E0B+1  // 
      rts           // 
L9D21: lda #$08      // 
      bne  L9D1D    // 
      brk           // 
D9D26: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00 // 
L9D40: rts           // 
D9D41: .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $AD // 
       .byte  $15, $03, $C9, $0F, $D0, $FE, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00 // 
L9D60: lda $C5       // 
      cmp #$40      // 
      beq  L9D78    // 
      cmp #$0D      // 
      bne  L9D6E    // 
      lda #$07      // 
      bne  L9D74    // 
L9D6E: cmp #$0A      // 
      bne  L9D78    // 
      lda #$0B      // 
L9D74: and $DC00     // Data Port A (Keyboard, Joystick, Paddles,  Light-Pen)   7-0 Write Keyboard Column Values for Keyboard  Scan 7-6-2006 Read Paddles on Port A / B (01 = Port A,  10 = Port B) 4 Joystick A Fire Button: 1 = Fire 3-2-2006 Paddle Fire Buttons 3-0 Joystick A Direction (0-15)    MOS 6526 Complex Interface Adapter (CIA) #2
      rts           // 
L9D78: lda $DC00     // Data Port A (Keyboard, Joystick, Paddles,  Light-Pen)   7-0 Write Keyboard Column Values for Keyboard  Scan 7-6-2006 Read Paddles on Port A / B (01 = Port A,  10 = Port B) 4 Joystick A Fire Button: 1 = Fire 3-2-2006 Paddle Fire Buttons 3-0 Joystick A Direction (0-15)    MOS 6526 Complex Interface Adapter (CIA) #2
      rts           // 
D9D7C: .byte  $00, $00, $00, $00, $A5, $C5, $C9, $40 // 
       .byte  $D0, $FA, $A5, $C5, $C9, $40, $F0, $FA // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $08, $18, $20 // 
       .byte  $00, $00, $00, $00, $80, $20, $80, $20 // 
       .byte  $00, $00, $00, $00, $01, $0F, $07, $0F // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
L9DBC: clc           // 
      jsr  L96C0    // 
      lda #$7F      // 
      sta $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      ldx #$5A      // 
      ldy #$06      // 
L9DC9: txa           // 
      sta  D47F8,Y  // 
      lda #$03      // 
      sta $D027,Y   // Sprite 0 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      dex           // 
      dey           // 
      bpl  L9DC9    // 
      ldx #$0D      // 
L9DD8: lda  D9DF0,X  // 
      sta $D000,X   // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      dex           // 
      bpl  L9DD8    // 
      inx           // 
      stx $D01C     // Sprites 0-7 Multi-Color Mode Select:  1 = M.C.M.  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      jsr  L9CB0    // 
      rts           // 
D9DE9: .byte  $00, $00, $00, $00, $00, $00, $00 // 
D9DF0: .byte  $44, $68, $5C, $68, $74, $68, $8C, $68 // 
       .byte  $A4, $68, $BC, $68, $D6, $68, $00, $00 // 
L9E00: lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      and #$12      // 
      beq  L9E15    // 
      ldx #$04      // 
      jsr  L9900    // 
      bcs  L9E16    // 
      ldx #$01      // 
      jsr  L9900    // 
      bcs  L9E1C    // 
L9E15: rts           // 
L9E16: lda #$EA      // 
      ldx #$01      // 
      bne  L9E20    // 
L9E1C: lda #$2A      // 
      ldx #$00      // 
L9E20: ldy $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sty  D43F0    // 
      sta $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta  D43F4    // 
      lda $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta  D43F1    // 
      lda current_screen      // 
      sta  D43F2    // 
      stx  D43F3    // 
      lda  D47F8    // 
      sta  D43F6    // 
      ldy #$08      // 
      lda #$C2      // 
      cpx #$00      // 
      bne  L9E4C    // 
      ldy #$FF      // 
      lda #$3B      // 
L9E4C: sty  D43F5    // 
      sta  D43F7    // 
      lda  L0E0B+1  // 
      sta  D43F8    // 
      lda #$01      // 
      sta  DF4E     // 
      jsr  L0EDA    // 
      lda #$0B      // 
      sta $D418     // Select Filter Mode and Volume 7 Cut-Off Voice 3 Output: 1 = Off, 0 = On   6 Select Filter High-Pass Mode: 1 = On 5 Select Filter Band-Pass Mode: 1 = On 4 Select Filter Low-Pass Mode: 1 = On 3-0 Select Output Volume: 0-15    MOS 6581 SOUND INTERFACE DEVICE (SID)
      pla           // 
      pla           // 
      jmp  L9800    // 
D9E6A: .byte  $00, $00, $00, $00, $00, $00 // 
L9E70: lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      and #$40      // 
      beq  L9E9B    // 
      lda $61       // 
      bne  L9E9B    // 
      ldx #$06      // 
      jsr  L9900    // 
      bcc  L9E9B    // 
      lda #$80      // 
      sta $8E       // 
      jsr  L0DAA    // 
      ldx current_screen      // 
      lda  D42C0,X  // 
      and #$BF      // 
      sta  D42C0,X  // 
      lda #$10      // 
      sta  D4371    // 
      jsr  L9D21    // 
L9E9B: rts           // 
D9E9C: .byte  $00, $00, $00, $00 // 
D9EA0: .byte  $A5, $99, $99, $A5, $99, $99, $A9, $55 // 
       .byte  $99, $99, $99, $A9, $65, $65, $65, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $A9, $95, $95, $95, $99, $99, $A9, $55 // 
       .byte  $55, $55, $55, $55, $55, $55, $55, $55 // 
       .byte  $A5, $99, $99, $99, $99, $99, $A5, $55 // 
       .byte  $99, $99, $99, $99, $99, $99, $A9, $55 // 
       .byte  $A5, $99, $99, $99, $99, $99, $A5, $55 // 
       .byte  $A5, $99, $99, $99, $99, $99, $A5, $55 // 
       .byte  $95, $95, $95, $95, $95, $95, $A9, $55 // 
       .byte  $A9, $95, $95, $A5, $95, $95, $A9, $55 // 
       .byte  $00, $00, $00, $00, $00, $00, $00 // 
L9EFF: rts           // 
L9F00: lda  D9997    // 
      beq  L9EFF    // 
      ldx #$03      // 
      lda $D015     // Sprite display Enable: 1 = Enable  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      and #$08      // 
      bne  L9F28    // 
      dec $48       // 
      bne  L9EFF    // 
      ldy #$01      // 
L9F14: lda  D4098,Y  // 
      sta $D006,Y   // Sprite 3 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      dey           // 
      bpl  L9F14    // 
      lda #$08      // 
      sta $D02A     // Sprite 3 Color  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      nop           // 
      nop           // 
      nop           // 
      jsr  L0DA0    // 
L9F28: ldy #$00      // 
      sty $FF       // 
      sec           // 
      lda $D006     // Sprite 3 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cpx $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      sta $55       // 
      sec           // 
      lda $D007     // Sprite 3 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cpx $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp $55       // 
      bcs  L9F80    // 
      jsr  L9F89    // 
      jsr  L9FAD    // 
L9F46: lda $FF       // 
      beq  L9F7C    // 
      dec  L9FD0+1  // 
      bpl  L9F7C    // 
      lda #$04      // 
      sta  L9FD0+1  // 
      ldy  L9FD0+2  // 
      dey           // 
      bpl  L9F5C    // 
      ldy #$03      // 
L9F5C: sty  L9FD0+2  // 
      lda $FE       // 
      beq  L9F65    // 
      lda #$04      // 
L9F65: clc           // 
      adc $FC       // 
      adc #$A0      // 
      sta $FE       // 
      lda #$92      // 
      sta $FF       // 
      lda ($FE),Y   // 
      sta  D47FB    // 
      ldx #$03      // 
      jsr  L9900    // 
      bcs  L9F7D    // 
L9F7C: rts           // 
L9F7D: jmp  L9BA2    // 
L9F80: jsr  L9FAD    // 
      jsr  L9F89    // 
      jmp  L9F46    // 
L9F89: ldx #$03      // 
      lda $D007     // Sprite 3 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp $D001     // Sprite 0 Y Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      beq  L9FAC    // 
      lda #$00      // 
      rol           // 
      pha           // 
      tay           // 
      lda  DBFA,Y   // 
      sta  L9F9E+1  // 
L9F9E: jsr  L0B8B    // 
      pla           // 
      bcs  L9FAC    // 
      sta $FE       // 
      lda #$08      // 
      sta $FC       // 
      inc $FF       // 
L9FAC: rts           // 
L9FAD: ldx #$03      // 
      lda $D006     // Sprite 3 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      cmp $D000     // Sprite 0 X Pos  MOS 6566 VIDEO INTERFACE CONTROLLER (VIC)
      beq  L9FD0    // 
      lda #$00      // 
      rol           // 
      pha           // 
      tay           // 
      lda  DBF8,Y   // 
      sta  L9FC2+1  // 
L9FC2: jsr  L0B00    // 
      pla           // 
      bcs  L9FD0    // 
      sta $FE       // 
      lda #$00      // 
      sta $FC       // 
      inc $FF       // 
L9FD0: rts           // 
       .byte  $04, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $FF, $02, $00, $00, $02, $02, $00 // 
       .byte  $00, $02, $02, $00, $80, $FF, $DF, $BA // 
       .byte  $00, $4E, $30, $3A, $54, $55, $44, $FA // 
       .byte  $B7, $41, $20, $43, $36, $34, $24, $55 // 
       
//*=$a001 start of free space       
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $00 // 
       .byte  $00, $00, $00, $00, $00, $00, $00, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF // 
       .byte  $FF, $FF, $FF, $FF, $FF, $FF, $FF 
MusicData:
.import binary "music_data_and_more.bin"

hero_princess_sprites_backup:       
.import binary "sprites_backup.bin"       


///**  not known if this is needed i'll keep it for now
//*=BC00        
       .byte  $95, $9A, $59, $59, $65, $65, $65, $65, $55 // 
       .byte  $AA, $55, $55, $65, $65, $65, $65, $55 // 
       .byte  $AA, $55, $55, $66, $66, $66, $66, $55 // 
       .byte  $AA, $55, $55, $A9, $55, $55, $95, $55 // 
       .byte  $AA, $55, $55, $95, $95, $95, $95, $55 // 
       .byte  $AA, $55, $55, $65, $65, $65, $65, $55 // 
       .byte  $AA, $55, $55, $55, $55, $55, $55, $56 // 
       .byte  $A6, $65, $65, $59, $59, $59, $59, $65 // 
       .byte  $65, $65, $65, $65, $65, $65, $65, $65 // 
       .byte  $65, $66, $69, $55, $55, $55, $55, $66 // 
       .byte  $66, $66, $A6, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $A9, $55, $55, $55, $55, $95 // 
       .byte  $95, $95, $A9, $55, $55, $55, $55, $65 // 
       .byte  $65, $65, $6A, $55, $55, $55, $55, $55 // 
       .byte  $55, $55, $95, $55, $55, $55, $55, $59 // 
       .byte  $59, $59, $59, $59, $59, $59, $59, $65 // 
       .byte  $65, $65, $65, $65, $65, $65, $65, $55 // 
       .byte  $5A, $59, $59, $59, $59, $59, $59, $55 // 
       .byte  $95, $66, $66, $66, $66, $66, $66, $55 // 
       .byte  $A5, $59, $59, $59, $59, $59, $59, $55 // 
       .byte  $A5, $A5, $99, $99, $99, $96, $96, $55 // 
       .byte  $9A, $99, $99, $9A, $99, $99, $99, $55 // 
       .byte  $A5, $55, $55, $55, $55, $55, $56, $59 // 
       .byte  $59, $59, $59, $59, $59, $59, $59, $65 // 
       .byte  $65, $65, $65, $59, $59, $9A, $95, $5A // 
       .byte  $55, $55, $55, $55, $55, $AA, $55, $95 // 
       .byte  $55, $55, $55, $55, $55, $AA, $55, $A5 // 
       .byte  $55, $55, $55, $55, $55, $AA, $55, $96 // 
       .byte  $55, $55, $55, $55, $55, $AA, $55, $9A // 
       .byte  $55, $55, $55, $55, $55, $AA, $55, $A6 // 
       .byte  $55, $55, $55, $55, $55, $AA, $55, $59 // 
       .byte  $59, $59, $59, $65, $65, $A6, $56  
//**/

*=$c000
//.import source "trainer_code.asm"
trainer_code:
	//copy sprite data
	ldx #0
!:	
	lda intro_sprite_data,x
	sta 832,x
	inx
	bpl !-
//--------------		

	jsr $E544	//clear the screen
//set trainer text	
	ldy #5
loop:	
	ldx #0 	
	lda #2
sm0:
	sta $D80d,x	
sm1:	
	lda intro_text,x
sm2:
	sta $040d,x 	
	inx
	cpx #20
	bne loop+2
	lda sm0+1
	clc 
	adc #$50
	sta sm0+1
	lda sm0+2
	adc #$0
	sta sm0+2
	lda sm1+1
	clc 
	adc #$14
	sta sm1+1
	lda sm1+2
	adc #$0
	sta sm1+2
	lda sm2+1
	clc 
	adc #$50
	sta sm2+1
	lda sm2+2
	adc #$00
	sta sm2+2
	dey
	bne loop
	
	lda #13	
	sta 2040	
	lda #14	
	sta 2041	
	lda #$02
	sta $d01c 	//sprite 1 multicolor
	lda #$00
	sta $d010	//msb x pos
	sta $d01d	//x expand
	sta $d017	//y expand
	//colors
	lda #0
	sta $d020
	sta $d021
	lda #RED
	sta $D027	//sprite 0 color
	lda #LIGHT_GREY
	sta $d028
	
	lda #GREY
	sta $d025	//sprite mc 1
	lda #DARK_GREY	
	sta $d026	//sprite mc 1
	
	//position
	lda #$60
	sta $d000
	sta $d002
	lda #$50
	sta $d001
	sta $d003
	lda #$03
	sta $D015	//sprite 0 and 1 enabled 

!:	lda $cb
	cmp #60
	bne !-
	rts      
intro_text:	
	.text "i'm too young to die"
	.text "hey! not too rough  "
	.text "hurt me plenty      "
	.text "ultra violence      "
	.text "nightmare!          "
intro_sprite_data:
.import binary "../sprites/sprites.bin"

