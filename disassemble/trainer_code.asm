trainer_code:

{
	ldy #25
loopOuter:	
	ldx #0
loopInner:
	lda swallowDataStart,x
	sta $6100,x
	inx
	cpx #64
	bne loopInner
	lda loopInner + 1
	clc
	adc #64
	sta loopInner + 1
	lda loopInner + 2
	adc #0
	sta loopInner + 2

	lda loopInner + 4
	clc
	adc #64
	sta loopInner + 4
	lda loopInner + 5
	adc #1
	sta loopInner + 5
	dey
	bne loopOuter
	//the first 256 bytes on the bottom line need to be cleared
	//it's where the text is drawn
	ldx #0
	lda #$55
!:	sta $6000 + 320*24,x
	inx
	bne !- 
}
	
	//copy sprite data
	ldx #0
!:	
	lda intro_sprite_data,x
	sta 832,x
	inx
	bpl !-
//--------------		

	jsr $E544	//clear the screen
//set trainer text	
	ldy #5
loop:	
	ldx #0 	
	lda #2
sm0a:
	sta $D85d,x	
sm1a:	
	lda intro_text,x
sm2a:
	sta $045d,x 	
	inx
	cpx #20
	bne loop+2
	lda sm0a+1
	clc 
	adc #$50
	sta sm0a+1
	lda sm0a+2
	adc #$0
	sta sm0a+2
	lda sm1a+1
	clc 
	adc #$14
	sta sm1a+1
	lda sm1a+2
	adc #$0
	sta sm1a+2
	lda sm2a+1
	clc 
	adc #$50
	sta sm2a+1
	lda sm2a+2
	adc #$00
	sta sm2a+2
	dey
	bne loop
	
	lda #13	
	sta 2040	
	lda #14	
	sta 2041	
	lda #$02
	sta $d01c 	//sprite 1 multicolor
	lda #$00
	sta $d010	//msb x pos
	sta $d01d	//x expand
	sta $d017	//y expand
	//colors
	lda #0
	sta $d020
	sta $d021
	lda #RED
	sta $D027	//sprite 0 color
	lda #LIGHT_GREY
	sta $d028
	
	lda #GREY
	sta $d025	//sprite mc 1
	lda #DARK_GREY	
	sta $d026	//sprite mc 1
	
	//position
	lda #$60
	sta $d000
	sta $d002
.var default_difficulty = 0	
	lda spritey_positions+default_difficulty
	sta $d001
	sta $d003
	lda #$03
	sta $D015	//sprite 0 and 1 enabled

	sei
	lda #<trainer_irq 		
	sta $0314
	lda #>trainer_irq
	sta $0315

	cli

	 
!:	lda done
	bne !-

	/* debug, set brk vector to $0400
	lda #JMP_ABS
	sta $0400
	lda #<$0400
	sta $0316
	sta $0401
	lda #>$0400
	sta $0317
	sta $0402
	*/
	rts
implement_trainers:
	lda difficulty_setting
	sta $0400
	beq exit_trainer
	jsr giantstouchno 
	dec difficulty_setting
	beq exit_trainer
	jsr infiniteswordsyes
	dec difficulty_setting
	beq exit_trainer
// should be combined with don't lose sword	
	jsr enemiesdontthrowswords
	dec difficulty_setting
	beq exit_trainer
	jsr inifinitelivesyes
//here something should be done about restart position	
// or remove enemy that could kill you	
	jsr start_at_princess
	jsr map_trainer_setup
exit_trainer:
	lda #0 
	sta done
	jmp irq_end      
do_animate:
	lda $d001
	clc
	adc animation+1
	dec animation
	jmp $ea31
trainer_irq:
	ldx color_animate_count
	beq !+
	lda color_animate,x
	sta $D027
	dex
	stx color_animate_count
!:	lda animation
	bne do_animate
	lda $cb
	cmp #60
	beq implement_trainers
	cmp #$07
	bne irq_end //no shift or up/down pressed
	ldx difficulty_setting
	lda $28d
	cmp #1	//shift pressed
	beq up_key
	lda #1
	sta animation+1
	lda #8
	sta animation
	dex
	bpl store_diff 
	ldx #$04	
	bne store_diff	
up_key:	
	lda #254
	sta animation+1
	lda #$10
	sta animation
	inx	
	cpx #5	
	bne store_diff	
	ldx #0
store_diff: 		
	stx difficulty_setting
	lda spritey_positions,x
	sta $d001
	sta $d003
	lda #$0f
	sta color_animate_count

irq_end:	
	jmp $ea31
intro_text:	
	.text "i'm too young to die"
	.text "hey! not too rough  "
	.text "hurt me plenty      "
	.text "ultra violence      "
	.text "nightmare!          "

spritey_positions:
	.fill 5, $80-(i*$10)
done:
	.byte 1
difficulty_setting:	
	.byte default_difficulty	
animation:		
	.byte 0, 0		
color_animate:	
	.byte RED, RED, RED, RED, RED, RED, ORANGE, ORANGE, ORANGE, ORANGE, ORANGE, YELLOW, YELLOW, YELLOW, WHITE, WHITE	
color_animate_count:			
	.byte 0			
intro_sprite_data:		
.import binary "../sprites/sprites.bin"

//trainer functions

giantstouchno:
		lda #CLC
		sta trainer_point_0 //$0c33	//always carry clear -> no collision
		rts
inifinitelivesyes:		
		lda #NOP		
		sta trainer_point_4	//DEC $40 -> NOP NOP		
		sta trainer_point_4+1
		sta trainer_point_5//$0cc4	//DEC $40 -> NOP NOP
		sta trainer_point_5+1//$0cc5
		rts
infiniteswordsyes:
		lda #JSR_ABS	//REROUTE 
		sta trainer_point_3
		lda #<swords_return
		sta trainer_point_3+1
		lda #>swords_return
		sta trainer_point_3+2
		lda #NOP
		sta trainer_point_3+3
		sta trainer_point_3+4
		sta trainer_point_3+5
		sta trainer_point_3+6
		sta trainer_point_3+7
		rts
swords_return:				
		lda $47ff	//if sprite #7 pointer = sword			
		cmp #$7f	//then skip remove from map			
		beq !+				
		lda backuplocationdefs,x				
		and #$f7				
		sta backuplocationdefs,x				
!:		rts						

enemiesdontthrowswords:
		lda #NOP
		sta trainer_point_6//$9945	//jsr 9af9 routine that checks distance enemy to player and throws is skipped
		sta trainer_point_6+1//$9946
		sta trainer_point_6+2//$9947
		rts
remove_princess_blockade: //(no need for key)		
		lda #0		
		sta trainer_point_1 + 1		
		rts		
no_mandarin:

slower_mandarin:

more_time_mandarin:

// pointing way back
satnav:


start_at_princess:			
	lda #$00			
	sta D8026			
	sta D8026 - 2			
	lda #$01			
	sta trainer_point_2			
	lda #$e0			
	sta startposition_player			
	sta D409C	// restart position
	lda #$78			
	sta startposition_player + 1				
	sta D409C + 1	// restart position				
// remove_princess_blockade:					
	lda #0		
	sta trainer_point_1 + 1		
//remove enemy				
	sta backuplocationdefs
	sta backuplocationdefs + 2			
	sta backuplocationdefs + 3			
	rts			
.memblock "trainer map code"	
 .import source "trainer_map.asm"	