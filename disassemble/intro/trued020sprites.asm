.var brkFile = createFile("debug.txt") 

.macro break() {
    .eval brkFile.writeln("break " + toHexString(*))
}

//huge pixels in multicolor sprite format (12x21), Hpixels are 14 lines high x 32 pixels wide (first or last pixel a bit wider).  
//294 lines high total (only 292 visible on a real c64)
//format: 252 bytes (0-251) where byte is value of STA ABS, STX ABS, STAX ABS or STY ABS
//sprite colors: byte 252 = y value, byte 253 = a value, byte 254 = x value, stax color is (a AND x), so choose colors wisely 

//use 64th byte for: color A and color X ?
//use tool to convert 00, 01, 10, 11 pattern to best fit 4 colors? because 4th color = A & X 

.var SCANLINE_1_LENGTH = 43
.var SCANLINE_2A_LENGTH = 50 + 2
.var SCANLINE_2B_LENGTH = 39

.var 	bgColor=0
.var 	aColor = 13
.var 	xColor = 3
.var	colorAZP = $fb 
.var	colorXZP = $fc 
.var	zpPointer = $fd 
.var 	curpixel = 0
.var 	pixelline = 0
.var 	subpixelline = 0 
.var 	calculatedAddress =0
.var	rewriteColorCount = 0  

.var 	fromZP = $06
.var 	toZP   = $08

.var 	firstLine = $00

.var debugCount=0

.const A = STA_ABS 
.const X = STX_ABS 
.const Y = STY_ABS 
.const N = (STA_ABS | STX_ABS) //$8F	 

//claims 64 bytes
.var spriteZp	=   $20
//---------------   $60
//claims 2 bytes used for zp addressing
.var nextSpriteZp = $60	

	
.pc = $0801 "Basic upstart"
:BasicUpstart($0810)		

.pc = $0810 "setup" 
	jsr $e544			//clear screen
	sei
	lda #$35
	sta $01
	jsr setupmusic
	//jsr alternative
	//jsr convertSprites	
.var screens = $c000
	lda #<screens
	sta zpPointer
	lda #>screens
	sta zpPointer + 1
	
	lda #13
	sta colorAZP
	lda #3
	sta colorXZP

	lda #<spritedata
	sta nextSpriteZp
	lda #>spritedata
	sta nextSpriteZp + 1

	ldx #63
!:	lda spritedata,x
	sta spriteZp,x
	dex
	bpl !-

.import source "setupirq.asm"

	jmp *

*=* "irq start"
.import source "setupandstabilizeirq.asm"	
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	jmp startwrite -2 //bit $01
	.print "startwrite: "+ toHexString(startwrite -2)
	
	
	ldx #xColor	// x is not used during the next 294 screen lines of code so can be used as constant.

startwrite: 

	 .for  (curpixel = 0 ; curpixel<12; curpixel++){
		
		ldy.z spriteZp + floor(curpixel/4) //ldy #curpixel
 		getConverterLda(curpixel) //lda (zpPointer),y
		.for(subpixelline = 0; subpixelline < 7; subpixelline++){
			.eval calculatedAddress = calculateAddress(curpixel,  subpixelline)
			sta calculatedAddress
		}  
	}
	
	.while (curpixel < 252){
		drawPixel()																// 56 cycles
		
		ldy.z spriteZp + floor(curpixel/4) //ldy #curpixel
		getConverterLda(curpixel) //lda (zpPointer),y		

		.for(subpixelline = 0; subpixelline < 7; subpixelline++){
			.eval calculatedAddress = calculateAddress(curpixel,  subpixelline) 
			sta calculatedAddress												//7*4
		}
		 .eval curpixel = curpixel + 1  										//28+7=35

		ldy.z spriteZp + floor(curpixel/4)
		getConverterLda(curpixel)

		.for(subpixelline = 0; subpixelline < 7; subpixelline++){
			.eval calculatedAddress = calculateAddress(curpixel,  subpixelline) 
			sta calculatedAddress
		}
		 .eval curpixel = curpixel + 1  
	}
	
	
	.var j
	.var lineLength
	
	// todo use the remaining 12 lines wisely
	 
	.for( var i =0; i< 27; i++) {

		drawPixel()

		
		.if (i<16) {
			.eval j= i
		}
		.if (i>=16) {
			.eval j=15 
		}
	.eval lineLength = *		
		
		ldy #j*4				//2
		lda (nextSpriteZp),y	//5
		sta.z spriteZp + j*4 	//3
	
		ldy #j*4 + 1
		lda (nextSpriteZp),y	
		sta.z spriteZp + j*4 +1 
	
		ldy #j*4 + 2				
		lda (nextSpriteZp),y
		sta.z spriteZp + j*4 +2	
		
		ldy #j*4 + 3
		lda (nextSpriteZp),y
		sta.z spriteZp + j*4 +3
		.fill 15, NOP
	}

	
*=* "irq end"
endint:
	.import source "reversemusic.asm"
	//animate sprite
	//jmp noUpdate
	//jmp nextSprite
			dec spriteDelay
			lda spriteDelay 
			bne noUpdate
			lda #10
			sta spriteDelay
nextSprite:			
			lda nextSpriteZp
			clc
			adc #$40
			sta.z nextSpriteZp
			lda.z nextSpriteZp+1
			adc #$00
			sta.z nextSpriteZp+1
	//joystick to start game
noUpdate:			lda $DC00
			and #$10
			bne !+
			jmp startWillow


	//end irq			
!:    		lda #firstLine
    		sta $d012
    		
			lda $d011
			//ora #$80
			and #%01101111
			sta $d011

    		lda #1
		    sta $D019
			sta $D01A

			lda #<int
			sta $fffe //0314
			lda #>int
			sta $ffff //0315

    pla
    tay
    pla
    tax
    pla
    rti


			//cli
			jmp *



		
*=* "game start sub"
startWillow: 		
			ldx #100
looop:		lda copycode,x 
			sta $0400,x
			dex
			bpl looop
	 		jmp $0400

/* 
This code will be run from $0400			
and will copy the exomized Willow Pattern game data to $0801			
*/			
copycode: 
.pseudopc $0400 { 
	ldy #90
loopouter:
	ldx #$00
loopinner:	
	lda willow,x
	sta $0801,x  
	inx
	bne loopinner
	inc loopinner+2
	inc loopinner+5
	dey
	//sty $0600
	bne	loopouter
/*	restore interrupt and ROM/RAM values otherwise the game will not run */
			sei           // disable interrupts	
     		lda #$37
    		sta $01		// turn on basic and kernel
        	lda #$31
        	sta $0314 	//reset vector 
        	lda #$ea
        	sta $0315        
    		

			lda #$1b
			sta $d011     // restore text screen mode
			lda #$81
			sta $dc0d     // enable Timer A interrupts on CIA 1
			lda #0
			sta $d01a     // disable video interrupts
			bit $dd0d     // re-enable NMI interrupts
			cli

	jmp $080d 
} 
.encoding "screencode_upper"
.text " INTRO BY GOERP "
	
spriteDelay:	.byte 10	
currentSprite:	.byte $00
subSpriteCount: .byte $40
nrSprites:		.byte $10

.align $100
*=* "sprite data"
spritedata:
.import binary "whale.bin"
.import binary "wizball.bin"
//.import binary "subchrist1.bin"
//.import binary "charsetsprites.bin"
*=* "game data"
willow:
.import binary "willowt.bin"
/*==================================================


====================================================*/
*=* "build code sub"
.print "startalternative" + toHexString(alternative) 	
alternative:

	write(LDX_IMM)
	write(xColor)
	ldx #0
altloop1:	
	write(LDY_IMM)
	lda curpixel2
	jsr writebyte
	write(LDA_IZPY)
	write(zpPointer)
	ldy #0
altloop2:
	write(STA_ABS)
	lda calcadrlo,x
	jsr writebyte
	lda calcadrhi,x
	jsr writebyte
	clc
	lda calcadrlo,x
altsm2:	
	adc #(SCANLINE_1_LENGTH + SCANLINE_2A_LENGTH) 
	sta calcadrlo,x
	lda calcadrhi,x
	adc #$00
	sta calcadrhi,x
	iny
	cpy #7
	bne altloop2
	inc curpixel2
	inx
	cpx #12
	bne altloop1
	clc
	lda editedline
	adc #$07
	sta editedline

	.print "editedline:" + toHexString(editedline) 
	.print "curpixel2:" + toHexString(curpixel2) 

//---------------------	
	ldx #0	
altloop3:	
	writeDrawPixel()

.for (var i=0; i<2; i++) {
	write(LDY_IMM)
	lda curpixel2
	jsr writebyte
	write(LDA_IZPY)
	write(zpPointer)
	ldy #0
altloop4a:
	write(STA_ABS)
	lda calcadrlo,x
	jsr writebyte
	lda calcadrhi,x
	jsr writebyte
	//sty altsm4a + 1
	tya 
	clc
	adc editedline
//altsm4a:	
	//adc #$00
	cmp #120
	bcs !+
	lda #(SCANLINE_1_LENGTH + SCANLINE_2A_LENGTH)
	jmp *+2
!:	lda #(SCANLINE_1_LENGTH + SCANLINE_2B_LENGTH)
	//sta altsm4a2 +1
	clc
	adc calcadrlo,x
//altsm4a2:	
	//adc #(SCANLINE_1_LENGTH + SCANLINE_2A_LENGTH) 
	sta calcadrlo,x
	lda calcadrhi,x
	adc #$00
	sta calcadrhi,x
	bcc !+
	nop
!:	iny
	cpy #7
	bne altloop4a
	inc curpixel2
	lda curpixel2
	cmp #252
	bne !+
	jmp altend
!:	inx
	cpx #12		//x=12 -> new pixel row, = seven selfmodified lines
	bne !+
	ldx #0
	clc
	lda editedline
	adc #$07
	sta editedline
!:				
}  jmp altloop3				

altend:
	//write last type of double lines
	ldx#27
altlast:	
		writeDrawPixel()	
		write35NOPs()	
		dex	
	bne altlast	

//end with jmp  to endirq
	write(JMP_ABS)
	lda #<endint
	jsr writebyte
	lda #>endint
	jsr writebyte
	//write(<endint)
//break()
	//write(>endint)
	rts
/*should be called with a loaded*/
/*will write code in current write position and update write position*/
writebyte:
	//cmp startwrite-2 //assert new writing method should yield same results: 
	//beq sm
	//sta $c000
	//lda curpixel2
	//sta $c001
	//lda editedline
	//sta $c002
	//break()			// if not: break in VICE
	//nop
sm: sta startwrite-2	//store a
	inc sm + 1			//update write head
	//inc writebyte + 1
	bne !+
	inc sm + 2
	//inc writebyte + 2
!:	rts

calcadrlo: 
	.byte <startwrite+300+4,<startwrite+300+4+3,<startwrite+300+4+6,<startwrite+300+4+9,<startwrite+300+4+12,<startwrite+300+4+15
	.byte <startwrite+300+4+18,<startwrite+300+4+21,<startwrite+300+4+24,<startwrite+300+4+27,<startwrite+300+4+30,<startwrite+300+4+33
calcadrhi:
	.byte >startwrite+300+4,>startwrite+300+4+3,>startwrite+300+4+6,>startwrite+300+4+9,>startwrite+300+4+12,>startwrite+300+4+15
	.byte >startwrite+300+4+18,>startwrite+300+4+21,>startwrite+300+4+24,>startwrite+300+4+27,>startwrite+300+4+30,>startwrite+300+4+33
curpixel2:	
	.byte 0

editedline:
	.byte 0


*=* "start generated code"
	nop
	nop

//startwrite:


//.align 	$100	
//screens:	
/* here memory will be filled with screen data, 1 block for each sprite converted! */




/*===========================================================
			MACRO AND FUNCTIONS
=============================================================*/			
	
.function calculateAddress(currentPixel, subPixelRow){
	.var pixelRow = floor(currentPixel/12)
	.var pixelColumn = mod(currentPixel,12)
	.var address=0
	.if((pixelRow*7) + subPixelRow < 120) {
		.eval address = startwrite + 300 + 12 + 4 + (pixelColumn*3) + ((SCANLINE_1_LENGTH + SCANLINE_2A_LENGTH) * ((pixelRow*7)+subPixelRow))
	} else {
		.eval address = startwrite + 300 + 12 +  ((SCANLINE_1_LENGTH + SCANLINE_2A_LENGTH) * 120)
		.eval address = address +  4 + (pixelColumn*3) + ((SCANLINE_1_LENGTH + SCANLINE_2B_LENGTH) * (((pixelRow * 7) + subPixelRow) - 120))
	}  
	.return address
}


.function calculateAddressForColor(row){

	.var address=0
	.if(row  < 120) {
		.eval address = startwrite + 300 + 1 + (SCANLINE_1_LENGTH + SCANLINE_2A_LENGTH * row)
	} else {
		.eval address = startwrite + 300 +  (SCANLINE_1_LENGTH + SCANLINE_2A_LENGTH * 120)
		.eval address = address +  1 + (SCANLINE_1_LENGTH + SCANLINE_2B_LENGTH * (row-120))
	}  
	.return address
}

.macro getConverterLda(curpixel){ 
		.var m = mod(curpixel,4) 
		.if(m == 0) {
			lda convertbits76,y
		} 
		.if(m == 1) {
			lda convertbits54,y
		}
		.if(m == 2) {
			lda convertbits32,y
		}
		.if(m == 3) {
			lda convertbits10,y
		}
}

.macro drawPixel(){
		ldy #bgColor
		lda #aColor		
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
		sty $d020
}

.macro write(b){
	lda.im b
	jsr writebyte
}
.macro writeDrawPixel(){
	write(LDY_IMM)
	write(bgColor)
	write(LDA_IMM)
	write(aColor)
	ldy #13
!:		write(STY_ABS)
		write($20)
		write($D0)
	dey 
	bne !-
}
.macro write35NOPs(){
	ldy #35
!:		write(NOP)	
	dey
	bne !-
}
.align $100
convertbits76:
	.fill 256,((i & %11000000) == 0 ? Y: (i & %11000000) == %11000000 ? X: (i & %11000000) == %10000000 ? A: (i & %11000000) == %01000000 ? N:$ff)
convertbits54:	
	.fill 256,((i & %00110000) == 0 ? Y: (i & %00110000) == %00110000 ? X: (i & %00110000) == %00100000 ? A: (i & %00110000) == %00010000 ? N:$ff)
convertbits32:	
	.fill 256,((i & %00001100) == 0 ? Y: (i & %00001100) == %00001100 ? X: (i & %00001100) == %00001000 ? A: (i & %00001100) == %00000100 ? N:$ff)
convertbits10:	
	.fill 256,((i & %00000011) == 0 ? Y: (i & %00000011) == %00000011 ? X: (i & %00000011) == %00000010 ? A: (i & %00000011) == %00000001 ? N:$ff)  	 
