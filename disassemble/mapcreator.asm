/*create bitmap from data in game
screen defs start at $7F40 and consist of 
blocks of 48 bytes 8 wide, 6 high, 
the total map is 8x8 screens*/

.var NotBitMask = $c800//tables
    //.fill 256, 255-pow(2,7-i&7)
.var BitMask = NotBitMask+256
    //.fill 256, pow(2,7-i&7)
.var X_Table = BitMask+256
    //.fill 256, floor(i/8)*8
.var Y_Table_Hi = X_Table+256
    //.fill 200, >GFX_MEM+[320*floor(i/8)]+[i&7]
    //.align $100
.var Y_Table_Lo = Y_Table_Hi + 256 
    //.fill 200, <GFX_MEM+[320*floor(i/8)]+[i&7]

//SO FREE MEMORY FROM CD00 or from c800 after generating map

.var screendata=$8000
.var locationdefs=$42c0
.var enemypos= $41c0
.var treasurepos= $3fc0 
.var bridgepos=$8c60
.var GFX_MEM = $E000
.var musicIRQexit1 = $0e09
.var musicIRQexit2 = $0ecb
.var rerouteaddress = $9966
.var originalroute = $9e00

* = $A203
	sei
	lda #$34
	sta $01

//copy existing bitmap will overwrite parts later
	ldy #32
	ldx #0
smc1:	
	lda $6000,x
	sta GFX_MEM,x
	inx
	bne smc1
	inc smc1+2
	inc smc1+5
	dey
	bne smc1
//also copy char data $4400> $C400
	ldx #0
!:  lda $4400,x	
	sta $C400,x	
    lda $4400+250,x	
	sta $C400+250,x	
    lda $4400+500,x	
	sta $C400+500,x	
    lda $4400+750,x	
	sta $C400+750,x	
	inx
	cpx #250
	bne !-
	
//create data
	jsr createData

	lda #<screendata
	sta $fd
	lda #00
	lda #>screendata
	sta $fe
	
	lda #0
	sta location_index
yloop:
xloop: 
	lax blockpos		
	tay		
	and #$7			//a-> 0-7, xpos in block
	asl
	asl
	clc
	adc xlarge		//add large x position
	sta xpos
	txa
	//and #$f0
	lsr
	lsr
	lsr
	asl
	asl
	clc
	adc ylarge
	sta ypos
	
	lda ($fd),y
	sta plot_or_not_indicator
	.for(var xi=0; xi<4; xi++){
		.for(var yi=0; yi<4; yi++){
			ldx xpos
			ldy ypos
			.for(var i=0; i<xi; i++){
				inx
			}
			.for(var i=0; i<yi; i++){
				iny
			}
			jsr plot_or_not
		}
	}
	
	lda blockpos
	clc
	adc #$01
	sta blockpos
	cmp #48
	beq !+
	jmp xloop

!:	lda #0
	sta blockpos

	ldx location_index
	lda locationdefs,x
	and #%10000000
	beq no_enemy
	sta isbridge
	lda enemypos,x
	jsr plot_enemy_and_bridge

no_enemy:	
	ldx location_index
	lda bridgepos,x
	beq no_bridge
	ldy #0
	sty isbridge
	jsr plot_enemy_and_bridge
	ldx location_index
	lda bridgepos,x
	clc
	adc #$10
	jsr plot_enemy_and_bridge
	
no_bridge:
	ldx location_index
	lda locationdefs,x

	and #%00001000
	beq no_special
	jsr plot_special
no_special:	
	inc location_index
	
	clc
	lda $fd
	adc #48
	sta $fd
	lda $fe
	adc #$00
	sta $fe
	
	lda xlarge
	clc
	adc #$20
	sta xlarge
	//cmp #64
	beq !+
	jmp xloop
	
!:	lda #0
	sta xlarge
	lda ylarge
	clc
	adc #$18
	sta ylarge
	cmp #$C0
	beq end
	jmp yloop
end:	

//now create sprite data at $c800, sprite pointer #$20
	lda #0
	ldx #$3e
!:	sta $c800,x	
	dex	
	bpl !-	
	lda #$f0
	sta $C800
	lda #$90
	sta $C803
	lda #$90
	sta $C806
	lda #$f0
	sta $C809

	lda #$36
	sta $01
	cli 
	//rts
	//reroute original code
	lda #<reroute
	sta rerouteaddress+1
	lda #>reroute
	sta rerouteaddress+2
	jmp $0836

			
plot_or_not:{		
	lda Y_Table_Hi,y
	sta $fc
	lda Y_Table_Lo,y
	sta $fb
	ldy X_Table,x
	lda plot_or_not_indicator
	beq not_plot
		lda BitMask,x
		ora ($fb),y	
		jmp !+
not_plot:
	lda NotBitMask,x
	and ($fb),y
!:	sta ($fb),y
	rts
}		

plot_enemy_and_bridge: {
			//enemy pos bit values:   XXXXYYYY
	tay	//keep value in Y				          	
	and #$f0	//high nybble is X position. has 8 odd values 1x to Ex -> must be translated to value 0-8
	lsr
	lsr
	lsr
	lsr
	tax	
	lda sprite_pos_x_table,x
	clc
	adc xlarge
	sta xpos
	
	tya
	and #$0f
	clc
	tax
	lda sprite_pos_y_table,x
	clc
	adc ylarge
	sta ypos

	.for(var xi=0; xi<4; xi++){
		.for(var yi=0; yi<4; yi++){
			.if(yi < 2){
				lda isbridge
				beq !+
			}
			ldx xpos
			ldy ypos
			.for(var i=0; i<xi; i++){
				inx
			}
			.for(var i=0; i<yi; i++){
				iny
			}
			.if((xi & 1) ==0){
				jsr plot_enemy_and_bridge2
			}else{
				jsr plot_enemy_and_bridge3
			}	 
!:		}	
	}	
	rts
}
plot_enemy_and_bridge2:{
	lda Y_Table_Hi,y
	sta $fc
	lda Y_Table_Lo,y
	sta $fb
	ldy X_Table,x
	lda BitMask,x
	ora ($fb),y	
	sta ($fb),y
	rts
}
plot_enemy_and_bridge3:{
	lda Y_Table_Hi,y
	sta $fc
	lda Y_Table_Lo,y
	sta $fb
	ldy X_Table,x
	lda NotBitMask,x
	and ($fb),y
	sta ($fb),y
	rts
}
plot_special:{
	lda treasurepos,x		//enemy pos bit values:   XXXXYYYY
	tay	//keep value in Y				          	
	and #$f0	//high nybble is X position. has 8 odd values 1x to Ex -> must be translated to value 0-8
	lsr
	lsr
	lsr
	lsr
	tax	
	lda sprite_pos_x_table,x
	clc
	adc xlarge
	sta xpos
	
	tya
	and #$0f
	clc
	tax
	lda sprite_pos_y_table,x
	clc
	adc ylarge
	sta ypos

	lda #4
	sta tx
	sta ty
	
	//.for(var xi=0; xi<4; xi++){
	//	.for(var yi=0; yi<4; yi++){
ps0:	
	lda ty
	cmp #2
	bcs ps1
			//.if(yi < 2){
	lda iskey
	bne ps1
	jmp endloop
			//}
ps1:			
	lda tx
	cmp #2
	bcs ps2
	//.if(xi < 2){
	lda issword
	bne ps2
	jmp endloop
	//		}
			
ps2:			
		ldx xpos
		ldy ypos
		
		lda tx
			//.for(var i=0; i<xi; i++){
!:				inx
			//}
		sec
		sbc #$01
		bne !-
		lda ty
			//.for(var i=0; i<yi; i++){
!:		iny
		sec
		sbc #$01
		bne !-
		
			//}
		lda tx
		and #$01
		beq !+
			//.if((xi & 1) == 1){			
		jsr plot_special2			
		jmp endloop		
			//} else {			
!:		jsr plot_special3						
		//	}			
					
		//}
	//}
endloop:	
	dec ty
	bne ps0
	lda #4
	sta ty
	dec tx
	bne ps0
 	
	rts
}
plot_special2:{	
			lda Y_Table_Hi,y
			sta $fc
			lda Y_Table_Lo,y
			sta $fb
		    ldy X_Table,x
				lda BitMask,x
				ora ($fb),y	
			sta ($fb),y
	rts
}	
plot_special3:{	
		lda Y_Table_Hi,y
		sta $fc
		lda Y_Table_Lo,y
		sta $fb
		lda NotBitMask,x
		and ($fb),y
		sta ($fb),y
	rts
}	
tx: .byte 0
ty: .byte 0

isbridge:
	.byte 0
issword:
	.byte 1
iskey:
	.byte 1

*=*
enemy_x: 	
	.byte 0
enemy_y: 	
	.byte 0

sprite_pos_x_table:
	.byte 0,0,4,4,8,8,12,12,16,16,20,20,24,24,28,28

sprite_pos_y_table:
	.byte 0,0,4,4,8,8,12,12,16,16,20,20,24,24,28,28

player_sprite_x:
	.fill 8, ($40+ i*$20)
player_sprite_y:	
	.fill 8, ($20+ i*$40)

location_index:
	.byte 0


plot_or_not_indicator:	
	.byte 0
xpos:
	.byte 0
ypos:	
	.byte 0	

xlarge:
	.byte 0
ylarge:	
	.byte 0	
blockpos:		
	.byte 0		
	  	
    .align $100
createData:    
	ldy #0
cd1:	tya	
	and #$07	
	tax
	lda pow2rev,x
	sta NotBitMask,y
	lda pow2,x
	sta BitMask,y
	tya
	lsr
	lsr	
	lsr
	asl
	asl
	asl
	sta X_Table,y
	tya
	//y table only below 200, but is aligned so does not save memory to skip last 55 
	lda tmplo
	sta Y_Table_Lo,y
	lda tmphi
	sta Y_Table_Hi,y

	lda tmplo
	clc
	adc #$01
	sta tmplo
	lda tmphi
	adc #$00
	sta tmphi
//only add 320-7 every 8 lines
!:	iny
	beq cdend
	tya
	and #$07
	bne !+
	lda tmplo
	clc
	adc #(320-256-8)
	sta tmplo
	lda tmphi
	adc #1
	sta tmphi
!:	jmp cd1
cdend:    
    rts

spriteson: .byte 0
spritex:	.byte 0
spritey:	.byte 0


altirq:
	rti

reroute:
	lda $c5
	cmp #36		//'M' pressed?
	beq showmap
	jmp originalroute 	//return to original address
    
showmap:
	//save sprite pos
	lda $d015
	sta spriteson
	//sprite 5 on, rest off
	lda #%00100000
	sta $d015

	
	lda $d000
	lsr
	lsr
	lsr
	lsr
	lsr
	sta spritex
 
	
	lda $d001
	lsr
	lsr
	lsr
	lsr
	lsr
	sta spritey

	//sprite pointer
	lda #$20
	sta $c7f8+5
	//sprite color
	lda #$00
	sta $d02c


	//put sprite 5 at right position
	//round(value of 5f/8) -> y position
	//value of 5f and 7 -> x position
	//one gives values of 0-7. Multiply by 32 to get values of 0-224
	//other values of 00-70 multiply by 2 to get values of 00 to e0
	lda $5f
	and #$07
	tax
	lda player_sprite_x,x
	clc
	adc spritex
	sta spritex

	lda $5f
	and #$f8
	lsr
	lsr
	lsr
	tax
	lda player_sprite_y,x
	adc spritey
	sta spritey
	
.print "spritex" + toHexString(spritex)	
	sei //disable interrupts because they point to kernal which will be disabled

	//vic bank 3
	lda $dd00
	and #%11111100
	ora #%00000000
	sta $dd00


	//use mem under KERNAL
	lda #$34
	sta $01
	//rewrite exit music routine so it no longer points to ea31, which cannot be use without kernal 
	lda #<altirq
	sta musicIRQexit1
	sta musicIRQexit2
	lda #>altirq
	sta musicIRQexit1+1
	sta musicIRQexit2+1
	//normal interrupt pointers son't work without kernal
	lda $0314 
	sta $fffe
	lda $0315 
	sta $ffff

	lda spritex
	sta $d00a
	lda spritey
	sta $d00b


	lda #$36
	sta $01
	
	lda spritex
	sta $d00a
	lda spritey
	sta $d00b
	
//rewrite colors, d800-dbff, c400-c7ff 
	lda #$00
	sta smcol1 + 1
	lda #$c4
	sta smcol1 + 2
	lda #$00
	sta smcol2 + 1
	lda #$d8
	sta smcol2 + 2
	ldy #24
!:	ldx #0
	lda #$45
smcol1:	
	sta $c400,x	
	lda #$78	
smcol2:		
	sta $d800,x	
	inx
	cpx #$20
	bne smcol1-2
	clc
	lda smcol1+1
	adc #$28
	sta smcol1+1
	lda smcol1+2
	adc #$00
	sta smcol1+2
	clc
	lda smcol2+1
	adc #$28
	sta smcol2+1
	lda smcol2+2
	adc #$00
	sta smcol2+2
	dey 	
	bne !- 	
	lda #$34
	sta $01

keyloop:
	//enable kernal to read joystick
	ldx #$36
	ldy #$34
	stx $01
	ldx spritey
	stx $d00b
	ldx spritex
	stx $d00a
	lda $dc00
	sty $01
	and #$10
	beq restore
	jmp keyloop
restore:	
	
//rewrite exit music routine to standard irq  
	sei	
	lda #$31
	sta musicIRQexit1
	sta musicIRQexit2
	lda #$ea
	sta musicIRQexit1+1
	sta musicIRQexit2+1
	lda #$36
	sta $01
	//vic bank 1
	lda $dd00
	and #%11111100
	ora #%00000010
	sta $dd00
	cli
	//rebuild screen, only D800 area was overwritten, but this is easiest
	jsr $0a00
	lda spriteson
	sta $d015

	cli
	jmp originalroute	//back to original address	
    
/*.var NotBitMask = fill 256, 255-pow(2,7-i&7)
.var BitMask = fill 256, pow(2,7-i&7)
.var X_Table = 256, floor(i/8)*8
.var Y_Table_Hi = fill 200, >GFX_MEM+[320*floor(i/8)]+[i&7]
.var Y_Table_Lo = fill 200, <GFX_MEM+[320*floor(i/8)]+[i&7]
*/    
tmplo: 
	.byte <GFX_MEM    
tmphi: 
	.byte >GFX_MEM    
tmp0:
	.byte 0
pow2:
	.fill 8, pow(2,7-i)
pow2rev:	
	.fill 8, 255-pow(2,7-i)	
	
tables:    
.print toHexString(NotBitMask) 
.print "yhi" + toHexString(Y_Table_Hi) 
.print "ylo" + toHexString(Y_Table_Lo) 